from django.db import models
from django.db.models import Q

from money.models.accounts import Account
from money.models.categories import Category


class OperationTemplate(models.Model):
    title = models.CharField(max_length=255)
    amount = models.FloatField(default=None, null=True, blank=True)
    description = models.CharField(max_length=255, blank=True, default=None, null=True)
    comment = models.TextField(blank=True, default=None, null=True)
    method = models.CharField(max_length=50, blank=True, default=None, null=True)
    accounts = models.ManyToManyField(Account)
    categories = models.ManyToManyField(Category, blank=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['title'], name='unique_name'),
            models.CheckConstraint(
                check=(
                    ~Q(amount=None)
                    | ~Q(description=None)
                    | ~Q(comment="")
                    | ~Q(method=None)
                ),
                name="must_provide_data"
            )
        ]

    def __str__(self):
        return "Operation template: " + self.title
