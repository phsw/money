from django.db import models
from django.utils.translation import gettext_lazy as _


class Category(models.Model):
    title = models.CharField(max_length=100)
    auto_affect = models.CharField(max_length=255, blank=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['title'], name='category_unique_name')
        ]

    def __str__(self):
        return self.title

    @property
    def auto_affects(self):
        if self.auto_affect == "":
            return []
        else:
            return self.auto_affect.split(",")

    @classmethod
    def without_category(cls):
        return Category(pk=0, title=_("Without category"))
