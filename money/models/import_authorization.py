from django.db import models
import datetime


class ImportAuthorization(models.Model):
    key = models.CharField(max_length=65)
    date = models.DateTimeField(default=datetime.datetime.now)
