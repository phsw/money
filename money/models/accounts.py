from django.db import models
from django.db.models import Q
from django.core.validators import MinValueValidator


class Account(models.Model):
    title = models.CharField(max_length=100)
    number = models.BigIntegerField(unique=True)
    balance = models.FloatField(default=None, blank=True, null=True)
    balance_date = models.DateField(default=None, blank=True, null=True)
    no_pendings = models.BooleanField(default=False)
    balance_limit = models.FloatField(default=None, blank=True, null=True, validators=[
        MinValueValidator(0.01)
    ])
    initial_amount = models.FloatField(default=0.00, blank=True, null=False, validators=[
        MinValueValidator(0.00)
    ])
    support_amount = models.FloatField(default=None, blank=True, null=True)
    comment = models.TextField(blank=True)
    bank_local_balances_can_be_different = models.BooleanField(default=False)

    class Meta:
        constraints = [
            models.CheckConstraint(
                check=Q(balance_limit=None) | Q(balance_limit__gt=0),
                name="account_balance_limit_positive"
            ),
            models.CheckConstraint(
                check=Q(support_amount=None) | Q(support_amount__gt=0),
                name="account_support_amount"
            ),
        ]

    def __str__(self):
        return self.title + " (" + str(self.number) + ")"
