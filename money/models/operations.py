from django.db import models
from django.db.models import Q, F
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.dispatch import receiver
import datetime

from money.models.accounts import Account
from money.models.categories import Category


class Operation(models.Model):
    class BankOperationImportMethod(models.IntegerChoices):
        WEB = 1
        CSV = 2

    _date_paid = models.DateField(db_column="date_paid", default=datetime.date.today)
    _bank_date = models.DateField(null=True, db_column="bank_date")
    import_datetime = models.DateTimeField(null=True, blank=True)
    _amount = models.FloatField(db_column="amount")
    description = models.CharField(max_length=255, blank=True, default="")
    bank_description = models.CharField(max_length=255, blank=True, default="")
    comment = models.TextField(blank=True)
    bank_comment = models.TextField(blank=True)
    read = models.BooleanField(default=False)
    method = models.CharField(max_length=50)
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    categories = models.ManyToManyField(Category, blank=True)
    bank_import_method = models.IntegerField(
        choices=BankOperationImportMethod.choices,
        null=True,
        default=None
    )
    linked_operations = models.ManyToManyField("self", blank=True)
    takes_from_support = models.BooleanField(default=False)

    class Meta:
        constraints = [
            models.CheckConstraint(check=~Q(_amount=0), name="operation_not_zero"),
            models.CheckConstraint(
                check=Q(_bank_date=None) | Q(_date_paid__lte=F('_bank_date')),
                name="date_paid_before_acted"
            ),
            models.CheckConstraint(
                check=(
                    Q(_bank_date=None) & Q(bank_import_method=None))
                | (~Q(_bank_date=None) & ~Q(bank_import_method=None)),
                name="bank_operation_has_import_method"
            ),
            models.CheckConstraint(
                check=(~Q(_bank_date=None) | Q(takes_from_support=False)),
                name="only_bank_operations_take_from_support"
            ),
        ]

    @property
    def date_paid(self):
        return self._date_paid

    @date_paid.setter
    def date_paid(self, value):
        if isinstance(value, str):
            value = datetime.date.fromisoformat(value)
        elif not isinstance(value, datetime.date):
            value = datetime.date(value)

        self._date_paid = value

    @property
    def bank_date(self):
        return self._bank_date

    @bank_date.setter
    def bank_date(self, value):
        if isinstance(value, str):
            value = datetime.date.fromisoformat(value)
        elif not isinstance(value, datetime.date):
            value = datetime.date(value)

        self._bank_date = value

        if self.bank_import_method is None:
            # By default, setting bank_date define import_method, mostly to ease tests
            # unless other method was already specified
            self.bank_import_method = self.BankOperationImportMethod.WEB

    @property
    def amount(self):
        return self._amount

    @amount.setter
    def amount(self, value):
        if not isinstance(value, float):
            value = float(value)

        self._amount = value

    @property
    def is_bank_operation(self):
        return (self._bank_date is not None)

    @property
    def is_merged_with_pending(self):
        return (len(self.description) != 0 and len(self.bank_description) != 0)

    @property
    def all_categories(self):
        if len(self.categories.all()) > 0:
            return self.categories.all()
        else:
            return [Category.without_category()]

    def clean_fields(self, exclude=None):
        if (exclude is None or 'amount' not in exclude) and self.amount == 0.:
            raise ValidationError({'amount': _("Amount cannot be zero.")})

        if self.bank_date is not None and self.date_paid > self.bank_date:
            raise ValidationError({"bank_date": _("Paid date must be before acted date.")})

        description_len = 0
        if self.description is not None:
            description_len = len(self.description)

        bank_description_len = 0
        if self.bank_description is not None:
            bank_description_len = len(self.bank_description)

        if description_len == 0 and bank_description_len == 0:
            raise ValidationError(_("Either description or bank description must be provided."))

        return super().clean_fields(exclude)

    def __str__(self):
        string = ""

        if self.date_paid is not None:
            string = str(self.date_paid) + ": "

        if hasattr(self, 'account'):
            string += self.account.title + " - "

        if self.method != "":
            string += self.method + " - "

        if self.description != "":
            string += self.description
        else:
            string += self.bank_description

        if self.amount is not None:
            string += " | " + format(self.amount, '.2f') + "€"

        if string == "" or string == str(datetime.date.today()) + ": ":
            string = "Empty Operation"

        return string

    def is_same_as(self, other):
        if not isinstance(other, Operation):
            return False

        # Comparing descriptions and coments sometime are not accurate, especially when
        # comparing imported bank operations from csv and imported bank operations from
        # web.

        compared_fields = ['_bank_date', '_amount']

        if self.bank_import_method == other.bank_import_method:
            compared_fields += ['bank_description']

        for field in compared_fields:
            if isinstance(vars(self)[field], str):
                # CANE web bank evolved and descriptions became uppercase...:
                if vars(self)[field].lower() != vars(other)[field].lower():
                    return False
            else:
                if vars(self)[field] != vars(other)[field]:
                    return False

        return True

    @property
    def transfer_opposite_operation(self):
        """
        If the operation is a transfer to another account, this property
        returns the operation at the other side of the transfer.
        """
        for o in self.linked_operations.all():
            if (o.account != self.account
                    and o.amount == -self.amount
                    and o.bank_date == self.bank_date):
                return o

        return None

    def toCsvRow(self):
        categories = ", ".join([c.title for c in self.categories.all()])

        return [
            self._bank_date.strftime("%d/%m/%Y"),
            self._date_paid.strftime("%d/%m/%Y"),
            self.method,
            self.bank_description,
            self.bank_comment,
            self.description,
            self.comment,
            str(self.amount),
            categories
        ]


class OperationReceipt(models.Model):
    class OperationReceiptType(models.IntegerChoices):
        OTHER = 0, _('Other')
        BILL = 1, _('Bill')
        RECEIPT = 2, _('Receipt')
        QUOTE = 3, _('Quote')
        WARRANTY = 4, _('Warranty')

    operation = models.ForeignKey(Operation, on_delete=models.CASCADE)
    receipt_file = models.FileField(upload_to='uploads')
    filename = models.CharField(max_length=255)
    kind = models.IntegerField(
        choices=OperationReceiptType.choices,
        default=OperationReceiptType.OTHER
    )

    def __str__(self):
        return str(self.operation) + " | " + self.filename + " | " + str(self.get_kind_display())


@receiver(models.signals.post_delete, sender=OperationReceipt)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    instance.receipt_file.delete(save=False)


@receiver(models.signals.pre_save, sender=OperationReceipt)
def auto_delete_old_file_on_update(sender, instance, **kwargs):
    instance.filename = instance.receipt_file.name

    if instance.pk:
        old_operation_receipt = OperationReceipt.objects.get(pk=instance.pk)
        if old_operation_receipt.receipt_file.name != instance.receipt_file.name:
            old_operation_receipt.receipt_file.delete(save=False)
