from django import template

from money.utils import constants


register = template.Library()


def _generate_pagination(n, index, padding=constants.paginator_padding):
    """Tells which index to show in pagination buttons.
    @param n number of items in the whole pagination
    @param index current index in the pagination
    @param padding number of available items before and after the current index
    @return a list of indexes to display, None values has to be omitted in the template
    """

    pagination = [1]

    if index != 1:
        padding_before_start = max(index - padding, 2)

        if padding_before_start == 3:
            pagination.append(2)
        elif padding_before_start != 2:
            pagination.append(None)

        pagination.extend(range(padding_before_start, index))

        pagination.append(index)

    if index == n:
        return pagination

    padding_after_end = min(index + padding, n-1)

    pagination.extend(range(index+1, padding_after_end+1))

    if padding_after_end == (n-2):
        pagination.append(n-1)
    elif padding_after_end != (n-1):
        pagination.append(None)

    pagination.append(n)

    return pagination


@register.inclusion_tag('templatetags/pagination.html', takes_context=True)
def display_pagination(context, *args, **kwargs):
    hashtag = ""

    if 'hashtag' in kwargs:
        hashtag = '#' + kwargs['hashtag']

    if 'paginator' in context:
        return {
            'indexes': _generate_pagination(
                context['paginator'].num_pages,
                context['page_obj'].number
            ),
            'current_index': context['page_obj'].number,
            'hashtag': hashtag
        }
    else:
        return {
            'indexes': []
        }
