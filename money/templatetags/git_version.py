import subprocess

from django import template


register = template.Library()


@register.simple_tag
def git_version():
    return subprocess.run(
        ["git", "describe", "--tags", "--always"],
        stdout=subprocess.PIPE,
        stderr=subprocess.DEVNULL
    ).stdout.decode('utf-8').strip()
