from django import template

from money.models.accounts import Account


register = template.Library()


@register.inclusion_tag('templatetags/account_occupancy.html')
def account_occupancy(account: Account):
    assert (account.balance_limit is not None)

    balance = account.balance
    occupancy = account.occupancy
    if balance is None:
        assert (occupancy is None)
        balance = 0
        occupancy = 0

    return {
        "balance": balance,
        "balance_limit": account.balance_limit,
        "bar_width": min(occupancy, 100.0),
        "occupancy": occupancy
    }
