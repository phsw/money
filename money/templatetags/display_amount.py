from django import template

register = template.Library()


@register.inclusion_tag('templatetags/display_amount.html')
def display_amount(amount, without_color=False):
    context = {'amount': amount}

    if amount is None or amount == 0 or without_color:
        context['css'] = None
    elif amount > 0:
        context['css'] = "text-success"
    else:
        context['css'] = "text-danger"

    return context
