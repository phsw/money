from django import template

register = template.Library()

"""
To get previous and next element of an element in this list,
based on https://stackoverflow.com/questions/32795907/how-to-access-the-next-and-the-previous-elements-in-a-django-template-forloop/32801096#32801096 # noqa: E501

Use:
{% with next_element=some_list|next:forloop.counter0 %}
{% with previous_element=some_list|previous:forloop.counter0 %}
"""


@register.filter
def next(some_list, current_index):
    try:
        return some_list[int(current_index) + 1]
    except Exception:
        return None


@register.filter
def previous(some_list, current_index):
    try:
        return some_list[int(current_index) - 1]
    except Exception:
        return None
