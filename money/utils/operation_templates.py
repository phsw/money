from money.models.operations import Operation


def get_init_values(o: Operation) -> dict:
    """
    Initialize values of the form to add a new operation template from an operation.
    """
    return {
        'title': o.description,
        'amount': o.amount,
        'description': o.description,
        'comment': o.comment,
        'method': o.method,
        'accounts': [o.account],
        'categories': o.categories.all()
    }
