import datetime
import csv
from typing import List, Union
import math
from django.conf import settings
from django.db import transaction
from django.db.models import Q
from django.http import HttpResponse
from django.utils.translation import gettext_lazy as _

from money.models.operations import Operation
from money.models.operation_templates import OperationTemplate
from money.models.accounts import Account


def unlink(bank_operation: Operation) -> Operation:
    """
    Split an operation into a bank operation and a pending operation.

    Modify the parameter operation to become only a bank operation and
    return a pending operation.
    """
    assert (bank_operation.is_merged_with_pending)

    pending_operation = Operation(
        date_paid=bank_operation.date_paid,
        description=bank_operation.description,
        comment=bank_operation.comment,
        amount=bank_operation.amount,
        read=False,
        account=bank_operation.account,
        method=bank_operation.method
    )

    bank_operation.read = False
    bank_operation.description = ""
    bank_operation.comment = ""
    bank_operation.date_paid = bank_operation.bank_date

    with transaction.atomic():
        pending_operation.save()
        for c in bank_operation.categories.all():
            if c not in pending_operation.categories.all():
                pending_operation.categories.add(c)
        pending_operation.save()

        bank_operation.save()

    return pending_operation


def merge_bank_in_pending_operation(bank_operation: Operation, pending_operation: Operation):
    assert (not bank_operation.is_merged_with_pending)
    assert (not pending_operation.is_merged_with_pending)
    assert (pending_operation.account == bank_operation.account)

    pending_operation.bank_date = bank_operation.bank_date
    pending_operation.bank_comment = bank_operation.bank_comment
    pending_operation.bank_description = bank_operation.bank_description
    pending_operation.read = False
    pending_operation.import_datetime = bank_operation.import_datetime

    for c in bank_operation.categories.all():
        if c not in pending_operation.categories.all():
            pending_operation.categories.add(c)


def get_page_containing_date(
        account: Account,
        date: datetime.date,
        paginate_by: int = settings.MONEY['paginate_by']
):
    nb_operations = Operation.objects.filter(
        Q(account=account) & ~Q(_bank_date=None) & Q(_bank_date__gte=date)
    ).count()

    response = math.ceil(nb_operations / paginate_by)

    if response == 0:
        return 1
    else:
        return response


def all_to_all_linked_operations(initial_operation: Operation) -> None:
    """
    Look recursively for all linked_operations and at the end,
    all operations are linked to all operations
    """
    operations = []
    to_explore = [initial_operation]

    while len(to_explore) > 0:
        operation = to_explore.pop()

        for op in operation.linked_operations.all():
            if op not in operations:
                operations.append(op)
                to_explore.append(op)

    for i in range(len(operations)):
        for j in range(i+1, len(operations)):
            if operations[j] not in operations[i].linked_operations.all():
                operations[i].linked_operations.add(operations[j])

    for op in operations:
        op.save()


def hidden_operations(operations, account) -> List[int]:
    hidden_operations = []

    for op in operations:
        s = 0.

        if op.linked_operations.count() > 0:
            for o in op.linked_operations.all():
                if o.account == account and o.is_bank_operation:
                    s += o.amount

            if round(s, 2) == round(-op.amount, 2):
                # hide current operation:
                if op.read and op.id not in hidden_operations:
                    hidden_operations.append(op.id)

                # hide other linked operation:
                for o in op.linked_operations.all():
                    if (o.account == account
                            and o.is_bank_operation
                            and o.read
                            and o.id not in hidden_operations):
                        hidden_operations.append(o.id)

    return hidden_operations


def filter_by_categories(operations, categories, has_all_categories, has_only_these_categories):
    """
    Returns only operations with given `categories`.
    If `has_all_categories` is set to True, operation must have all categories in
    `categories` (and), otherwise having only one category from `categories` is
    sufficient to keep the operation (or)
    If `has_only_these_categories` is set to True, operations cannot have other
    categories than those in `categories` to be kept.
    """
    filtered = []

    for o in operations:
        add = None

        number_matching_categories = 0
        for c in categories:
            if c in o.categories.all():
                number_matching_categories += 1

        if has_all_categories:
            if len(categories) == number_matching_categories:  # all asked categories are present:
                if has_only_these_categories:  # operation cannot have more than asked categories:
                    if o.categories.count() > len(categories):
                        add = False
                    else:
                        assert (o.categories.count() == len(categories))
                        add = True
                else:  # operation can have more than asked categories
                    add = True
            else:
                assert (number_matching_categories < len(categories))
                add = False
        else:  # not has_all_categories, having only one asked category is sufficient:
            if number_matching_categories > 0:
                if has_only_these_categories:  # there is no other categories than those asked:
                    for c in o.categories.all():
                        if c not in categories:
                            add = False
                    if add is None:
                        add = True
                else:  # operation can have other categories:
                    add = True
            else:
                add = False

        if len(categories) == 0:
            if has_only_these_categories:
                add = (o.categories.count() == 0)
            else:
                add = True

        assert (add is not None)

        if add:
            filtered.append(o)

    return filtered


def operations_to_csv(operations, filetitle: str):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="' + filetitle + '.csv"'

    writer = csv.writer(response)
    writer.writerow([
        "Bank date",
        "Paid date",
        "Method",
        "Bank description",
        "Bank comment",
        "Description",
        "Comment",
        "Amount",
        "Categories"
    ])

    for o in operations:
        writer.writerow(o.toCsvRow())

    return response


def get_init_values(obj: Union[Operation, OperationTemplate], is_refund: bool = False) -> dict:
    """
    Initialize values of the form to add a new operation.
    """
    values = {
        'method': obj.method,
        '_amount': obj.amount,
        'description': obj.description,
        'comment': obj.comment,
        'categories': obj.categories.all()
    }

    if is_refund:
        values['_amount'] = -values['_amount']
        values['description'] += _(" - Refund")
        values['linked_operations'] = [obj]

    return values
