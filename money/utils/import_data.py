import bs4
import csv
import datetime
from enum import Enum
import logging
import re
from typing import List, Tuple
import warnings

from django.db import transaction
from django.db.models import Q
from django.utils.translation import gettext_lazy as _

from money.models.accounts import Account
from money.models.operations import Operation
from money.models.categories import Category
from money.utils import auto_affect_categories

logger = logging.getLogger(__name__)

warnings.filterwarnings('ignore', category=bs4.XMLParsedAsHTMLWarning)


FRENCH_MONTHS = [
    "janvier", "février", "mars", "avril", "mai", "juin",
    "juillet", "août", "septembre", "octobre", "novembre", "décembre"
]


def french2python_date(date_str: str) -> datetime.date:
    """Convert a string following the format "06 juillet 2023" to a Python date
    object."""

    french_months_to_number = {m: i+1 for i, m in enumerate(FRENCH_MONTHS)}
    date_elements = date_str.split(" ")

    return datetime.date(
        int(date_elements[2]),
        french_months_to_number[date_elements[1]],
        int(date_elements[0])
    )


class AccountType(Enum):
    DEFAULT = 0
    EPARGNE_LONG_TERME = 1
    EPARGNE = 2


class ProcessImportBankOperations:
    def __init__(self):
        self.operations: List[Operation] = []
        self.to_create: List[Operation] = []
        self.to_update: List[Operation] = []
        self.account: Account = None

    @staticmethod
    def _find_date_indication(operation: Operation) -> datetime.date:
        assert (operation.is_bank_operation)

        if len(operation.bank_description) == 0:
            return None

        p = re.compile(r"^.+\s+(\d{2})/(\d{2})$")
        m = p.match(operation.bank_description)

        if m is None:
            return None

        day = int(m.group(1))
        month = int(m.group(2))
        year = operation.bank_date.year

        if day > 31 or month > 12:
            return None

        if month > operation.bank_date.month:
            year -= 1

        desc_date = datetime.date(year=year, month=month, day=day)

        delta = operation.bank_date - desc_date

        if abs(delta.days) > 14:
            return None

        return desc_date

    def keep_only_new_operations(self) -> None:
        max_date = max([o.bank_date for o in self.operations])
        min_date = min([o.bank_date for o in self.operations])

        assert (min_date <= max_date)
        stored_operations = Operation.objects.filter(
            account=self.account,
            _bank_date__range=(min_date, max_date)
        )

        to_remove = []
        # General idea of these two nested loops:
        # once a couple operation<->stored_operation is found, exclude them and
        # interrupt current iterations of the two loops.
        i_exclude = []
        j_exclude = []
        for i in range(len(self.operations)):
            if i not in i_exclude:
                for j in range(len(stored_operations)):
                    if j not in j_exclude:
                        if stored_operations[j].is_same_as(self.operations[i]):
                            assert (i not in i_exclude)
                            assert (j not in j_exclude)
                            assert (i not in to_remove)
                            to_remove.append(i)
                            i_exclude.append(i)
                            j_exclude.append(j)
                            break
                    if i in i_exclude:
                        break

        for r in reversed(to_remove):
            del self.operations[r]

    def match_with_pendings(self):
        import_datetime = datetime.datetime.now()

        # We don't need to match with pendings, if account doesn't support pending operations:
        if self.account.no_pendings:
            for operation in self.operations:
                operation.import_datetime = import_datetime
                self.to_create.append(operation)

            return

        for operation in self.operations:
            matched_pendings = Operation.objects.filter(
                _bank_date__isnull=True,
                _date_paid__lte=operation.bank_date,
                # bank has 20 days to register an operation:
                _date_paid__gte=operation.bank_date - datetime.timedelta(days=20),
                bank_description__exact="",
                bank_comment__exact="",
                _amount=operation.amount,
                account=self.account,
                # exclude pending operation already merged during the current import:
            ).exclude(pk__in=[e.pk for e in self.to_update]).order_by("_date_paid")

            pending = None

            if len(matched_pendings) == 1:
                pending = matched_pendings[0]
            elif (len(matched_pendings) > 1 and matched_pendings[0].date_paid in
                    [operation.bank_date, self._find_date_indication(operation)] and
                    matched_pendings[0].date_paid != matched_pendings[1].date_paid):
                # If there are several possible pending operations, we can
                # match only with the oldest if dates coincide, otherwise we
                # cannot know if the bank operation is for the older or more
                # recent pending operation. The selected pending operation has
                # to be the lonely one with that date (otherwise we cannot
                # select the right one).
                pending = matched_pendings[0]

            if pending is not None and pending not in self.to_update:
                pending.bank_date = operation.bank_date
                pending.bank_comment = operation.bank_comment
                pending.bank_description = operation.bank_description
                pending.read = False
                pending.import_datetime = import_datetime

                self.to_update.append(pending)
            else:
                operation.import_datetime = import_datetime
                self.to_create.append(operation)

    def _affect_categories(self):
        categories = Category.objects.filter(~Q(auto_affect=""))

        # cannot use bulk_* functions because we are updating ManyToMany fields
        for o in self.to_create:
            auto_affect_categories.affect_categories(o, categories)
            o.save()

        # we don't affect categories to self.to_update, since this list contains pending operations
        # which normally have already categories.

    def save(self):
        with transaction.atomic():
            # cannot use bulk_create, because we need generated IDs to then affect categories
            for o in self.to_create:
                o.account = self.account
                o.save()

            Operation.objects.bulk_update(
                self.to_update,
                [
                    'bank_import_method',
                    '_bank_date',
                    'bank_comment',
                    'bank_description',
                    'read',
                    'import_datetime'
                ]
            )

            self._affect_categories()

    def process(self):
        self.parse()
        self.keep_only_new_operations()
        self.match_with_pendings()
        self.save()

    @property
    def nb_new(self):
        return len(self.to_create) + len(self.to_update)

    @property
    def nb_pendings(self):
        return len(self.to_update)


class ImportWebBankOperations(ProcessImportBankOperations):
    def __init__(self, content: str, parser: str):
        ProcessImportBankOperations.__init__(self)

        parsers = {
            'cane2019': CANE2019BankWebParser,
            'cane2020default': CANE2020DefaultBankWebParser,
            'cane2020epargne': CANE2020EpargneBankWebParser,
            'cane2020longterme': CANE2020LongTermeBankWebParser,
            'cane2023epargne': CANE2023EpargneBankWebParser,
            'cane2024epargne': CANE2024EpargneBankWebParser,
        }

        if parser in parsers:
            self.parser: WebParser = parsers[parser](content)
        else:
            raise Exception(_("There is no parser for this bank."))

    def save(self):
        with transaction.atomic():
            ProcessImportBankOperations.save(self)
            self.account.save()

    def parse(self):
        self.parser.parse()

        try:
            self.account = Account.objects.get(number=self.parser.account_number)
        except Account.DoesNotExist:
            raise Exception(_("Account does not exist."))

        self.account.balance_date = self.parser.date
        self.account.balance = self.parser.balance
        self.operations = sorted(self.parser.operations, key=lambda e: e.bank_date)


class WebParser:
    def __init__(self, content: str):
        self.account_number: int = None
        self.balance: float = None
        self.date: datetime.date = None
        self.operations: List[Operation] = []

    @staticmethod
    def _convert_amount(amount_str: str) -> float:
        return float(
            amount_str.replace('\xa0', '')
                      .replace(' ', '')
                      .replace(',', '.')
                      .replace('\n', '')
                      .replace('\u202f', '')
                      .replace('€', '')
                      .strip()
        )

    def parse():
        raise Exception("Child classes must redefine this method.")


class CANE2020DefaultBankWebParser(WebParser):
    def __init__(self, content: str):
        super().__init__(content)
        self._soup = bs4.BeautifulSoup(content, "html.parser")

    def _find_number(self):
        number_element = self._soup.select('div.accountListItem-descriptionItem:nth-child(2)')
        self.account_number = int(number_element[0].contents[1][4:])

    def _find_balance_date(self):
        """
        Must be called after parsing operations

        Use first operation to find year (maybe wrong ?)
        """
        date_element = self._soup.select('div.OperationMainAccount-headerAmountTitle:nth-child(1)')
        date_elements = date_element[0].contents[0][9:].split('/')

        self.date = datetime.date(
            self.operations[0].bank_date.year,
            int(date_elements[1]),
            int(date_elements[0])
        )

    def _find_balance_value(self):
        balance_element = self._soup.select('.OperationMainAccount-headerAmountValue')
        self.balance = self._convert_amount(balance_element[0].contents[0])

    def _convert_icon_to_method(self, classes):
        if "npc-debit" in classes:
            return "Prélèvement"
        elif "npc-card" in classes:
            return "Paiement par carte"
        elif "npc-transfer" in classes:
            return "Virement"
        elif "npc-various" in classes:
            return "Autre"
        else:
            logger.warning("Unknown method '" + str(classes) + "'")
            return "Inconnue"

    def _remove_b_tag(self, content):
        if isinstance(content, list):
            if len(content) == 0:
                return ""
            else:
                return self._remove_b_tag(content[0])
        elif isinstance(content, str):
            return content
        else:
            if len(content.contents) == 0:
                return ""
            else:
                return content.contents[0]

    @staticmethod
    def _parse_date(date_txt) -> datetime.date:
        parsed_date = None
        for fmt in ['%b %d, %Y, %I:%M:%S %p', '%b %d, %Y %I:%M:%S %p']:
            try:
                parsed_date = datetime.datetime.strptime(date_txt, fmt).date()
                break
            except ValueError:
                pass

        if parsed_date is None:
            raise Exception(_("Could not convert date."))

        return parsed_date

    def _convert_line(self, line: bs4.element.Tag) -> Operation:
        operation = Operation()

        date = self._parse_date(line.select('#dateOperation')[0].attrs['aria-label'])
        operation.bank_date = date
        operation.date_paid = date

        operation.bank_description = \
            self._remove_b_tag(line.select(".Operation-name")[0].contents[0]).strip().capitalize()
        operation.amount = self._convert_amount(
            self._remove_b_tag(line.select('#montant')[0].contents[0])
        )

        if len(line.select('.Operation-type')[0].contents) > 0:
            operation.method = self._remove_b_tag(
                line.select('.Operation-type')[0].contents[0]
            ).strip().capitalize()
        if operation.method == "":
            # sometime operation method isn't well formatted, so we use operation icon to guess:
            operation.method = self._convert_icon_to_method(
                line.select('.Operation-icon')[0]['class']
            )

        for c in line.select('.Operation-descriptionLine'):
            if len(c.contents) > 0:
                operation.bank_comment += c.contents[0].strip() + "\n"

        if len(operation.bank_comment) > 0:
            operation.bank_comment = operation.bank_comment[:-1]  # remove the last \n

        assert (operation.bank_import_method == Operation.BankOperationImportMethod.WEB)

        return operation

    def _parse_operations(self):
        lines = self._soup.select('#bloc-operations li')

        for line in lines:
            self.operations.append(self._convert_line(line))

    def parse(self):
        self._find_number()
        self._find_balance_value()
        self._parse_operations()
        self._find_balance_date()


class CANE2020LongTermeBankWebParser(WebParser):
    def __init__(self, content: str):
        super().__init__(content)
        self._soup = bs4.BeautifulSoup(content, "html.parser")

    def _find_number(self):
        number_element = self._soup.select('div.gras')
        number_text = number_element[0].contents[0]
        self.account_number = int(number_text[number_text.index('°')+1:].strip())

    def _find_balance_date(self):
        date_element = self._soup.select('.col-xs-7')
        date_elements = date_element[0].contents[0].strip()[14:].strip()[3:].split('/')

        self.date = datetime.date(
            int(date_elements[2]),
            int(date_elements[1]),
            int(date_elements[0])
        )

    def _find_balance_value(self):
        balance_element = self._soup.select('.col-xs-5')
        self.balance = self._convert_amount(balance_element[0].contents[0])

    def _convert_line(self, line: bs4.element.Tag) -> Operation:
        operation = Operation()

        date_elements = line.select('.col-xs-3')[0].contents[1].contents[0].strip().split('/')
        date = datetime.date(
            int(date_elements[2]),
            int(date_elements[1]),
            int(date_elements[0])
        )
        operation.bank_date = date
        operation.date_paid = date

        description_elements = line.select('.col-xs-6')[0].contents[1].contents
        operation.method = description_elements[0].strip()
        operation.bank_description = operation.method
        operation.amount = self._convert_amount(
            line.select('.cell-amount span')[0].contents[0].strip()
        )
        operation.bank_comment = description_elements[2].strip()

        assert (operation.bank_import_method == Operation.BankOperationImportMethod.WEB)

        return operation

    def _parse_operations(self):
        lines = self._soup.select('.table-mother tr')

        for line in lines:
            self.operations.append(self._convert_line(line))

    def parse(self):
        self._find_number()
        self._find_balance_value()
        self._parse_operations()
        self._find_balance_date()


class CANE2020EpargneBankWebParser(WebParser):
    def __init__(self, content: str):
        super().__init__(content)
        self._soup = bs4.BeautifulSoup(content, "html.parser")

    def _find_number(self):
        number_element = self._soup.select('.gras')
        number_text = number_element[0].contents[0]
        self.account_number = int(number_text[number_text.index('°')+1:].strip())

    def _find_balance_date(self):
        date_element = self._soup.select(
            'table.table:nth-child(3) > tbody:nth-child(2) > tr:nth-child(1) '
            '> td:nth-child(1) > div:nth-child(2) > div:nth-child(1)'
        )
        date_elements = date_element[0].contents[0].strip()[9:-2].split('/')

        self.date = datetime.date(
            int(date_elements[2]),
            int(date_elements[1]),
            int(date_elements[0])
        )

    def _find_balance_value(self):
        balance_element = self._soup.select(
            'table.table:nth-child(3) > tbody:nth-child(2) '
            '> tr:nth-child(1) > td:nth-child(1) > div:nth-child(2) > div:nth-child(2)'
        )
        self.balance = self._convert_amount(balance_element[0].contents[0].strip())

    def _convert_line(self, line: bs4.element.Tag, previous_date: datetime.date) -> Operation:
        operation = Operation()

        date_elements = line.select('.col-xs-3')[0].contents[0].split('/')

        date = datetime.date(previous_date.year, int(date_elements[1]), int(date_elements[0]))
        if date.month > previous_date.month:
            date = datetime.date(date.year - 1, date.month, date.day)

        operation.bank_date = date
        operation.date_paid = date

        line_number = 0
        date_pattern = re.compile(r'\d{2}/\d{2}/\d{4}')
        for content in line.select('.col-xs-5')[0].contents[2:]:
            if isinstance(content, str) and date_pattern.match(content.strip()) is None:
                if line_number == 0:
                    operation.bank_description = content.strip()
                else:
                    operation.bank_comment += content.strip() + "\n"

                line_number += 1

        if line_number >= 2:
            operation.bank_comment = operation.bank_comment[:-1]  # remove the last \n

        operation.amount = self._convert_amount(line.select('.cell-amount')[0].contents[0].strip())
        operation.method = line.select('.col-xs-5')[0].contents[0].strip()

        assert (operation.bank_import_method == Operation.BankOperationImportMethod.WEB)

        return operation

    def _parse_operations(self):
        assert (self.date is not None)

        lines = self._soup.select('.table')[1].select('.col-xs-12')
        previous_date = self.date

        for line in lines:
            self.operations.append(self._convert_line(line, previous_date))
            previous_date = self.operations[-1].bank_date

    def parse(self):
        self._find_number()
        self._find_balance_value()
        self._find_balance_date()
        self._parse_operations()


class CANE2023EpargneBankWebParser(WebParser):
    def __init__(self, content: str):
        super().__init__(content)
        self._soup = bs4.BeautifulSoup(content, "html.parser")

    def _find_number(self):
        number_text = self._soup.select('.sub-title-account')[0].contents[0]
        self.account_number = int(number_text[number_text.index('°')+1:].strip())

    def _find_balance_date(self):
        date_elements = self._soup.select(
            ".collapse-date-effet"
        )[0].contents[0].strip()[9:].split('/')

        self.date = datetime.date(
            int(date_elements[2]),
            int(date_elements[1]),
            int(date_elements[0])
        )

    def _find_balance_value(self):
        self.balance = self._convert_amount(
            self._soup.select(".collapse-amount")[0].contents[0].strip()
        )

    @staticmethod
    def _find_operation_method_description(full_description: str) -> Tuple[str, str]:
        conversion_values = {
            "VIREMENT EN VOTRE FAVEUR": "Virement",
            "VIREMENT EMIS": "Virement",
            "INTERETS CREDITEURS": "Interets Crediteurs",
            "PRELEVEMENT": "Prélèvement",
        }

        for key in conversion_values:
            if full_description.startswith(key):
                return conversion_values[key], full_description[len(key)+1:]

        # Some types of accounts don't provide an operation method:
        return "Inconnue", full_description

    def _convert_line(self, line: bs4.element.Tag) -> Operation:
        operation = Operation()

        date_str = line.select(
            'msl-column:nth-child(1) > msl-flex:nth-child(1) > span:nth-child(1)'
        )[0].contents[0].strip()
        date = french2python_date(date_str)

        operation.bank_date = date
        operation.date_paid = date
        operation.method, operation.bank_description = self._find_operation_method_description(
            line.select(".libelle-opetation")[0].contents[0].strip()
        )
        operation.amount = self._convert_amount(line.select(
            'msl-column:nth-child(3) > msl-flex:nth-child(1) > span:nth-child(1)'
        )[0].contents[0].strip())

        comment_element = line.select("msl-column:nth-child(2) > div:nth-child(3)")
        if len(comment_element) > 0:
            operation.bank_comment = comment_element[0].contents[0].strip()

        assert (operation.bank_import_method == Operation.BankOperationImportMethod.WEB)

        return operation

    def _parse_operations(self):
        for line in self._soup.select("app-ui-operations-list"):
            self.operations.append(self._convert_line(line))

    def parse(self):
        self._find_number()
        self._find_balance_value()
        self._find_balance_date()
        self._parse_operations()


class CANE2024EpargneBankWebParser(CANE2023EpargneBankWebParser):
    def _find_number(self):
        number_text = self._soup.select('.msl-text-body-xl > span:nth-child(1)')[0].contents[0]
        self.account_number = int(number_text[number_text.index('°')+1:].strip())

    def _find_balance_date(self):
        date_elements = self._soup.select(
            "div.msl-justify-end:nth-child(2)"
        )[0].contents[0].strip()[9:].split('/')

        self.date = datetime.date(
            int(date_elements[2]),
            int(date_elements[1]),
            int(date_elements[0])
        )

    def _find_balance_value(self):
        self.balance = self._convert_amount(
            self._soup.select(
                "div.msl-justify-end:nth-child(1) > mds-amount:nth-child(1)"
            )[0].contents[0].strip()
        )

    def _convert_line(self, line: bs4.element.Tag, date: datetime.date) -> Operation:
        operation = Operation()

        operation.bank_date = date
        operation.date_paid = date
        operation.method, operation.bank_description = self._find_operation_method_description(
            line.select(
                "div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1)"
            )[0].contents[0].strip()
        )
        operation.amount = self._convert_amount(line.select('mds-amount')[0].contents[0].strip())

        comment_element = line.select(
            "div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2)"
        )
        if len(comment_element) > 0:
            operation.bank_comment = comment_element[0].contents[0].strip()

        assert (operation.bank_import_method == Operation.BankOperationImportMethod.WEB)

        return operation

    def _parse_operations(self):
        date = None
        date_regex = re.compile(r"\d{2} (" + "|".join(FRENCH_MONTHS) + r") \d{4}")

        for line in self._soup.select("app-home > div"):
            if len(line.contents) == 1 and line.contents[0].name is None:
                # Is it a date line ?
                date_attempt = line.contents[0].strip()
                if date_regex.match(date_attempt):
                    date = french2python_date(date_attempt)
            else:
                # Is it an operation line ?
                operation_attempt = line.find("app-operation")
                if operation_attempt is not None:
                    assert (date is not None)
                    self.operations.append(self._convert_line(operation_attempt, date))


class CANE2019BankWebParser(WebParser):
    def __init__(self, content: str):
        super().__init__(content)
        self._soup = bs4.BeautifulSoup(content, "html.parser")
        self._type: AccountType = None

    def _find_number_and_type_account(self):
        number_element = self._soup.select(
                'table.ca-table:nth-child(2) > tbody > tr.ligne-impaire > th'
        )

        if len(number_element) == 0:
            number_element = self._soup.select('th.cel-texte')

            if number_element == 0:
                raise Exception(_("Could not find account number."))

            number_content = number_element[0].contents[4].strip()
            self._type = AccountType.EPARGNE_LONG_TERME
        elif len(number_element) == 1:
            number_content = number_element[0].contents[0].strip()
            if number_content.find("n°") == -1:
                number_content = number_element[0].contents[4].strip()
                self._type = AccountType.EPARGNE
            else:
                self._type = AccountType.DEFAULT
        else:
            raise Exception(_("Could not find account number."))

        number_position = number_content.find('n°')
        if number_position == -1:
            raise Exception(_("Wrong account number format."))

        self.account_number = int(number_content[number_position+2:])
        assert (self._type is not None)

    def _find_balance_date(self):
        date_element = []
        if self._type == AccountType.DEFAULT:
            date_element = self._soup.select("td.cel-texte:nth-child(2)")
        elif self._type == AccountType.EPARGNE_LONG_TERME or self._type == AccountType.EPARGNE:
            date_element = self._soup.select("tr.ligne-impaire:nth-child(1) > td:nth-child(2)")

        if len(date_element) != 1:
            raise Exception(_("Could not find date."))

        date_values = []
        if self._type == AccountType.DEFAULT or self._type == AccountType.EPARGNE:
            date_values = date_element[0].contents[0].strip()[9:-2].split("/")
        elif self._type == AccountType.EPARGNE_LONG_TERME:
            date_values = date_element[0].contents[4].strip()[3:-2].split("/")

        date_list = [int(e) for e in date_values]
        self.date = datetime.date(date_list[2], date_list[1], date_list[0])

    def _find_balance_value(self):
        balance_element = []
        if self._type == AccountType.DEFAULT:
            balance_element = self._soup.select("td.cel-num:nth-child(3)")
        elif self._type == AccountType.EPARGNE_LONG_TERME or self._type == AccountType.EPARGNE:
            balance_element = self._soup.select("tr.ligne-impaire:nth-child(1) > td:nth-child(3)")

        if len(balance_element) != 1:
            raise Exception(_("Could not find balance."))

        self.balance = self._convert_amount(balance_element[0].contents[0])

    def _convert_line(self, line: bs4.element.Tag, previous_date: datetime.date) -> Operation:
        cells = line.select('td')

        if self._type == AccountType.DEFAULT or self._type == AccountType.EPARGNE:
            if self._type == AccountType.DEFAULT:
                date_items = cells[0].contents[0].strip().split("/")
            elif self._type == AccountType.EPARGNE:
                date_items = cells[0].contents[1].contents[0].strip().split("/")

            date = datetime.date(previous_date.year, int(date_items[1]), int(date_items[0]))

            if date.month > previous_date.month:
                date = datetime.date(date.year - 1, date.month, date.day)
        elif self._type == AccountType.EPARGNE_LONG_TERME:
            date_items = cells[0].contents[1].contents[0].strip().split("/")
            date = datetime.date(int(date_items[2]), int(date_items[1]), int(date_items[0]))

        comment = ""
        if self._type == AccountType.DEFAULT:
            comment_cells = cells[2].contents[3].contents
        elif self._type == AccountType.EPARGNE_LONG_TERME or self._type == AccountType.EPARGNE:
            comment_cells = cells[1].contents[1].contents[2:]

        for c in comment_cells:
            if type(c) is bs4.element.NavigableString:
                comment += c.strip()
            elif type(c) is bs4.element.Tag and c.name == "br":
                comment += '\n'

        operation = Operation()

        if self._type == AccountType.DEFAULT:
            operation.bank_description = cells[2].contents[2].strip()
            operation.amount = self._convert_amount(cells[4].contents[0])
            operation.method = cells[2].contents[0].strip()
            operation.bank_comment = comment
        elif self._type == AccountType.EPARGNE_LONG_TERME:
            operation.bank_description = cells[1].contents[1].contents[0].strip()
            operation.amount = self._convert_amount(cells[2].contents[1].contents[0])
            operation.method = "Virement"
            operation.bank_comment = comment
        elif self._type == AccountType.EPARGNE:
            operation.method = cells[1].contents[1].contents[0].strip()
            operation.amount = self._convert_amount(cells[2].contents[1].contents[0])
            operation.bank_description = comment.split('\n')[0]
            operation.bank_comment = '\n'.join(comment.split('\n')[1:])

        operation.bank_date = date
        operation.date_paid = date
        # operation.account is set just before saving operation, by ProcessImportBankOperations

        assert (operation.bank_import_method == Operation.BankOperationImportMethod.WEB)

        return operation

    def _convert_lines(self, lines: List[bs4.element.Tag], date: datetime.datetime):
        assert (date <= datetime.date.today())

        line_number = 2

        for line in lines:
            try:
                self.operations.append(self._convert_line(line, date))
            except Exception as e:
                raise Exception("Line %d: %s" % (line_number, str(e)))

            date = self.operations[-1].bank_date
            line_number += 2

    def _parse_operations(self):
        tables = []
        if self._type == AccountType.DEFAULT:
            tables = self._soup.select('table.ca-table:nth-child(4) > tbody:nth-child(3)')
        elif self._type == AccountType.EPARGNE_LONG_TERME or self._type == AccountType.EPARGNE:
            tables = self._soup.select('table.ca-table:nth-child(3) > tbody:nth-child(2)')

        if len(tables) != 1:
            raise Exception(_("There number of matching tables is different than expected."))

        self._convert_lines(tables[0].select('tr'), self.date)

    def parse(self):
        self._find_number_and_type_account()
        self._find_balance_date()
        self._find_balance_value()
        self._parse_operations()


class ImportCsvOperations:
    def __init__(self, filename: str, account: Account):
        self.filename = filename
        self.account: Account = account
        self.operations: List[(Operation, str)] = []

    def _parse_line(self, row: List[str]) -> (Operation, str):
        date = "-".join(reversed(row[0].split('/')))
        method = row[1]
        amount = float(row[3].replace('€', '').replace(',', '.'))
        description = row[2]

        if len(row) >= 5:
            comment = row[4]
        else:
            comment = ""

        if len(row) >= 6:
            categories = row[5]
        else:
            categories = ""

        return Operation(
            date_paid=date,
            method=method,
            amount=amount,
            description=description,
            comment=comment,
            account=self.account
        ), categories

    def _parse_operations(self):
        line_number = 2
        with open(self.filename, 'r') as csvFile:
            csv_file_reader = csv.reader(csvFile)
            next(csv_file_reader)
            for row in csv_file_reader:
                if len("".join(row)) != 0:
                    try:
                        self.operations.append(self._parse_line(row))
                    except Exception as e:
                        raise Exception("Line %d: %s" % (line_number, str(e)))

                line_number += 1

    def _affect_categories(self):
        categories = Category.objects.filter(~Q(auto_affect=""))

        for o, c in self.operations:
            auto_affect_categories.affect_categories(o, categories, c)
            o.save()

    def _save(self):
        for o, c in self.operations:
            o.save()

    def process(self):
        self._parse_operations()
        self._save()
        self._affect_categories()


class ImportCsvBankOperations(ProcessImportBankOperations):
    def __init__(self, filename: str, account: Account):
        ProcessImportBankOperations.__init__(self)
        self.filename = filename
        self.account = account

    def _parse_line(self, row: List[str]) -> Operation:
        date = "-".join(reversed(row[0].split('/')))
        method = row[1]
        amount = float(row[4].replace('\xa0', '').replace('€', '').replace(',', '.'))
        description = row[2]
        comment = row[3]

        if len(method) == 0:
            raise Exception(_("Method field is empty."))

        return Operation(
            date_paid=date,
            bank_date=date,
            method=method,
            amount=amount,
            bank_description=description,
            bank_comment=comment,
            account=self.account,
            bank_import_method=Operation.BankOperationImportMethod.CSV
        )

    def parse(self):
        line_number = 2

        with open(self.filename, 'r') as csvFile:
            csv_file_reader = csv.reader(csvFile)
            next(csv_file_reader)
            for row in csv_file_reader:
                if any([e != '' for e in row]):
                    try:
                        self.operations.append(self._parse_line(row))
                    except Exception as e:
                        raise Exception("Line %d: %s" % (line_number, str(e)))
                line_number += 1
