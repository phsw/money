import datetime
from typing import List
from django.db.models import Min, Sum, Q
from django.db.models.functions import TruncMonth, TruncYear

from money.models.accounts import Account
from money.models.operations import Operation
from money.models.categories import Category


def get_account_monthly_balances(account: Account, date_to: datetime.date):
    cumsum = 0.
    operation_filter = ~Q(_bank_date=None) & Q(_bank_date__lt=date_to) & Q(takes_from_support=False)
    account_filter = Q(initial_amount__gt=0.) | ~Q(support_amount=None)

    if account is not None:
        operation_filter &= Q(account=account)
        account_filter &= Q(pk=account.pk)

    initial_amounts = list(Account.objects.filter(account_filter).annotate(
        start_month=TruncMonth(Min('operation___bank_date'))
    ).values(
        'pk', 'number', 'start_month', 'initial_amount', 'support_amount'
    ).order_by('start_month'))

    result = Operation.objects.filter(operation_filter).annotate(
        month=TruncMonth('_bank_date')
    ).values('month').order_by('month').annotate(balance=Sum('_amount'))

    response = []
    max_balance = 0.

    for r in result:
        if len(initial_amounts) > 0:
            to_delete = []

            for i, a in enumerate(initial_amounts):
                if a['start_month'] == r['month']:
                    cumsum += round(a['initial_amount'], 2)
                    if a['support_amount'] is not None:
                        r['balance'] += round(a['support_amount'], 2)

                    to_delete.append(i)

            for i in reversed(to_delete):
                del initial_amounts[i]

        cumsum += round(r['balance'], 2)
        response.append({
            'period': r['month'].strftime("%B %Y"),
            'balance': round(r['balance'], 2),
            'global_balance': round(cumsum, 2)
        })
        max_balance = max(max_balance, cumsum, r['balance'])

    display_balance_limit = (
        (account is not None) and
        (account.balance_limit is not None) and
        (max_balance > 0.5 * account.balance_limit)
    )

    return response, display_balance_limit


def get_all_accounts_monthly_balances(date_to: datetime.date):
    result, _ = get_account_monthly_balances(None, date_to)

    return result


def get_all_accounts_yearly_balances(date_to: datetime.date):
    result = Operation.objects.filter(
        ~Q(_bank_date=None) & Q(_bank_date__lt=date_to) & Q(takes_from_support=False)
    ).annotate(
        year=TruncYear('_bank_date')
    ).values('year').order_by('year').annotate(balance=Sum('_amount'))

    initial_amounts = list(Account.objects.filter(
        Q(initial_amount__gt=0.) | ~Q(support_amount=None)
    ).annotate(
        start_year=TruncYear(Min('operation___bank_date'))
    ).values(
        'pk', 'number', 'start_year', 'initial_amount', 'support_amount'
    ).order_by('start_year'))

    response = []
    cumsum = 0.

    for r in result:
        if len(initial_amounts) > 0:
            to_delete = []

            for i, a in enumerate(initial_amounts):
                if a['start_year'] == r['year']:
                    cumsum += round(a['initial_amount'], 2)
                    if a['support_amount'] is not None:
                        r['balance'] += round(a['support_amount'], 2)

                    to_delete.append(i)

            for i in reversed(to_delete):
                del initial_amounts[i]

        cumsum += round(r['balance'], 2)
        response.append({
            'period': r['year'].year,
            'balance': round(r['balance'], 2),
            'global_balance': round(cumsum, 2)
        })

    return response


def _get_account_categories_abstract_repartition(operations: List[Operation]):
    repartition = []

    for o in operations:
        categories = [c.id for c in o.all_categories]
        found = False
        for r in repartition:
            if categories == r['ids']:
                r['balance'] += o.amount
                found = True
                break

        if not found:
            title = "/".join([str(c.title) for c in o.all_categories])
            assert (len(categories) > 0)

            repartition.append({
                'ids': categories,  # only used as a hash of categories, just above
                'title': title,
                'categories': [{
                    'id': categories[i],
                    'title': o.all_categories[i].title
                } for i in range(len(categories))],
                'balance': round(o.amount, 2)
            })

    return repartition


def get_account_categories_expenses_repartition(
        account: Account, date_from: datetime.date, date_to: datetime.date
):
    operations = Operation.objects.filter(
        Q(account=account)
        & Q(_bank_date__gte=date_from)
        & Q(_bank_date__lt=date_to)
        & Q(_amount__lt=0)
    ).prefetch_related('categories')

    return _get_account_categories_abstract_repartition(operations)


def get_account_categories_incomes_repartition(
        account: Account, date_from: datetime.date, date_to: datetime.date
):
    operations = Operation.objects.filter(
        Q(account=account)
        & Q(_bank_date__gte=date_from)
        & Q(_bank_date__lt=date_to)
        & Q(_amount__gt=0)
    ).prefetch_related('categories')

    return _get_account_categories_abstract_repartition(operations)


def get_account_categories_repartition(account: Account):
    operations = Operation.objects.filter(
        Q(account=account) & ~Q(_bank_date=None)
    ).prefetch_related('categories')

    repartition = _get_account_categories_abstract_repartition(operations)

    return [r for r in repartition if round(r['balance'], 2) != 0]


def get_operation_category_filter(category):
    q_filter = ~Q(_bank_date=None)
    if category == Category.without_category():
        q_filter &= Q(categories=None)
    else:
        q_filter &= Q(categories=category)

    return q_filter


def get_category_monthly_balance(category: Category):
    result = Operation.objects.filter(get_operation_category_filter(category)).annotate(
        month=TruncMonth('_bank_date')
    ).values('month').order_by('month').annotate(balance=Sum('_amount'))

    response = []

    for r in result:
        response.append({
            'month': r['month'].strftime("%B %Y"),
            'balance': round(r['balance'], 2),
        })

    return response


def get_category_repartition(category: Category):
    results = Operation.objects.filter(
        get_operation_category_filter(category)
    ).values(
        'account', 'account__title', 'account__balance'
    ).annotate(balance=Sum('_amount')).filter(~Q(balance=0))

    return [r for r in results if round(r['balance'], 2) != 0]
