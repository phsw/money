import datetime
from django.db.models import Q
from django.utils.translation import gettext_lazy as _

from money.models.accounts import Account
from money.models.operations import Operation


def _get_account_categories_repartition(filters, get_period, period_label: str):
    operations = Operation.objects.filter(
        filters
    ).order_by('_bank_date').prefetch_related('categories')

    # period can be month or year
    period_indexes = dict()
    i_period = 0
    category_indexes = dict()
    i_category = 0
    for o in operations:
        period = get_period(o.bank_date)

        if period not in period_indexes:
            period_indexes[period] = i_period
            i_period += 1

        categories = [c.id for c in o.all_categories]
        categories.sort()

        if str(categories) not in category_indexes:
            category_indexes[str(categories)] = {
                "i": i_category,
                "categories": o.all_categories
            }
            i_category += 1

    nb_period_indexes = len(period_indexes)

    for c in category_indexes:
        category_indexes[c]["amounts"] = [0.] * (nb_period_indexes+1)

    for o in operations:
        categories = [c.id for c in o.all_categories]
        categories.sort()

        category_indexes[str(categories)]["amounts"][
            period_indexes[get_period(o.bank_date)]
        ] += o.amount

    sums = [0.] * (nb_period_indexes+1)
    sums_pos = [0.] * (nb_period_indexes+1)
    sums_neg = [0.] * (nb_period_indexes+1)
    for c in category_indexes:
        for i in range(nb_period_indexes):
            amount = category_indexes[c]["amounts"][i]
            sums[i] += amount
            if amount > 0:
                sums_pos[i] += amount
            else:
                sums_neg[i] += amount

    if nb_period_indexes > 0:
        sums[-1] = sum(sums[:-1]) / nb_period_indexes
        sums_pos[-1] = sum(sums_pos[:-1]) / nb_period_indexes
        sums_neg[-1] = sum(sums_neg[:-1]) / nb_period_indexes

    sum_check = 0
    for c in category_indexes:
        category_indexes[c]["amounts"][-1] = sum(
            category_indexes[c]["amounts"]
        ) / nb_period_indexes
        sum_check += category_indexes[c]["amounts"][-1]

    assert (abs(sum_check - sums[-1]) < 1e-6)

    to_delete = []
    # Round every amount to avoid displaying 0.00:
    for c in category_indexes:
        only_zeros = True
        for i in range(nb_period_indexes+1):
            category_indexes[c]["amounts"][i] = round(category_indexes[c]["amounts"][i], 2)
            if category_indexes[c]["amounts"][i] != 0:
                only_zeros = False
        if only_zeros:
            to_delete.append(c)

    # delete empty lines:
    for c in to_delete:
        del category_indexes[c]

    return {
        'periods': list(period_indexes.keys()) + [_("Average by " + period_label)],
        'lines': category_indexes.values(),
        'sums': sums,
        'sums_pos': sums_pos,
        'sums_neg': sums_neg
    }


def get_categories_repartition_by_months(account: Account, date_to: datetime.date):
    if date_to.month == 1:
        date_from = datetime.date(date_to.year-2, 12, 1)
    else:
        date_from = datetime.date(date_to.year-1, date_to.month-1, 1)

    filters = ~Q(_bank_date=None) & Q(_bank_date__gte=date_from) & Q(_bank_date__lt=date_to)
    if account is not None:
        filters &= Q(account=account)

    return _get_account_categories_repartition(filters, lambda d: d.strftime("%B %Y"), "month")


def get_categories_repartition_by_years(account: Account, year_to: int):
    date_to = datetime.date(year_to, 1, 1)

    filters = ~Q(_bank_date=None) & Q(_bank_date__lt=date_to)
    if account is not None:
        filters &= Q(account=account)

    return _get_account_categories_repartition(filters, lambda d: d.year, "year")
