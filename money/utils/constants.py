forms_design = {
    "form_class": "form-horizontal",
    "label_class": "col-lg-2",
    "field_class": "col-lg-8",
    "datepicker": "datepicker",
    "date_field_css": "col-lg-2 datepicker",
    "small_field_css": "col-lg-2",
    "submit_button_css": "btn-default offset-lg-2",
    "second_submit_button_css": "btn-secondary"
}

# number of available items before and after the current index:
paginator_padding = 4
