from typing import List

from money.models.operations import Operation
from money.models.categories import Category


def get_matching_categories(description: str, categories: List[Category]) -> List[Category]:
    matching: List[Category] = []
    desc = description.lower()

    for c in categories:
        for a in c.auto_affects:
            if a.lower() in desc and c not in matching:
                matching.append(c)

    return matching


def affect_categories(operation: Operation, categories: List[Category], sup_field: str = ""):
    bank_description_lower = operation.bank_description.lower()
    description_lower = operation.description.lower()
    sup_field_lower = sup_field.lower()

    for c in categories:
        for a in c.auto_affects:
            a_lower = a.lower()
            if ((a_lower in bank_description_lower
                    or a_lower in description_lower
                    or a_lower in sup_field_lower)
                    and c not in operation.categories.all()):
                operation.categories.add(c)
