from django import forms
from django.utils.translation import gettext_lazy as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Field
from crispy_forms.bootstrap import AppendedText

from money.models.accounts import Account
from money.utils import constants


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = [
            'title',
            'number',
            'no_pendings',
            'balance_limit',
            'comment',
            'initial_amount',
            'support_amount',
            'bank_local_balances_can_be_different',
        ]
        labels = {
            'no_pendings': _("No pending operations"),
            'bank_local_balances_can_be_different': _(
                "Global account balance can be different than the sum of operations"
            ),
            'support_amount': _("Support amount"),
        }

    def __init__(self, *args, **kwargs):
        def amount_field(name: str):
            return AppendedText(
                name,
                '€',
                css_class=constants.forms_design['small_field_css']
            )

        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_class = constants.forms_design['form_class']
        self.helper.label_class = constants.forms_design['label_class']
        self.helper.field_class = constants.forms_design['field_class']
        self.helper.layout = Layout(
            'title',
            'number',
            amount_field('balance_limit'),
            amount_field('initial_amount'),
            Field('comment', rows='4'),
            'no_pendings',
            'bank_local_balances_can_be_different',
            amount_field('support_amount'),
            Submit('Save', 'Save', css_class=constants.forms_design['submit_button_css']),
        )

        if len(self.initial) == 0:  # New account
            self.fields['csvBankOperations'] = forms.FileField(
                required=False,
                label=_('CSV file of bank operations')
            )
            self.helper.layout.insert(3, 'csvBankOperations')

            self.fields['csvOperations'] = forms.FileField(
                required=False,
                label=_('CSV file of operations')
            )
            self.helper.layout.insert(4, 'csvOperations')
