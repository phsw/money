from django import forms
from django.utils.translation import gettext_lazy as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Field
from crispy_forms.bootstrap import AppendedText

from money.models.operation_templates import OperationTemplate
from money.models.accounts import Account
from money.models.categories import Category
from money.utils import constants


class OperationTemplateForm(forms.ModelForm):
    class Meta:
        model = OperationTemplate
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['categories'].queryset = Category.objects.all().order_by('title')
        self.fields['accounts'].queryset = Account.objects.all().order_by('title')
        self.fields['accounts'].initial = Account.objects.all().order_by('title')

        self.helper = FormHelper()

        self.helper.form_class = constants.forms_design['form_class']
        self.helper.label_class = constants.forms_design['label_class']
        self.helper.field_class = constants.forms_design['field_class']
        self.helper.layout = Layout(
            'title',
            'method',
            AppendedText('amount', '€', css_class=constants.forms_design['small_field_css']),
            'description',
            Field('comment', rows='4'),
            'accounts',
            'categories',
            Submit('Save', 'Save', css_class=constants.forms_design['submit_button_css']),
        )

    def clean(self):
        cleaned_data = super().clean()

        if (
                cleaned_data.get("method") is None
                and cleaned_data.get("amount") is None
                and cleaned_data.get("description") is None
                and cleaned_data.get("comment") == ""
           ):
            raise forms.ValidationError(_("You must fill at least one field."))
