from django import forms
from django.utils.translation import gettext_lazy as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Field

from money.models.categories import Category
from money.utils import constants


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = '__all__'
        labels = {
            'auto_affect': _("Auto affect to operations with description containing")
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()

        self.helper.form_class = constants.forms_design['form_class']
        self.helper.label_class = constants.forms_design['label_class']
        self.helper.field_class = constants.forms_design['field_class']
        self.helper.layout = Layout(
            'title',
            Field('auto_affect', placeholder=_("Separate values with commas")),
            Submit('Save', 'Save', css_class=constants.forms_design['submit_button_css']),
        )

    def clean_title(self):
        data = self.cleaned_data['title']

        if data.lower() == Category.without_category().title.lower():
            raise forms.ValidationError(_("You cannot use such category title."))

        return data
