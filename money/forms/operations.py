import datetime

from django import forms
from django.utils.translation import gettext_lazy as _
from django.db.models import Q
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Field, HTML, Div
from crispy_forms.bootstrap import AppendedText

from money.models.operations import Operation, OperationReceipt
from money.models.accounts import Account
from money.models.categories import Category
from money.utils import constants


class OperationForm(forms.ModelForm):
    class Meta:
        model = Operation
        fields = '__all__'
        labels = {
            '_amount': _("Amount"),
            '_date_paid': _("Paid date"),
            '_bank_date': _("Bank date")
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        del self.fields['account']
        del self.fields['bank_import_method']
        del self.fields['import_datetime']

        self.fields['all_to_all'] = forms.BooleanField(
            initial=False,
            required=False,
            label=_('All-to-all linked operations')
        )
        self.fields['categories'].queryset = Category.objects.all().order_by('title')

        self.helper = FormHelper()
        self.helper.form_tag = False

        self.helper.form_class = constants.forms_design['form_class']
        self.helper.label_class = constants.forms_design['label_class']
        self.helper.field_class = constants.forms_design['field_class']
        self.helper.layout = Layout(
            Field('method'),
            AppendedText('_amount', '€', css_class=constants.forms_design['small_field_css']),
            Field('description'),
            Field('comment', rows='4'),
            Field('categories'),
            Field('linked_operations'),
            Field('all_to_all')
        )

        linked_operations_query = None

        if self.instance.pk is None:  # New pending operation
            linked_operations_query = Operation.objects
            del self.fields['_date_paid']
        else:  # Edit an operation
            linked_operations_query = Operation.objects.filter(~Q(pk=self.instance.pk))
            self.helper.layout.insert(
                0,
                Field('_date_paid', css_class=constants.forms_design['date_field_css'])
            )

        # we order by date_paid to always have a valid date:
        linked_operations_query = linked_operations_query.order_by("-_date_paid")
        pending_operations = [
            [o.id, str(o)] for o in linked_operations_query.filter(_bank_date=None)
                                                           .prefetch_related('account')
        ]
        acted_operations = [
            [o.id, str(o)] for o in linked_operations_query.filter(~Q(_bank_date=None))
                                                           .prefetch_related('account')
        ]
        self.fields['linked_operations'].choices = [
            [_("Pending operations"), pending_operations],
            [_("Acted operations"), acted_operations],
        ]

        if not self.instance.is_bank_operation:  # Pending operation
            del self.fields['_bank_date']
            del self.fields['bank_description']
            del self.fields['bank_comment']
            del self.fields['read']
            del self.fields['takes_from_support']
        else:  # Bank operation (merged or not with a pending one)
            assert (self.instance.pk is not None)  # We can only *edit* a bank operation

            self.fields['_amount'].disabled = True
            self.fields['method'].disabled = True
            self.fields['_bank_date'].disabled = True
            self.fields['bank_description'].disabled = True
            self.fields['bank_comment'].disabled = True
            self.fields['description'].required = False

            self.helper.layout.insert(
                1,
                Field('_bank_date', css_class=constants.forms_design['date_field_css'])
            )
            self.helper.layout.insert(5, Field('bank_description'))
            self.helper.layout.insert(7, Field('bank_comment', rows='4'))
            self.helper.layout.insert(8, Field('read'))

            if self.instance.account.support_amount is None:
                del self.fields['takes_from_support']
            else:
                self.helper.layout.append("takes_from_support")

        # Put focus on the first empty field:
        for f in self.helper.layout.fields:
            value = self[f[0]].value()
            if value is None or value == "":
                self.helper[f[0]].update_attributes(autofocus=True)
                break


class OperationPaidDateForm(forms.Form):
    date_paid = forms.DateField(
        required=True,
        label=_("Paid date"),
        initial=datetime.date.today
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_show_labels = False

        self.helper.layout = Layout(Div(
            Field(
                'date_paid',
                wrapper_class='col-2',
                css_class=constants.forms_design["datepicker"]
            ),
            Div(css_class="dates-formset-delete-button align-self-center form-group"),
            css_class="form-row dates-formset-row"
        ))


OperationPaidDateFormSet = forms.formset_factory(OperationPaidDateForm, extra=0, min_num=1)


class OperationReceiptForm(forms.ModelForm):
    class Meta:
        model = OperationReceipt
        fields = ['kind', 'receipt_file']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if 'instance' in kwargs:
            self.fields['receipt_file'].widget.entity = kwargs['instance']

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_show_labels = False

        self.helper.layout = Layout(Div(
            Field('receipt_file', wrapper_class='col-7'),
            HTML('<label class="col-form-label">Kind</label>'),
            Field('kind', wrapper_class='col-1'),
            Div(css_class="receipts-formset-delete-button"),
            'DELETE',
            css_class="form-row receipts-formset-row"
        ))

        self.fields['kind'].required = False
        self.fields['receipt_file'].required = False


"""
Some resources for formsets:
* https://medium.com/@adandan01/django-inline-formsets-example-mybook-420cc4b6225d
* https://github.com/elo80ka/django-dynamic-formset/
"""
OperationReceiptFormSet = forms.inlineformset_factory(
    Operation,
    OperationReceipt,
    form=OperationReceiptForm,
    extra=1,
    can_delete=True
)


class BankOperationSetPendingForm(forms.Form):
    class MyModelChoiceField(forms.ModelChoiceField):
        def label_from_instance(self, obj):
            string = str(obj.date_paid) + " - "

            if obj.method != "":
                string += obj.method + " - "

            string += obj.description
            string += " | " + format(obj.amount, '.2f') + "€"

            return string

    pending_operation = MyModelChoiceField(queryset=None, empty_label=None, label=_("Operation"))

    def __init__(self, operation, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['pending_operation'].queryset = Operation.objects.filter(
            account=operation.account,
            _amount=operation.amount,
            _bank_date=None
        ).select_related('account')

        self.helper = FormHelper()

        self.helper.form_class = constants.forms_design['form_class']
        self.helper.label_class = constants.forms_design['label_class']
        self.helper.field_class = constants.forms_design['field_class']
        self.helper.layout = Layout(
            Field('pending_operation'),
            Submit('save', _('Save'), css_class=constants.forms_design['submit_button_css']),
        )


class OperationSearchForm(forms.Form):
    account = forms.ModelMultipleChoiceField(
        queryset=Account.objects.all().order_by('title'),
        initial=Account.objects.all()
    )
    categories = forms.ModelMultipleChoiceField(
        queryset=Category.objects.all().order_by('title'),
        required=False
    )
    has_all_categories = forms.BooleanField(
        initial=False,
        required=False,
        label=_("Operations must have all selected categories")
    )
    has_only_these_categories = forms.BooleanField(
        initial=False,
        required=False,
        label=_("Operations must have only selected categories")
    )
    amount = forms.FloatField(required=False)
    description = forms.CharField(max_length=255, required=False)
    select_pendings = forms.BooleanField(
        initial=True,
        required=False,
        label=_('Pending operations')
    )
    select_bank_operations = forms.BooleanField(
        initial=True,
        required=False,
        label=_('Bank operations')
    )
    date_start = forms.DateField(
        required=False,
        label=_("Paid after")
    )
    date_end = forms.DateField(
        required=False,
        label=_("Paid before")
    )
    read = forms.BooleanField(
        initial=True,
        required=False,
        label=_("Read operations")
    )
    unread = forms.BooleanField(
        initial=True,
        required=False,
        label=_("Not read operations")
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()

        self.helper.form_class = constants.forms_design['form_class']
        self.helper.label_class = constants.forms_design['label_class']
        self.helper.field_class = constants.forms_design['field_class']
        self.helper.layout = Layout(
            Field('account'),
            Field('amount'),
            Field('description'),
            Field('select_pendings'),
            Field('select_bank_operations'),
            Field('categories'),
            Field('has_all_categories'),
            Field('has_only_these_categories'),
            Field('date_start', css_class=constants.forms_design['date_field_css']),
            Field('date_end', css_class=constants.forms_design['date_field_css']),
            Field('read'),
            Field('unread'),
            Submit('search', _('Search'), css_class=constants.forms_design['submit_button_css']),
            Submit(
                'csv_export',
                _('Export to CSV'),
                css_class=constants.forms_design['second_submit_button_css']
            )
        )
