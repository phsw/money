import datetime
from django.test import TestCase

from money.forms.operations import OperationForm
from money.models.operations import Operation
from money.models.operation_templates import OperationTemplate
from money.models.accounts import Account
from money.models.categories import Category
from money.utils import operations as utils


class OperationTest(TestCase):
    def test_is_same_as(self):
        o1 = Operation(id=1, bank_date='2018-02-22', bank_description='Stuff', amount=14.18)

        # all same:
        o2 = Operation(id=2, bank_date='2018-02-22', bank_description='Stuff', amount=14.18)
        self.assertTrue(o1.is_same_as(o2))

        # bank_date different
        o2 = Operation(id=2, bank_date='2018-02-23', bank_description='Stuff', amount=14.18)
        self.assertFalse(o1.is_same_as(o2))

        # amount different
        o2 = Operation(id=2, bank_date='2018-02-22', bank_description='Stuff', amount=14.17)
        self.assertFalse(o1.is_same_as(o2))

        o1 = Operation(amount=14)
        o2 = Operation(amount=14.)
        self.assertTrue(o1.is_same_as(o2))

        o1 = Operation(bank_date='2018-02-15')
        o2 = Operation(bank_date=datetime.date(2018, 2, 15))
        self.assertTrue(o1.is_same_as(o2))

        o1 = Operation(bank_date='2018-02-15')
        o2 = Operation(bank_date=datetime.date(2018, 2, 15))
        self.assertTrue(o1.is_same_as(o2))

        self.assertFalse(o1.is_same_as("other class"))

        o1 = Operation(
                id=1,
                date_paid='2018-02-22',
                bank_description='Stuff',
                amount=14.18,
                bank_import_method=Operation.BankOperationImportMethod.CSV
        )

        # bank_description different
        o2 = Operation(
                id=2,
                date_paid='2018-02-22',
                bank_description='Stuf',
                amount=14.18,
                bank_import_method=Operation.BankOperationImportMethod.CSV
        )
        self.assertFalse(o1.is_same_as(o2))
        o2 = Operation(
                id=2,
                date_paid='2018-02-22',
                bank_description='Stuf',
                amount=14.18,
                bank_import_method=Operation.BankOperationImportMethod.WEB
        )
        self.assertTrue(o1.is_same_as(o2))

        o1 = Operation(
                id=1,
                date_paid='2018-02-22',
                bank_description='Stuff',
                amount=14.18,
                bank_import_method=Operation.BankOperationImportMethod.WEB
        )
        o2 = Operation(
                id=2,
                date_paid='2018-02-22',
                bank_description='Stuf',
                amount=14.18,
                bank_import_method=Operation.BankOperationImportMethod.WEB
        )
        self.assertFalse(o1.is_same_as(o2))

    def test_to_str(self):
        account = Account(title="DummyAccount")

        o = Operation(
                bank_date='2018-02-23',
                date_paid='2018-02-23',
                description='Stuff B',
                amount=15,
                account=account,
                method="Virement"
            )

        self.assertEqual(str(o), "2018-02-23: DummyAccount - Virement - Stuff B | 15.00€")

        o = Operation(
                bank_date='2018-02-23',
                date_paid='2018-02-23',
                description='Stuff B',
                amount=15,
                method="Virement"
            )

        self.assertEqual(str(o), "2018-02-23: Virement - Stuff B | 15.00€")

        o = Operation(
                bank_date='2018-02-23',
                date_paid='2018-02-23',
                description='Stuff B',
                amount=15
            )

        self.assertEqual(str(o), "2018-02-23: Stuff B | 15.00€")

        o = Operation()
        self.assertEqual(str(o), "Empty Operation")

    def test_toCsvRow(self):
        account = Account(title="DummyAccount", number=12)
        account.save()

        c0 = Category(title="c0")
        c0.save()
        c1 = Category(title="c1")
        c1.save()

        o = Operation(
            bank_date="2018-02-03",
            date_paid="2018-02-01",
            amount=-12.24,
            description="Description",
            bank_description="Bank description",
            comment="Comment",
            bank_comment="Bank comment\nwith many\nnew lines",
            method="Virement",
            account=account
        )
        o.save()

        o.categories.set([c0, c1])
        o.save()

        csvRow = o.toCsvRow()
        self.assertEqual(csvRow[0], "03/02/2018")
        self.assertEqual(csvRow[1], "01/02/2018")
        self.assertEqual(csvRow[2], "Virement")
        self.assertEqual(csvRow[3], "Bank description")
        self.assertEqual(csvRow[4], "Bank comment\nwith many\nnew lines")
        self.assertEqual(csvRow[5], "Description")
        self.assertEqual(csvRow[6], "Comment")
        self.assertEqual(csvRow[7], "-12.24")
        self.assertEqual(csvRow[8], "c0, c1")

    def test_transfer_opposite_operation(self):
        account_a = Account(title="Account A", number=12)
        account_a.save()
        account_b = Account(title="Account B", number=13)
        account_b.save()

        # Case 1: A receives an amount and it is then transfered to B
        operation1 = Operation(
                bank_date='2018-02-21',
                date_paid='2018-02-21',
                description='Stuff 1',
                method="Virement",
                account=account_a,
                amount=100
        )
        operation1.save()
        operation2 = Operation(
                bank_date='2018-02-22',
                date_paid='2018-02-22',
                description='A to B',
                method="Virement",
                account=account_a,
                amount=-100
        )
        operation2.save()
        operation3 = Operation(
                bank_date='2018-02-22',
                date_paid='2018-02-22',
                description='B from A',
                method="Virement",
                account=account_b,
                amount=100
        )
        operation3.save()
        operation1.linked_operations.add(operation2)
        operation1.linked_operations.add(operation3)
        utils.all_to_all_linked_operations(operation1)
        operation1.save()
        operation2.save()
        operation3.save()

        self.assertEqual(operation2.transfer_opposite_operation, operation3)
        self.assertEqual(operation3.transfer_opposite_operation, operation2)
        self.assertIsNone(operation1.transfer_opposite_operation)

        # Case 2: same as case 1, but date doesn't match
        operation1 = Operation(
                bank_date='2018-02-21',
                date_paid='2018-02-21',
                description='Stuff 1',
                method="Virement",
                account=account_a,
                amount=100
        )
        operation1.save()
        operation2 = Operation(
                bank_date='2018-02-22',
                date_paid='2018-02-22',
                description='A to B',
                method="Virement",
                account=account_a,
                amount=-100
        )
        operation2.save()
        operation3 = Operation(
                bank_date='2018-02-23',
                date_paid='2018-02-23',
                description='B from A',
                method="Virement",
                account=account_b,
                amount=100
        )
        operation3.save()
        operation1.linked_operations.add(operation2)
        operation1.linked_operations.add(operation3)
        utils.all_to_all_linked_operations(operation1)
        operation1.save()
        operation2.save()
        operation3.save()

        self.assertIsNone(operation1.transfer_opposite_operation)
        self.assertIsNone(operation2.transfer_opposite_operation)
        self.assertIsNone(operation3.transfer_opposite_operation)

        # Case 3: same as case 1, but all operations are on the same account
        operation1 = Operation(
                bank_date='2018-02-21',
                date_paid='2018-02-21',
                description='Stuff 1',
                method="Virement",
                account=account_a,
                amount=100
        )
        operation1.save()
        operation2 = Operation(
                bank_date='2018-02-22',
                date_paid='2018-02-22',
                description='A to B',
                method="Virement",
                account=account_a,
                amount=-100
        )
        operation2.save()
        operation3 = Operation(
                bank_date='2018-02-22',
                date_paid='2018-02-22',
                description='B from A',
                method="Virement",
                account=account_a,
                amount=100
        )
        operation3.save()
        operation1.linked_operations.add(operation2)
        operation1.linked_operations.add(operation3)
        utils.all_to_all_linked_operations(operation1)
        operation1.save()
        operation2.save()
        operation3.save()

        self.assertIsNone(operation1.transfer_opposite_operation)
        self.assertIsNone(operation2.transfer_opposite_operation)
        self.assertIsNone(operation3.transfer_opposite_operation)

        # Case 4: amounts don't match and on same account:
        operation1 = Operation(
                bank_date='2018-02-21',
                date_paid='2018-02-21',
                description='Stuff 1',
                method="Virement",
                account=account_a,
                amount=-50
        )
        operation1.save()
        operation2 = Operation(
                bank_date='2018-02-22',
                date_paid='2018-02-22',
                description='A to B',
                method="Virement",
                account=account_a,
                amount=40
        )
        operation2.save()
        operation1.linked_operations.add(operation2)
        operation1.save()
        operation2.save()

        self.assertIsNone(operation1.transfer_opposite_operation)
        self.assertIsNone(operation2.transfer_opposite_operation)

        # Case 5: transfer from A to B
        operation1 = Operation(
                bank_date='2018-02-21',
                date_paid='2018-02-21',
                description='A to B',
                method="Virement",
                account=account_a,
                amount=-75
        )
        operation1.save()
        operation2 = Operation(
                bank_date='2018-02-21',
                date_paid='2018-02-21',
                description='B from A',
                method="Virement",
                account=account_b,
                amount=75
        )
        operation2.save()
        operation1.linked_operations.add(operation2)
        operation1.save()
        operation2.save()

        self.assertEqual(operation1.transfer_opposite_operation, operation2)
        self.assertEqual(operation2.transfer_opposite_operation, operation1)

        # Case 6: on same account:
        operation1 = Operation(
                bank_date='2018-02-21',
                date_paid='2018-02-21',
                description='Stuff 1',
                method="Virement",
                account=account_a,
                amount=-50
        )
        operation1.save()
        operation2 = Operation(
                bank_date='2018-02-22',
                date_paid='2018-02-22',
                description='Refund of stuff 1',
                method="Virement",
                account=account_a,
                amount=50
        )
        operation2.save()
        operation1.linked_operations.add(operation2)
        operation1.save()
        operation2.save()

        self.assertIsNone(operation1.transfer_opposite_operation)
        self.assertIsNone(operation2.transfer_opposite_operation)

    def test_all_categories(self):
        account = Account(title="DummyAccount", number=13)
        account.save()

        operation = Operation(
            date_paid='2021-02-14',
            description='Foo',
            amount=12.05,
            account=account
        )
        operation.save()

        self.assertEqual(len(operation.all_categories), 1)
        self.assertEqual(operation.all_categories[0], Category.without_category())

        category = Category(title="Dummy")
        category.save()

        operation.categories.add(category)
        operation.save()

        self.assertEqual(len(operation.all_categories), 1)
        self.assertEqual(operation.all_categories[0], category)


class UtilsOperationTest(TestCase):
    def test_unlink(self):
        category_a = Category(title="A")
        category_a.save()
        category_b = Category(title="B")
        category_b.save()

        account = Account(title="DummyAccount", number=123)
        account.save()

        o = Operation(
            bank_date='2018-02-23',
            date_paid='2018-02-21',
            description='Stuff B',
            comment='Comment',
            bank_description='Bank description',
            bank_comment='Bank Comment',
            amount=15,
            account=account,
            method="Virement"
        )
        o.save()
        o.categories.add(category_a)
        o.categories.add(category_b)
        o.save()

        pending_operation = utils.unlink(o)
        pending_operation.save()

        self.assertEqual(o.is_merged_with_pending, False)
        self.assertEqual(o.read, False)
        self.assertEqual(o.description, "")
        self.assertEqual(o.comment, "")
        self.assertEqual(o.bank_date, o.date_paid)
        self.assertIn(category_a, o.categories.all())
        self.assertIn(category_b, o.categories.all())

        self.assertEqual(pending_operation.read, False)
        self.assertEqual(str(pending_operation.date_paid), '2018-02-21')
        self.assertEqual(pending_operation.bank_date, None)
        self.assertEqual(pending_operation.description, "Stuff B")
        self.assertEqual(pending_operation.bank_description, "")
        self.assertEqual(pending_operation.comment, "Comment")
        self.assertEqual(pending_operation.bank_comment, "")
        self.assertEqual(pending_operation.amount, o.amount)
        self.assertEqual(pending_operation.method, o.method)
        self.assertEqual(pending_operation.account, o.account)
        self.assertIn(category_a, pending_operation.categories.all())
        self.assertIn(category_b, pending_operation.categories.all())

    def test_merge_bank_in_pending_operation(self):
        category_a = Category(title="A")
        category_a.save()
        category_b = Category(title="B")
        category_b.save()
        category_c = Category(title="C")
        category_c.save()

        account = Account(title="DummyAccount", number=12)
        account.save()

        bank_operation = Operation(
                bank_date='2018-02-23',
                date_paid='2018-02-23',
                bank_description='Bank description',
                bank_comment='Bank Comment',
                account=account,
                amount=15,
                import_datetime=datetime.datetime.now()
        )
        bank_operation.save()
        bank_operation.categories.add(category_a)
        bank_operation.categories.add(category_b)
        bank_operation.save()

        pending_operation = Operation(
                date_paid='2018-02-21',
                description='Stuff B',
                comment='Comment',
                method="Virement",
                account=account,
                amount=15
        )
        pending_operation.save()
        pending_operation.categories.add(category_b)
        pending_operation.categories.add(category_c)
        pending_operation.save()

        utils.merge_bank_in_pending_operation(bank_operation, pending_operation)

        self.assertEqual(pending_operation.read, False)
        self.assertEqual(pending_operation.is_merged_with_pending, True)
        self.assertEqual(str(pending_operation.date_paid), '2018-02-21')
        self.assertEqual(str(pending_operation.bank_date), '2018-02-23')
        self.assertEqual(pending_operation.description, "Stuff B")
        self.assertEqual(pending_operation.bank_description, "Bank description")
        self.assertEqual(pending_operation.comment, "Comment")
        self.assertEqual(pending_operation.bank_comment, "Bank Comment")
        self.assertIsNotNone(pending_operation.import_datetime)
        self.assertEqual(pending_operation.categories.count(), 3)
        self.assertIn(category_a, pending_operation.categories.all())
        self.assertIn(category_b, pending_operation.categories.all())
        self.assertIn(category_c, pending_operation.categories.all())

    def test_all_to_all_linked_operations(self):
        account = Account(title="DummyAccount", number=12)
        account.save()

        operation_a = Operation(
                date_paid='2018-02-21',
                description='Stuff A',
                comment='Comment',
                method="Virement",
                account=account,
                amount=15
        )
        operation_a.save()

        operation_b = Operation(
                date_paid='2018-02-21',
                description='Stuff B',
                comment='Comment',
                method="Virement",
                account=account,
                amount=15
        )
        operation_b.save()

        operation_c = Operation(
                date_paid='2018-02-21',
                description='Stuff C',
                comment='Comment',
                method="Virement",
                account=account,
                amount=15
        )
        operation_c.save()

        operation_d = Operation(
                date_paid='2018-02-21',
                description='Stuff D',
                comment='Comment',
                method="Virement",
                account=account,
                amount=15
        )
        operation_d.save()

        operation_c.linked_operations.add(operation_d)
        operation_c.save()

        operation_a.linked_operations.add(operation_b)
        operation_a.linked_operations.add(operation_c)

        utils.all_to_all_linked_operations(operation_a)

        self.assertEqual(operation_a.linked_operations.count(), 3)
        self.assertNotIn(operation_a, operation_a.linked_operations.all())
        self.assertIn(operation_b, operation_a.linked_operations.all())
        self.assertIn(operation_c, operation_a.linked_operations.all())
        self.assertIn(operation_d, operation_a.linked_operations.all())

        self.assertEqual(operation_b.linked_operations.count(), 3)
        self.assertNotIn(operation_b, operation_b.linked_operations.all())
        self.assertIn(operation_a, operation_b.linked_operations.all())
        self.assertIn(operation_c, operation_b.linked_operations.all())
        self.assertIn(operation_d, operation_b.linked_operations.all())

        self.assertEqual(operation_c.linked_operations.count(), 3)
        self.assertNotIn(operation_c, operation_c.linked_operations.all())
        self.assertIn(operation_a, operation_c.linked_operations.all())
        self.assertIn(operation_b, operation_c.linked_operations.all())
        self.assertIn(operation_d, operation_c.linked_operations.all())

        self.assertEqual(operation_d.linked_operations.count(), 3)
        self.assertNotIn(operation_d, operation_d.linked_operations.all())
        self.assertIn(operation_a, operation_d.linked_operations.all())
        self.assertIn(operation_b, operation_d.linked_operations.all())
        self.assertIn(operation_c, operation_d.linked_operations.all())

        # Second test, same as OperationEditViewTest.test_link_all_to_all:
        account = Account(title="DummyAccount 2", number=13)
        account.save()

        operation_a = Operation(
                date_paid='2020-12-22',
                description='Stuff A',
                method="Card",
                account=account,
                amount=-50
        )
        operation_a.save()

        operation_b = Operation(
                date_paid='2020-12-23',
                description='Stuff B',
                method="Card",
                account=account,
                amount=30
        )
        operation_b.save()

        operation_a.linked_operations.add(operation_b)
        operation_a.save()
        operation_b.save()

        operation_c = Operation(
                date_paid='2020-12-24',
                description='Stuff C',
                method="Card",
                account=account,
                amount=10
        )
        operation_c.save()

        operation_c.linked_operations.add(operation_a)
        operation_c.save()
        utils.all_to_all_linked_operations(operation_c)

        operations = Operation.objects.filter(account=account)

        self.assertEqual(operations[2].linked_operations.count(), 2)
        self.assertNotIn(operations[2], operations[2].linked_operations.all())
        self.assertIn(operations[0], operations[2].linked_operations.all())
        self.assertIn(operations[1], operations[2].linked_operations.all())

        self.assertEqual(operations[0].linked_operations.count(), 2)
        self.assertNotIn(operations[0], operations[0].linked_operations.all())
        self.assertIn(operations[1], operations[0].linked_operations.all())
        self.assertIn(operations[2], operations[0].linked_operations.all())

        self.assertEqual(operations[1].linked_operations.count(), 2)
        self.assertNotIn(operations[1], operations[1].linked_operations.all())
        self.assertIn(operations[0], operations[1].linked_operations.all())
        self.assertIn(operations[2], operations[1].linked_operations.all())

    def test_get_page_containing_date(self):
        account = Account(title="DummyAccount", number=12)
        account.save()

        bank_operation = Operation(
                bank_date='2018-02-23',
                date_paid='2018-02-23',
                bank_description='Bank description',
                account=account,
                amount=15
        )
        bank_operation.save()

        bank_operation = Operation(
                bank_date='2018-02-22',
                date_paid='2018-02-22',
                bank_description='Bank description',
                account=account,
                amount=15
        )
        bank_operation.save()

        bank_operation = Operation(
                bank_date='2018-02-21',
                date_paid='2018-02-21',
                bank_description='Bank description',
                account=account,
                amount=15
        )
        bank_operation.save()

        bank_operation = Operation(
                bank_date='2018-02-20',
                date_paid='2018-02-20',
                bank_description='Bank description',
                account=account,
                amount=15
        )
        bank_operation.save()

        bank_operation = Operation(
                bank_date='2018-02-19',
                date_paid='2018-02-19',
                bank_description='Bank description',
                account=account,
                amount=15
        )
        bank_operation.save()

        bank_operation = Operation(
                bank_date='2018-02-18',
                date_paid='2018-02-18',
                bank_description='Bank description',
                account=account,
                amount=15
        )
        bank_operation.save()

        # after;
        self.assertEqual(
            utils.get_page_containing_date(account, datetime.date.fromisoformat("2018-03-02"), 2),
            1
        )

        # first;
        self.assertEqual(
            utils.get_page_containing_date(account, datetime.date.fromisoformat("2018-02-23"), 2),
            1
        )

        # second;
        self.assertEqual(
            utils.get_page_containing_date(account, datetime.date.fromisoformat("2018-02-20"), 2),
            2
        )

        # last;
        self.assertEqual(
            utils.get_page_containing_date(account, datetime.date.fromisoformat("2018-02-19"), 2),
            3
        )

        # before;
        self.assertEqual(
            utils.get_page_containing_date(account, datetime.date.fromisoformat("2018-01-19"), 2),
            3
        )

    def _create_operation(self, account: Account):
        o = Operation(
                bank_date='2018-02-23',
                date_paid='2018-02-23',
                bank_description='Bank description',
                description="User description",
                comment="A comment",
                account=account,
                amount=15,
                method="Dummy method"
        )
        o.save()

        return o

    def test_filter_by_categories(self):
        account = Account(title="DummyAccount", number=12)
        account.save()

        c0 = Category(pk=1, title="c0")
        c0.save()
        c1 = Category(pk=2, title="c1")
        c1.save()
        c2 = Category(pk=3, title="c2")
        c2.save()

        o = self._create_operation(account)
        o1 = self._create_operation(account)
        o1.categories.set([c0])
        o2 = self._create_operation(account)
        o2.categories.set([c0, c1])
        o3 = self._create_operation(account)
        o3.categories.set([c0, c1, c2])
        o4 = self._create_operation(account)
        o4.categories.set([c1])
        o5 = self._create_operation(account)
        o5.categories.set([c1, c2])
        o6 = self._create_operation(account)
        o6.categories.set([c2])

        operations = Operation.objects.all()

        filtered = utils.filter_by_categories(operations, [], False, False)
        self.assertEqual(len(filtered), operations.count())

        filtered = utils.filter_by_categories(operations, [], False, True)
        self.assertEqual(filtered[0], o)
        self.assertEqual(len(filtered), 1)

        filtered = utils.filter_by_categories(operations, [], True, False)
        self.assertEqual(len(filtered), operations.count())

        filtered = utils.filter_by_categories(operations, [], True, True)
        self.assertEqual(filtered[0], o)
        self.assertEqual(len(filtered), 1)

        filtered = utils.filter_by_categories(operations, [c0], False, False)
        self.assertEqual(len(filtered), 3)
        self.assertTrue(o1 in filtered)
        self.assertTrue(o2 in filtered)
        self.assertTrue(o3 in filtered)

        filtered = utils.filter_by_categories(operations, [c0], False, True)
        self.assertEqual(filtered[0], o1)
        self.assertEqual(len(filtered), 1)

        filtered = utils.filter_by_categories(operations, [c0], True, False)
        self.assertEqual(len(filtered), 3)
        self.assertTrue(o1 in filtered)
        self.assertTrue(o2 in filtered)
        self.assertTrue(o3 in filtered)

        filtered = utils.filter_by_categories(operations, [c0], True, True)
        self.assertEqual(filtered[0], o1)
        self.assertEqual(len(filtered), 1)

        filtered = utils.filter_by_categories(operations, [c0, c1], False, False)
        self.assertEqual(len(filtered), 5)
        self.assertTrue(o1 in filtered)
        self.assertTrue(o2 in filtered)
        self.assertTrue(o3 in filtered)
        self.assertTrue(o4 in filtered)
        self.assertTrue(o5 in filtered)

        filtered = utils.filter_by_categories(operations, [c0, c1], False, True)
        self.assertEqual(len(filtered), 3)
        self.assertTrue(o1 in filtered)
        self.assertTrue(o2 in filtered)
        self.assertTrue(o4 in filtered)

        filtered = utils.filter_by_categories(operations, [c0, c1], True, False)
        self.assertEqual(len(filtered), 2)
        self.assertTrue(o2 in filtered)
        self.assertTrue(o3 in filtered)

        filtered = utils.filter_by_categories(operations, [c0, c1], True, True)
        self.assertEqual(filtered[0], o2)
        self.assertEqual(len(filtered), 1)

        filtered = utils.filter_by_categories(operations, [c0, c1, c2], False, False)
        self.assertEqual(len(filtered), 6)
        self.assertTrue(o1 in filtered)
        self.assertTrue(o2 in filtered)
        self.assertTrue(o3 in filtered)
        self.assertTrue(o4 in filtered)
        self.assertTrue(o5 in filtered)
        self.assertTrue(o6 in filtered)

        filtered = utils.filter_by_categories(operations, [c0, c1, c2], False, True)
        self.assertEqual(len(filtered), 6)
        self.assertTrue(o1 in filtered)
        self.assertTrue(o2 in filtered)
        self.assertTrue(o3 in filtered)
        self.assertTrue(o4 in filtered)
        self.assertTrue(o5 in filtered)
        self.assertTrue(o6 in filtered)

        filtered = utils.filter_by_categories(operations, [c0, c1, c2], True, False)
        self.assertEqual(filtered[0], o3)
        self.assertEqual(len(filtered), 1)

        filtered = utils.filter_by_categories(operations, [c0, c1, c2], True, True)
        self.assertEqual(filtered[0], o3)
        self.assertEqual(len(filtered), 1)

    def test_hidden_operations(self):
        account = Account(title="DummyAccount", number=12)
        account.save()

        o1 = self._create_operation(account)
        o1.amount = -51.41
        o1.read = True
        o1.save()
        o2 = self._create_operation(account)
        o2.amount = -210.05
        o2.read = True
        o2.save()
        o3 = self._create_operation(account)
        o3.amount = 261.46
        o3.read = True
        o3.linked_operations.add(o1)
        o3.linked_operations.add(o2)
        o3.save()

        o4 = self._create_operation(account)
        o4.amount = 50
        o4.read = True
        o4.save()
        o5 = self._create_operation(account)
        o5.amount = -50
        o5.linked_operations.add(o4)
        o5.read = False
        o5.save()

        hidden = utils.hidden_operations(Operation.objects.all(), account)

        self.assertEqual(len(hidden), 4)
        self.assertTrue(o3.pk in hidden)
        self.assertTrue(o1.pk in hidden)
        self.assertTrue(o2.pk in hidden)
        self.assertTrue(o4.pk in hidden)
        self.assertTrue(o5.pk not in hidden)  # because unread

    def test_get_init_values(self):
        account = Account(title="DummyAccount", number=12)
        account.save()

        c0 = Category(pk=1, title="c0")
        c0.save()
        c1 = Category(pk=2, title="c1")
        c1.save()
        c2 = Category(pk=3, title="c2")
        c2.save()

        # Initialize with an Operation:
        o = self._create_operation(account)
        o.categories.set([c0, c1])
        o.save()

        values = utils.get_init_values(o)
        self.assertEqual(values['method'], o.method)
        self.assertEqual(values['_amount'], o._amount)
        self.assertEqual(values['description'], o.description)
        self.assertEqual(values['comment'], o.comment)
        self.assertEqual(2, len(values['categories']))
        self.assertIn(c0, values['categories'])
        self.assertIn(c1, values['categories'])
        self.assertNotIn('linked_operations', values)

        # Initialize with a refunded operation:
        values = utils.get_init_values(o, is_refund=True)
        self.assertEqual(values['method'], o.method)
        self.assertEqual(values['_amount'], -o._amount)
        self.assertEqual(values['description'][:len(o.description)], o.description)
        self.assertGreater(len(values['description']), len(o.description))
        self.assertEqual(values['comment'], o.comment)
        self.assertEqual(2, len(values['categories']))
        self.assertIn(c0, values['categories'])
        self.assertIn(c1, values['categories'])
        self.assertEqual(1, len(values['linked_operations']))
        self.assertIn(o, values['linked_operations'])

        # Initialize with an OperationTemplate:
        t = OperationTemplate(
                title="Dummy template",
                amount=14.12,
                description='Template description',
                method="Method template",
                comment="Template comment",
        )
        t.save()
        t.accounts.add(account)
        t.categories.set([c0, c1])
        t.save()

        values = utils.get_init_values(t)
        self.assertEqual(values['method'], t.method)
        self.assertEqual(values['_amount'], t.amount)
        self.assertEqual(values['description'], t.description)
        self.assertEqual(values['comment'], t.comment)
        self.assertEqual(2, len(values['categories']))
        self.assertIn(c0, values['categories'])
        self.assertIn(c1, values['categories'])
        self.assertNotIn('linked_operations', values)


class OperationFormTest(TestCase):
    def test_present_fields(self):
        """ Test present fields according to what the form edits."""
        account = Account(title="DummyAccount", number=123)
        account.save()

        # Empty form, to create a pending operation:
        form = OperationForm()
        self.assertEqual(7, len(form.fields))
        self.assertIn('method', form.fields)
        self.assertIn('_amount', form.fields)
        self.assertIn('description', form.fields)
        self.assertIn('comment', form.fields)
        self.assertIn('categories', form.fields)
        self.assertIn('linked_operations', form.fields)
        self.assertIn('all_to_all', form.fields)

        # Edit a pending operation:
        pending_operation = Operation(
            date_paid='2018-02-21',
            description='Stuff B',
            comment='Comment',
            method="Virement",
            account=account,
            amount=15
        )
        pending_operation.save()
        form = OperationForm(instance=pending_operation)
        self.assertEqual(8, len(form.fields))
        self.assertIn('_date_paid', form.fields)
        self.assertIn('method', form.fields)
        self.assertIn('_amount', form.fields)
        self.assertIn('description', form.fields)
        self.assertIn('comment', form.fields)
        self.assertIn('categories', form.fields)
        self.assertIn('linked_operations', form.fields)
        self.assertIn('all_to_all', form.fields)

        # Edit a bank operation (not merged with a pending):
        bank_operation = Operation(
            bank_date='2018-02-19',
            date_paid='2018-02-19',
            bank_description='Bank description',
            account=account,
            amount=15
        )
        bank_operation.save()
        form = OperationForm(instance=bank_operation)
        self.assertEqual(12, len(form.fields))
        self.assertIn('_date_paid', form.fields)
        self.assertIn('_bank_date', form.fields)
        self.assertIn('method', form.fields)
        self.assertIn('_amount', form.fields)
        self.assertIn('description', form.fields)
        self.assertIn('bank_description', form.fields)
        self.assertIn('comment', form.fields)
        self.assertIn('bank_comment', form.fields)
        self.assertIn('read', form.fields)
        self.assertIn('categories', form.fields)
        self.assertIn('linked_operations', form.fields)
        self.assertIn('all_to_all', form.fields)

        # Edit a bank operation merged with a pending one:
        o = Operation(
            bank_date='2018-02-23',
            date_paid='2018-02-23',
            description='Stuff B',
            amount=15,
            account=account,
            method="Virement"
        )
        o.save()
        form = OperationForm(instance=o)
        self.assertEqual(12, len(form.fields))
        self.assertIn('_date_paid', form.fields)
        self.assertIn('_bank_date', form.fields)
        self.assertIn('method', form.fields)
        self.assertIn('_amount', form.fields)
        self.assertIn('description', form.fields)
        self.assertIn('bank_description', form.fields)
        self.assertIn('comment', form.fields)
        self.assertIn('bank_comment', form.fields)
        self.assertIn('read', form.fields)
        self.assertIn('categories', form.fields)
        self.assertIn('linked_operations', form.fields)
        self.assertIn('all_to_all', form.fields)
