from django.test import TestCase
from django.urls import reverse

from money.models.operation_templates import OperationTemplate


class OperationTemplateDeleteView(TestCase):
    def test_delete(self):
        template = OperationTemplate(title="Foo")
        template.save()

        response = self.client.post(reverse('operation_templates_delete', args=[template.pk]))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('operation_templates_index'))
        self.assertEqual(OperationTemplate.objects.count(), 0)
