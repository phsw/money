from django.test import TestCase

from money.models.categories import Category


class CategoryTest(TestCase):
    def test_str(self):
        c = Category(title="Foo")
        self.assertEqual(str(c), c.title)

    def test_auto_affects(self):
        c = Category(auto_affect="")
        self.assertEqual(c.auto_affects, [])

        c.auto_affect = "foo,bar"
        self.assertEqual(c.auto_affects, ["foo", "bar"])
