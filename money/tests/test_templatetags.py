from django.test import TestCase

from money.models.accounts import Account
from money.templatetags.display_amount import display_amount
from money.templatetags.money_paginator import _generate_pagination
from money.templatetags.account_occupancy import account_occupancy
from money.templatetags import list_prev


class DisplayAmountTemplateTagTest(TestCase):
    def test_display_amount(self):
        c = display_amount(None)
        self.assertIsNone(c['amount'])
        self.assertIsNone(c['css'])

        c = display_amount(3.14)
        self.assertEqual(c['amount'], 3.14)
        self.assertEqual(c['css'], "text-success")

        c = display_amount(-3.14)
        self.assertEqual(c['amount'], -3.14)
        self.assertEqual(c['css'], "text-danger")

        c = display_amount(None, True)
        self.assertIsNone(c['amount'])
        self.assertIsNone(c['css'])

        c = display_amount(3.14, True)
        self.assertEqual(c['amount'], 3.14)
        self.assertIsNone(c['css'])

        c = display_amount(-3.14, True)
        self.assertEqual(c['amount'], -3.14)
        self.assertIsNone(c['css'])


class AccountOccupancyTemplateTagTest(TestCase):
    def test_account_occupancy(self):
        a = Account(balance_limit=None, balance=100)

        with self.assertRaises(AssertionError):
            account_occupancy(a)

        a.balance_limit = 200
        a.occupancy = 50
        r = account_occupancy(a)
        self.assertEqual(r['balance'], a.balance)
        self.assertEqual(r['balance_limit'], a.balance_limit)
        self.assertEqual(r['occupancy'], a.occupancy)
        self.assertEqual(r['bar_width'], a.occupancy)

        a.balance_limit = 200
        a.occupancy = 200
        r = account_occupancy(a)
        self.assertEqual(r['balance'], a.balance)
        self.assertEqual(r['balance_limit'], a.balance_limit)
        self.assertEqual(r['occupancy'], a.occupancy)
        self.assertEqual(r['bar_width'], 100)

        # When the account is just created:
        a.balance_limit = 200
        a.occupancy = None
        a.balance = None
        r = account_occupancy(a)
        self.assertEqual(r['balance'], 0)
        self.assertEqual(r['balance_limit'], a.balance_limit)
        self.assertEqual(r['occupancy'], 0)
        self.assertEqual(r['bar_width'], 0)


class ListPrevTemplateTagTest(TestCase):
    list_to_test = ['a', 'b', 'c']

    def test_next(self):
        self.assertEqual(list_prev.next(self.list_to_test, -1), self.list_to_test[0])
        self.assertEqual(list_prev.next(self.list_to_test, 0), self.list_to_test[1])
        self.assertEqual(list_prev.next(self.list_to_test, 1), self.list_to_test[2])
        self.assertIsNone(list_prev.next(self.list_to_test, 2))

    def test_previous(self):
        self.assertIsNone(list_prev.previous(self.list_to_test, 4))
        self.assertEqual(list_prev.previous(self.list_to_test, 3), self.list_to_test[2])
        self.assertEqual(list_prev.previous(self.list_to_test, 2), self.list_to_test[1])
        self.assertEqual(list_prev.previous(self.list_to_test, 1), self.list_to_test[0])


class DisplayPaginationTemplateTagTest(TestCase):
    def test_generate_pagination(self):
        p = _generate_pagination(1, 1, 2)
        self.assertEqual(p, [1])

        p = _generate_pagination(2, 1, 2)
        self.assertEqual(p, [1, 2])

        p = _generate_pagination(2, 2, 2)
        self.assertEqual(p, [1, 2])

        p = _generate_pagination(3, 1, 2)
        self.assertEqual(p, [1, 2, 3])

        p = _generate_pagination(3, 2, 2)
        self.assertEqual(p, [1, 2, 3])

        p = _generate_pagination(3, 3, 2)
        self.assertEqual(p, [1, 2, 3])

        p = _generate_pagination(4, 1, 2)
        self.assertEqual(p, [1, 2, 3, 4])

        p = _generate_pagination(4, 2, 2)
        self.assertEqual(p, [1, 2, 3, 4])

        p = _generate_pagination(4, 3, 2)
        self.assertEqual(p, [1, 2, 3, 4])

        p = _generate_pagination(4, 4, 2)
        self.assertEqual(p, [1, 2, 3, 4])

        p = _generate_pagination(5, 1, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5])

        p = _generate_pagination(5, 2, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5])

        p = _generate_pagination(5, 3, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5])

        p = _generate_pagination(5, 4, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5])

        p = _generate_pagination(5, 5, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5])

        p = _generate_pagination(6, 1, 2)
        self.assertEqual(p, [1, 2, 3, None, 6])

        p = _generate_pagination(6, 2, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5, 6])

        p = _generate_pagination(6, 3, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5, 6])

        p = _generate_pagination(6, 4, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5, 6])

        p = _generate_pagination(6, 5, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5, 6])

        p = _generate_pagination(6, 6, 2)
        self.assertEqual(p, [1, None, 4, 5, 6])

        p = _generate_pagination(7, 1, 2)
        self.assertEqual(p, [1, 2, 3, None, 7])

        p = _generate_pagination(7, 2, 2)
        self.assertEqual(p, [1, 2, 3, 4, None, 7])

        p = _generate_pagination(7, 3, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5, 6, 7])

        p = _generate_pagination(7, 4, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5, 6, 7])

        p = _generate_pagination(7, 5, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5, 6, 7])

        p = _generate_pagination(7, 6, 2)
        self.assertEqual(p, [1, None, 4, 5, 6, 7])

        p = _generate_pagination(7, 7, 2)
        self.assertEqual(p, [1, None, 5, 6, 7])

        p = _generate_pagination(8, 1, 2)
        self.assertEqual(p, [1, 2, 3, None, 8])

        p = _generate_pagination(8, 2, 2)
        self.assertEqual(p, [1, 2, 3, 4, None, 8])

        p = _generate_pagination(8, 3, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5, None, 8])

        p = _generate_pagination(8, 4, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5, 6, 7, 8])

        p = _generate_pagination(8, 5, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5, 6, 7, 8])

        p = _generate_pagination(8, 6, 2)
        self.assertEqual(p, [1, None, 4, 5, 6, 7, 8])

        p = _generate_pagination(8, 7, 2)
        self.assertEqual(p, [1, None, 5, 6, 7, 8])

        p = _generate_pagination(8, 8, 2)
        self.assertEqual(p, [1, None, 6, 7, 8])

        p = _generate_pagination(9, 1, 2)
        self.assertEqual(p, [1, 2, 3, None, 9])

        p = _generate_pagination(9, 2, 2)
        self.assertEqual(p, [1, 2, 3, 4, None, 9])

        p = _generate_pagination(9, 3, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5, None, 9])

        p = _generate_pagination(9, 4, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5, 6, None, 9])

        p = _generate_pagination(9, 5, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5, 6, 7, 8, 9])

        p = _generate_pagination(9, 6, 2)
        self.assertEqual(p, [1, None, 4, 5, 6, 7, 8, 9])

        p = _generate_pagination(9, 7, 2)
        self.assertEqual(p, [1, None, 5, 6, 7, 8, 9])

        p = _generate_pagination(9, 8, 2)
        self.assertEqual(p, [1, None, 6, 7, 8, 9])

        p = _generate_pagination(9, 9, 2)
        self.assertEqual(p, [1, None, 7, 8, 9])

        p = _generate_pagination(10, 1, 2)
        self.assertEqual(p, [1, 2, 3, None, 10])

        p = _generate_pagination(10, 2, 2)
        self.assertEqual(p, [1, 2, 3, 4, None, 10])

        p = _generate_pagination(10, 3, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5, None, 10])

        p = _generate_pagination(10, 4, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5, 6, None, 10])

        p = _generate_pagination(10, 5, 2)
        self.assertEqual(p, [1, 2, 3, 4, 5, 6, 7, None, 10])

        p = _generate_pagination(10, 6, 2)
        self.assertEqual(p, [1, None, 4, 5, 6, 7, 8, 9, 10])

        p = _generate_pagination(10, 7, 2)
        self.assertEqual(p, [1, None, 5, 6, 7, 8, 9, 10])

        p = _generate_pagination(10, 8, 2)
        self.assertEqual(p, [1, None, 6, 7, 8, 9, 10])

        p = _generate_pagination(10, 9, 2)
        self.assertEqual(p, [1, None, 7, 8, 9, 10])

        p = _generate_pagination(10, 10, 2)
        self.assertEqual(p, [1, None, 8, 9, 10])

        p = _generate_pagination(20, 10, 2)
        self.assertEqual(p, [1, None, 8, 9, 10, 11, 12, None, 20])
