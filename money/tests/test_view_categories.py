from django.test import TestCase
from django.urls import reverse
from django.utils.encoding import force_str

from money.models.accounts import Account
from money.models.categories import Category
from money.models.operations import Operation


class CategoryIndexViewTest(TestCase):
    def test_without_category(self):
        response = self.client.get(reverse('categories_index'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['categories'], [])
        self.assertContains(response, Category.without_category().title)

    def test_with_many_categories(self):
        category_c = Category(title="Category C")
        category_c.save()
        category_b = Category(title="Category B")
        category_b.save()
        category_a = Category(title="Category A")
        category_a.save()

        response = self.client.get(reverse('categories_index'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(
            response.context['categories'],
            [category_a, category_b, category_c]
        )
        self.assertContains(response, category_a.title)
        self.assertContains(response, category_b.title)
        self.assertContains(response, category_c.title)
        self.assertContains(response, Category.without_category().title)


class CategoryAddViewTest(TestCase):
    route = reverse("categories_add")

    def test_get(self):
        response = self.client.get(self.route)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        response = self.client.post(self.route, {}, follow=False)
        self.assertFalse(response.context['form'].is_valid())
        self.assertEqual(response.status_code, 200)

        response = self.client.post(self.route, {"title": "Foo"}, follow=False)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('categories_view', args=[1]))
        category = Category.objects.first()
        self.assertEqual(category.title, "Foo")
        self.assertEqual(category.auto_affect, "")
        category.delete()

        response = self.client.post(self.route, {
            "title": "Bar",
            "auto_affect": "foo,bar"
        }, follow=False)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('categories_view', args=[2]))
        category = Category.objects.first()
        self.assertEqual(category.title, "Bar")
        self.assertEqual(category.auto_affect, "foo,bar")

        response = self.client.post(self.route, {"title": "Bar"}, follow=False)
        self.assertFalse(response.context['form'].is_valid())
        self.assertEqual(response.status_code, 200)

        response = self.client.post(self.route, {
            "title": str(Category.without_category().title)
        }, follow=False)
        self.assertFalse(response.context['form'].is_valid())
        self.assertEqual(response.status_code, 200)


class CategoryEditViewTest(TestCase):
    def test_valid_get(self):
        category = Category(title="Foo")
        category.save()

        response = self.client.get(reverse('categories_edit', args=[category.pk]))
        self.assertEqual(response.status_code, 200)

    def test_wrong_get(self):
        response = self.client.get(reverse('categories_edit', args=[1]))
        self.assertEqual(response.status_code, 404)

        response = self.client.get(reverse(
            'categories_edit',
            args=[Category.without_category().pk]
        ))
        self.assertEqual(response.status_code, 404)

    def test_wrong_post(self):
        response = self.client.post(reverse('categories_edit', args=[1]), {"title": "Bar"})
        self.assertEqual(response.status_code, 404)

        response = self.client.post(reverse(
            'categories_edit',
            args=[Category.without_category().pk]
        ), {"title": "Bar"})
        self.assertEqual(response.status_code, 404)

        category = Category(title="Foo", auto_affect="foo,bar")
        category.save()
        response = self.client.post(reverse('categories_edit', args=[category.pk]), {
            "title": str(Category.without_category().title)
        }, follow=False)
        self.assertFalse(response.context['form'].is_valid())
        self.assertEqual(response.status_code, 200)

    def test_valid_post(self):
        category = Category(title="Foo", auto_affect="foo,bar")
        category.save()

        response = self.client.post(reverse('categories_edit', args=[category.pk]), {
            "title": "Bar",
            "auto_affect": "bar,foo"
        }, follow=False)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('categories_view', args=[category.pk]))

        category.refresh_from_db()
        self.assertEqual(category.title, "Bar")
        self.assertEqual(category.auto_affect, "bar,foo")


class CategoryDeleteViewTest(TestCase):
    def test_unvalid(self):
        response = self.client.get(reverse('categories_delete', args=[1]))
        self.assertEqual(response.status_code, 405)

        response = self.client.post(reverse('categories_delete', args=[1]))
        self.assertEqual(response.status_code, 404)

        response = self.client.post(reverse(
            'categories_delete',
            args=[Category.without_category().pk]
        ), {"title": "Bar"})
        self.assertEqual(response.status_code, 404)

    def test_valid(self):
        category = Category(title="Foo", auto_affect="foo,bar")
        category.save()

        response = self.client.post(reverse('categories_delete', args=[category.pk]))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('categories_index'))
        self.assertEqual(Category.objects.count(), 0)


class CategoryGetAutoAffectViewTest(TestCase):
    def test_get(self):
        route = reverse('categories_get_auto_affect')

        response = self.client.get(route)
        self.assertJSONEqual(force_str(response.content), [])

        response = self.client.get(route + "?desc=foo")
        self.assertJSONEqual(force_str(response.content), [])

        c1 = Category(title="c1", auto_affect="")
        c1.save()
        c2 = Category(title="c2", auto_affect="foo")
        c2.save()
        c3 = Category(title="c3", auto_affect="foo,bar")
        c3.save()
        c4 = Category(title="c4", auto_affect="dead,beef")
        c4.save()

        response = self.client.get(route + "?desc=foo bar")
        self.assertJSONEqual(force_str(response.content), [c2.pk, c3.pk])


class CategoryDetailViewTest(TestCase):
    def test_unvalid_get(self):
        response = self.client.get(reverse('categories_view', args=[1]))
        self.assertEqual(response.status_code, 404)

    def test_valid_get(self):
        c1 = Category(title="c1", auto_affect="")
        c1.save()
        c2 = Category(title="c2", auto_affect="foo")
        c2.save()

        account = Account(title="Dummy Account", number=12, balance=1.02)
        account.save()
        other_account = Account(title="Other Dummy Account", number=13)
        other_account.save()

        response = self.client.get(reverse('categories_view', args=[c1.pk]))
        self.assertEqual(response.status_code, 200)

        bank_operation_without_category = Operation(
            bank_date="2021-02-14",
            date_paid="2021-02-14",
            amount=14.02,
            account=account
        )
        bank_operation_without_category.save()

        bank_operation_other_category = Operation(
            bank_date="2021-01-14",
            date_paid="2021-01-14",
            amount=15.02,
            account=account
        )
        bank_operation_other_category.save()
        bank_operation_other_category.categories.add(c2)
        bank_operation_other_category.save()

        bank_operation_a = Operation(
            bank_date="2021-01-14",
            date_paid="2021-01-14",
            amount=15.02,
            account=account
        )
        bank_operation_a.save()
        bank_operation_a.categories.add(c1)
        bank_operation_a.save()

        bank_operation_b = Operation(
            bank_date="2020-12-14",
            date_paid="2020-12-14",
            amount=-5,
            account=account
        )
        bank_operation_b.save()
        bank_operation_b.categories.add(c1)
        bank_operation_b.categories.add(c2)
        bank_operation_b.save()

        pending_operation = Operation(
            date_paid="2021-01-27",
            amount=13.12,
            description="Pending operation",
            method="Card",
            account=account
        )
        pending_operation.save()
        pending_operation.categories.add(c1)
        pending_operation.save()

        bank_operation_c = Operation(
            bank_date="2020-11-14",
            date_paid="2020-11-14",
            amount=27,
            account=other_account
        )
        bank_operation_c.save()
        bank_operation_c.categories.add(c1)
        bank_operation_c.save()

        response = self.client.get(reverse('categories_view', args=[c1.pk]))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(
            response.context['object_list'],
            [bank_operation_a, bank_operation_b, bank_operation_c]
        )

    def test_get_without_category(self):
        c1 = Category(title="c1", auto_affect="")
        c1.save()

        account = Account(title="Dummy Account", number=12, balance=1.02)
        account.save()
        other_account = Account(title="Other Dummy Account", number=13)
        other_account.save()

        response = self.client.get(reverse('categories_view', args=[c1.pk]))
        self.assertEqual(response.status_code, 200)

        bank_operation_with_category = Operation(
            bank_date="2021-02-14",
            date_paid="2021-02-14",
            amount=14.02,
            account=account
        )
        bank_operation_with_category.save()
        bank_operation_with_category.categories.add(c1)
        bank_operation_with_category.save()

        bank_operation_a = Operation(
            bank_date="2021-01-14",
            date_paid="2021-01-14",
            amount=15.02,
            account=account
        )
        bank_operation_a.save()

        bank_operation_b = Operation(
            bank_date="2020-12-14",
            date_paid="2020-12-14",
            amount=-5,
            account=account
        )
        bank_operation_b.save()

        pending_operation = Operation(
            date_paid="2021-01-27",
            amount=13.12,
            description="Pending operation",
            method="Card",
            account=account
        )
        pending_operation.save()

        bank_operation_c = Operation(
            bank_date="2020-11-14",
            date_paid="2020-11-14",
            amount=27,
            account=other_account
        )
        bank_operation_c.save()

        response = self.client.get(reverse(
            'categories_view',
            args=[Category.without_category().pk]
        ))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(
            response.context['object_list'],
            [bank_operation_a, bank_operation_b, bank_operation_c]
        )
