import datetime

from django.test import TestCase
from django.urls import reverse
from django.utils.encoding import force_str

from money.models.accounts import Account
from money.models.import_authorization import ImportAuthorization


class ImportFromBankViewTest(TestCase):
    def test_get(self):
        response = self.client.get(reverse('import_web_bank_import_from_bank'))
        self.assertEqual(response.status_code, 405)

    def test_without_grant(self):
        response = self.client.post(reverse('import_web_bank_import_from_bank'))
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_str(response.content), {"response": "Import not allowed."})

    def test_bad_grant_key(self):
        grant = ImportAuthorization(key="foo")
        grant.save()

        response = self.client.post(reverse('import_web_bank_import_from_bank'), {"key": "bar"})
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_str(response.content), {"response": "Import not allowed."})

    def test_bad_grant_time(self):
        grant = ImportAuthorization(key="foo", date=datetime.datetime(2000, 12, 30, 19, 45, 54))
        grant.save()

        response = self.client.post(reverse('import_web_bank_import_from_bank'), {"key": grant.key})
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_str(response.content), {"response": "Import not allowed."})

    def test_missing_data(self):
        grant = ImportAuthorization(key="foo")
        grant.save()

        response = self.client.post(reverse('import_web_bank_import_from_bank'), {
            "key": grant.key
        })
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_str(response.content), {
            "response": "Error: Missing information."
        })

        response = self.client.post(reverse('import_web_bank_import_from_bank'), {
            "key": grant.key,
            "content": "empty"
        })
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_str(response.content), {
            "response": "Error: Missing information."
        })

        response = self.client.post(reverse('import_web_bank_import_from_bank'), {
            "key": grant.key,
            "parser": "empty"
        })
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_str(response.content), {
            "response": "Error: Missing information."
        })

    def test_unknown_parser(self):
        grant = ImportAuthorization(key="foo")
        grant.save()

        response = self.client.post(reverse('import_web_bank_import_from_bank'), {
            "key": grant.key,
            "content": "empty",
            "parser": "wrong"
        })
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_str(response.content), {
            "response": "Error: There is no parser for this bank."
        })

    def test_correct_parser(self):
        grant = ImportAuthorization(key="foo")
        grant.save()

        response = self.client.post(reverse('import_web_bank_import_from_bank'), {
            "key": grant.key,
            "content": "empty",
            "parser": "cane2020default"
        })
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_str(response.content), {
            "response": "Error: list index out of range"
        })


class ImportWebBankAllowViewTest(TestCase):
    def test_view(self):
        account = Account(title="DummyAccount", number=13)
        account.save()

        grant = ImportAuthorization(key="foo")
        grant.save()
        grant = ImportAuthorization(key="bar")
        grant.save()

        response = self.client.get(reverse('import_web_bank_allow'))
        self.assertEqual(response.status_code, 200)

        self.assertEqual(ImportAuthorization.objects.all().count(), 1)
        grant = ImportAuthorization.objects.first()
        self.assertNotIn(grant.key, ["foo", "bar"])

        self.assertEqual(response.context['key'], grant.key)
        self.assertContains(response, grant.key)

    def test_view_no_account(self):
        response = self.client.get(reverse('import_web_bank_allow'))
        self.assertEqual(response.status_code, 200)

        self.assertEqual(ImportAuthorization.objects.all().count(), 0)


class ImportWebBankAjaxImportedAccountsViewTest(TestCase):
    def test_view(self):
        response = self.client.get(reverse('import_web_bank_imported_accounts'))
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_str(response.content), [])

        imported_accounts = [
            {'url': 'aa', 'title': 'Account A', 'pk': '1'},
            {'url': 'bb', 'title': 'Account B', 'pk': '2'}
        ]
        session = self.client.session
        session['imported_accounts'] = imported_accounts
        session.save()
        response = self.client.get(reverse('import_web_bank_imported_accounts'))
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_str(response.content), imported_accounts)

    def test_view_after_import(self):
        grant = ImportAuthorization(key="foo")
        grant.save()

        account = Account(number=123456789)
        account.save()

        with open('fixtures/cane2023epargne.html', 'r') as f:
            response = self.client.post(reverse('import_web_bank_import_from_bank'), {
                "key": grant.key,
                "content": f.read(),
                "parser": "cane2023epargne"
            })
            self.assertEqual(response.status_code, 200)
            self.assertJSONEqual(force_str(response.content), {
                "response": 'Import succeded: 3 imported operations, 0 matching pending operations'
            })

            response = self.client.get(reverse('import_web_bank_imported_accounts'))
            self.assertEqual(response.status_code, 200)
            self.assertJSONEqual(
                force_str(response.content),
                [{'url': '/accounts/view/1', 'title': '', 'pk': '1'}]
            )

        with open('fixtures/cane2023epargne2.html', 'r') as f:
            response = self.client.post(reverse('import_web_bank_import_from_bank'), {
                "key": grant.key,
                "content": f.read(),
                "parser": "cane2023epargne"
            })
            self.assertEqual(response.status_code, 200)
            self.assertJSONEqual(force_str(response.content), {
                "response": 'Import succeded: 1 imported operation, 0 matching pending operations'
            })

            response = self.client.get(reverse('import_web_bank_imported_accounts'))
            self.assertEqual(response.status_code, 200)
            self.assertJSONEqual(
                force_str(response.content),
                [{'url': '/accounts/view/1', 'title': '', 'pk': '1'}]
            )


class ImportWebBankDeleteImportedAccountViewTest(TestCase):
    def test_view(self):
        route = reverse('import_web_bank_delete_imported_account', args=[12])

        response = self.client.get(route)
        self.assertEqual(response.status_code, 405)

        response = self.client.post(route)
        self.assertEqual(response.status_code, 400)

        response = self.client.post(route, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_str(response.content), {"result": True})

        imported_accounts = [
            {'url': 'aa', 'title': 'Account A', 'pk': '12'},
            {'url': 'bb', 'title': 'Account B', 'pk': '13'}
        ]
        session = self.client.session
        session['imported_accounts'] = imported_accounts
        session.save()
        response = self.client.post(route, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_str(response.content), {"result": True})
        self.assertEqual(len(self.client.session['imported_accounts']), 1)
        self.assertEqual(self.client.session['imported_accounts'][0]['pk'], '13')


class ImportWebBankDeleteAllImportedAccountsViewTest(TestCase):
    def test_view(self):
        route = reverse('import_web_bank_delete_all_imported_accounts')

        response = self.client.get(route)
        self.assertEqual(response.status_code, 405)

        response = self.client.post(route)
        self.assertEqual(response.status_code, 400)

        response = self.client.post(route, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_str(response.content), {"result": True})

        imported_accounts = [
            {'url': 'aa', 'title': 'Account A', 'pk': '12'},
            {'url': 'bb', 'title': 'Account B', 'pk': '13'}
        ]
        session = self.client.session
        session['imported_accounts'] = imported_accounts
        session.save()
        response = self.client.post(route, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(force_str(response.content), {"result": True})
        self.assertNotIn('imported_accounts', self.client.session)
