from django.test import TestCase

from money.models.operations import Operation
from money.models.categories import Category
from money.models.accounts import Account
from money.utils import auto_affect_categories


class AutoAffectCategoriesTest(TestCase):
    def test_affect_categories(self):
        account = Account(number=123)
        account.save()

        c1 = Category(title="c1", auto_affect="")
        c2 = Category(title="c2", auto_affect="foo")
        c3 = Category(title="c3", auto_affect="foo,bar")
        c4 = Category(title="c4", auto_affect="dead,beef")

        categories = [c1, c2, c3, c4]
        Category.objects.bulk_create(categories)
        categories = Category.objects.all()

        o = Operation(
            date_paid="2019-12-04",
            amount=3.12,
            account=account
        )
        o.save()
        auto_affect_categories.affect_categories(o, categories)
        self.assertEqual(o.categories.count(), 0)

        o = Operation(
            date_paid="2019-12-04",
            amount=3.12,
            account=account,
            description="nothing"
        )
        o.save()
        auto_affect_categories.affect_categories(o, categories)
        self.assertEqual(o.categories.count(), 0)

        o = Operation(
            date_paid="2019-12-04",
            amount=3.12,
            account=account,
            description="foo"
        )
        o.save()
        auto_affect_categories.affect_categories(o, categories)
        self.assertEqual(o.categories.count(), 2)
        self.assertEqual(o.categories.all()[0].title, "c2")
        self.assertEqual(o.categories.all()[1].title, "c3")

        o = Operation(
            date_paid="2019-12-04",
            amount=3.12,
            account=account,
            description="dead beef"
        )
        o.save()
        auto_affect_categories.affect_categories(o, categories)
        self.assertEqual(o.categories.count(), 1)
        self.assertEqual(o.categories.all()[0].title, "c4")

        o = Operation(
            date_paid="2019-12-04",
            amount=3.12,
            account=account,
            description="foo bar"
        )
        o.save()
        o.categories.add(categories[1])
        o.save()
        auto_affect_categories.affect_categories(o, categories)
        self.assertEqual(o.categories.count(), 2)
        self.assertEqual(o.categories.all()[0].title, "c2")
        self.assertEqual(o.categories.all()[1].title, "c3")

    def test_get_matching_categories(self):
        c1 = Category(title="c1", auto_affect="")
        c2 = Category(title="c2", auto_affect="foo")
        c3 = Category(title="c3", auto_affect="foo,bar")
        c4 = Category(title="c4", auto_affect="dead,beef")

        categories = [c1, c2, c3, c4]

        matching = auto_affect_categories.get_matching_categories("", categories)
        self.assertEqual(len(matching), 0)

        matching = auto_affect_categories.get_matching_categories("nothing", categories)
        self.assertEqual(len(matching), 0)

        matching = auto_affect_categories.get_matching_categories("foo", categories)
        self.assertEqual(len(matching), 2)
        self.assertEqual(matching[0].title, "c2")
        self.assertEqual(matching[1].title, "c3")

        matching = auto_affect_categories.get_matching_categories("dead beef", categories)
        self.assertEqual(len(matching), 1)
        self.assertEqual(matching[0].title, "c4")

        matching = auto_affect_categories.get_matching_categories("foo bar", categories)
        self.assertEqual(len(matching), 2)
        self.assertEqual(matching[0].title, "c2")
        self.assertEqual(matching[1].title, "c3")
