from django.test import TestCase

from money.models.accounts import Account
from money.models.categories import Category
from money.models.operations import Operation
from money.models.operation_templates import OperationTemplate
from money.utils import operation_templates as utils


class UtilsOperationTemplateTest(TestCase):
    def test_get_init_values(self):
        account = Account(title="DummyAccount", number=12)
        account.save()

        c0 = Category(pk=1, title="c0")
        c0.save()
        c1 = Category(pk=2, title="c1")
        c1.save()
        c2 = Category(pk=3, title="c2")
        c2.save()

        o = Operation(
                bank_date='2018-02-23',
                date_paid='2018-02-23',
                bank_description='Bank description',
                description="User description",
                comment="A comment",
                account=account,
                amount=15,
                method="Dummy method"
        )
        o.save()
        o.categories.set([c0, c1])
        o.save()

        values = utils.get_init_values(o)
        self.assertEqual(values['title'], o.description)
        self.assertEqual(values['amount'], o._amount)
        self.assertEqual(values['description'], o.description)
        self.assertEqual(values['comment'], o.comment)
        self.assertEqual(values['method'], o.method)
        self.assertIn(account, values['accounts'])
        self.assertEqual(2, len(values['categories']))
        self.assertIn(c0, values['categories'])
        self.assertIn(c1, values['categories'])


class OperationTemplateTest(TestCase):
    def test_str(self):
        o = OperationTemplate(title="Foo")
        self.assertEqual(str(o), "Operation template: Foo")
