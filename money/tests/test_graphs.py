import datetime
from django.test import TestCase

from money.models.operations import Operation
from money.models.accounts import Account
from money.models.categories import Category
from money.utils import graphs


class GraphsTest(TestCase):
    def _generate_fixtures(self):
        account = Account(number=123)
        account.save()

        account_bis = Account(number=124)
        account_bis.save()

        o = Operation(
            bank_date="2019-12-04",
            date_paid="2019-12-04",
            amount=3.12,
            account=account_bis
        )
        o.save()

        o = Operation(
            bank_date="2019-12-04",
            date_paid="2019-12-04",
            amount=3.12,
            account=account
        )
        o.save()

        o = Operation(
            date_paid="2019-12-04",
            amount=3.12,
            account=account
        )
        o.save()

        o = Operation(
            bank_date="2019-12-04",
            date_paid="2019-12-04",
            amount=7.12,
            account=account
        )
        o.save()

        o = Operation(
            bank_date="2019-12-20",
            date_paid="2019-12-20",
            amount=-15.12,
            account=account
        )
        o.save()

        o = Operation(
            bank_date="2020-01-20",
            date_paid="2020-01-20",
            amount=100,
            account=account
        )
        o.save()

        o = Operation(
            bank_date="2020-12-20",
            date_paid="2020-12-20",
            amount=11.16,
            account=account
        )
        o.save()

        return account

    def test_get_account_monthly_balances(self):
        account = self._generate_fixtures()

        g, display_limit = graphs.get_account_monthly_balances(account, datetime.date.today())
        self.assertFalse(display_limit)  # no balance limit
        self.assertEqual(len(g), 3)
        self.assertEqual(str(g[0]['period']), "December 2019")
        self.assertEqual(g[0]['balance'], -4.88)
        self.assertEqual(g[0]['global_balance'], -4.88)
        self.assertEqual(str(g[1]['period']), "January 2020")
        self.assertEqual(g[1]['balance'], 100)
        self.assertEqual(g[1]['global_balance'], 95.12)
        self.assertEqual(str(g[2]['period']), "December 2020")
        self.assertEqual(g[2]['balance'], 11.16)
        self.assertEqual(g[2]['global_balance'], 106.28)

        # Previous month:
        g, display_limit = graphs.get_account_monthly_balances(account, datetime.date(2020, 12, 1))
        self.assertFalse(display_limit)  # no balance limit
        self.assertEqual(len(g), 2)
        self.assertEqual(str(g[0]['period']), "December 2019")
        self.assertEqual(g[0]['balance'], -4.88)
        self.assertEqual(g[0]['global_balance'], -4.88)
        self.assertEqual(str(g[1]['period']), "January 2020")
        self.assertEqual(g[1]['balance'], 100)
        self.assertEqual(g[1]['global_balance'], 95.12)

        # max value under 0.5*balance limit
        account.balance_limit = 300
        g, display_limit = graphs.get_account_monthly_balances(account, datetime.date.today())
        self.assertFalse(display_limit)

        # max value above 0.5*balance limit
        account.balance_limit = 150
        g, display_limit = graphs.get_account_monthly_balances(account, datetime.date.today())
        self.assertTrue(display_limit)

        # Account with an initial amount:
        account.initial_amount = 100
        account.save()
        g, display_limit = graphs.get_account_monthly_balances(account, datetime.date(2020, 12, 1))
        self.assertEqual(str(g[0]['period']), "December 2019")
        self.assertEqual(g[0]['balance'], -4.88)
        self.assertEqual(g[0]['global_balance'], -4.88+account.initial_amount)
        self.assertEqual(str(g[1]['period']), "January 2020")
        self.assertEqual(g[1]['balance'], 100)
        self.assertEqual(g[1]['global_balance'], 95.12+account.initial_amount)

        # Account with a support amount
        account.initial_amount = 0
        account.support_amount = 200
        account.save()
        g, display_limit = graphs.get_account_monthly_balances(account, datetime.date.today())
        self.assertEqual(str(g[0]['period']), "December 2019")
        self.assertEqual(g[0]['balance'], -4.88+account.support_amount)
        self.assertEqual(g[0]['global_balance'], -4.88+account.support_amount)
        self.assertEqual(str(g[1]['period']), "January 2020")
        self.assertEqual(g[1]['balance'], 100)
        self.assertEqual(g[1]['global_balance'], 95.12+account.support_amount)

    def test_get_all_accounts_monthly_balances(self):
        account = self._generate_fixtures()

        g = graphs.get_all_accounts_monthly_balances(datetime.date.today())
        self.assertEqual(len(g), 3)
        self.assertEqual(str(g[0]['period']), "December 2019")
        self.assertEqual(g[0]['balance'], -1.76)
        self.assertEqual(g[0]['global_balance'], -1.76)
        self.assertEqual(str(g[1]['period']), "January 2020")
        self.assertEqual(g[1]['balance'], 100)
        self.assertEqual(g[1]['global_balance'], 98.24)
        self.assertEqual(str(g[2]['period']), "December 2020")
        self.assertEqual(g[2]['balance'], 11.16)
        self.assertEqual(g[2]['global_balance'], 109.40)

        # Previous month:
        g = graphs.get_all_accounts_monthly_balances(datetime.date(2020, 12, 1))
        self.assertEqual(len(g), 2)
        self.assertEqual(str(g[0]['period']), "December 2019")
        self.assertEqual(g[0]['balance'], -1.76)
        self.assertEqual(g[0]['global_balance'], -1.76)
        self.assertEqual(str(g[1]['period']), "January 2020")
        self.assertEqual(g[1]['balance'], 100)
        self.assertEqual(g[1]['global_balance'], 98.24)

        # One account with an initial amount:
        account.initial_amount = 100
        account.save()
        g = graphs.get_all_accounts_monthly_balances(datetime.date.today())
        self.assertEqual(len(g), 3)
        self.assertEqual(str(g[0]['period']), "December 2019")
        self.assertEqual(g[0]['balance'], -1.76)
        self.assertEqual(g[0]['global_balance'], -1.76+account.initial_amount)
        self.assertEqual(str(g[1]['period']), "January 2020")
        self.assertEqual(g[1]['balance'], 100)
        self.assertEqual(g[1]['global_balance'], 98.24+account.initial_amount)
        self.assertEqual(str(g[2]['period']), "December 2020")
        self.assertEqual(g[2]['balance'], 11.16)
        self.assertEqual(g[2]['global_balance'], 109.40+account.initial_amount)

        # Second account with an initial amount, opened later:
        new_account = Account(number=125, initial_amount=200)
        new_account.save()
        o = Operation(
            bank_date="2020-07-20",
            date_paid="2020-07-20",
            amount=24,
            account=new_account
        )
        o.save()
        g = graphs.get_all_accounts_monthly_balances(datetime.date.today())
        self.assertEqual(len(g), 4)
        self.assertEqual(str(g[0]['period']), "December 2019")
        self.assertEqual(g[0]['balance'], -1.76)
        self.assertEqual(g[0]['global_balance'], -1.76+account.initial_amount)
        self.assertEqual(str(g[1]['period']), "January 2020")
        self.assertEqual(g[1]['balance'], 100)
        self.assertEqual(g[1]['global_balance'], 98.24+account.initial_amount)
        self.assertEqual(str(g[2]['period']), "July 2020")
        self.assertEqual(g[2]['balance'], o.amount)
        final_amount = o.amount+account.initial_amount+new_account.initial_amount
        self.assertEqual(g[2]['global_balance'], 98.24+final_amount)
        self.assertEqual(str(g[3]['period']), "December 2020")
        self.assertEqual(g[3]['balance'], 11.16)
        self.assertEqual(g[3]['global_balance'], 109.40+final_amount)

    def test_get_all_accounts_yearly_balances(self):
        account = self._generate_fixtures()

        g = graphs.get_all_accounts_yearly_balances(datetime.date.today())
        self.assertEqual(len(g), 2)
        self.assertEqual(str(g[0]['period']), "2019")
        self.assertEqual(g[0]['balance'], -1.76)
        self.assertEqual(g[0]['global_balance'], -1.76)
        self.assertEqual(str(g[1]['period']), "2020")
        self.assertEqual(g[1]['balance'], 111.16)
        self.assertEqual(g[1]['global_balance'], 109.4)

        # Previous year:
        g = graphs.get_all_accounts_yearly_balances(datetime.date(2020, 1, 1))
        self.assertEqual(len(g), 1)
        self.assertEqual(str(g[0]['period']), "2019")
        self.assertEqual(g[0]['balance'], -1.76)
        self.assertEqual(g[0]['global_balance'], -1.76)

        # One account with an initial amount:
        account.initial_amount = 100
        account.save()
        g = graphs.get_all_accounts_yearly_balances(datetime.date.today())
        self.assertEqual(len(g), 2)
        self.assertEqual(str(g[0]['period']), "2019")
        self.assertEqual(g[0]['balance'], -1.76)
        self.assertEqual(g[0]['global_balance'], -1.76+account.initial_amount)
        self.assertEqual(str(g[1]['period']), "2020")
        self.assertEqual(g[1]['balance'], 111.16)
        self.assertEqual(g[1]['global_balance'], 109.4+account.initial_amount)

        # Second account with an initial amount, opened later:
        new_account = Account(number=125, initial_amount=200)
        new_account.save()
        o = Operation(
            bank_date="2020-07-20",
            date_paid="2020-07-20",
            amount=24,
            account=new_account
        )
        o.save()
        g = graphs.get_all_accounts_yearly_balances(datetime.date.today())
        self.assertEqual(len(g), 2)
        self.assertEqual(str(g[0]['period']), "2019")
        self.assertEqual(g[0]['balance'], -1.76)
        self.assertEqual(g[0]['global_balance'], -1.76+account.initial_amount)
        self.assertEqual(str(g[1]['period']), "2020")
        self.assertEqual(g[1]['balance'], 111.16+o.amount)
        self.assertEqual(
            g[1]['global_balance'],
            109.4+o.amount+account.initial_amount+new_account.initial_amount
        )

        # This account has now a support amount, opened later:
        new_account.initial_amount = 0
        new_account.support_amount = 200
        new_account.save()
        g = graphs.get_all_accounts_yearly_balances(datetime.date.today())
        self.assertEqual(len(g), 2)
        self.assertEqual(str(g[0]['period']), "2019")
        self.assertEqual(g[0]['balance'], -1.76)
        self.assertEqual(g[0]['global_balance'], -1.76+account.initial_amount)
        self.assertEqual(str(g[1]['period']), "2020")
        self.assertEqual(g[1]['balance'], round(111.16+o.amount+new_account.support_amount, 2))
        self.assertEqual(
            g[1]['global_balance'],
            109.4+o.amount+account.initial_amount+new_account.support_amount
        )

    def test_get_account_categories_expenses_repartition(self):
        account = Account(number=123)
        account.save()

        account_bis = Account(number=124)
        account_bis.save()

        c1 = Category(title="c1", auto_affect="")
        c2 = Category(title="c2", auto_affect="foo")
        Category.objects.bulk_create([c1, c2])
        categories = Category.objects.all()

        # wrong period:
        o = Operation(
            bank_date="2019-11-04",
            date_paid="2019-11-04",
            amount=-3.12,
            account=account
        )
        o.save()
        o = Operation(
            bank_date="2020-01-01",
            date_paid="2020-01-01",
            amount=-3.12,
            account=account
        )
        o.save()

        # wrong account:
        o = Operation(
            bank_date="2019-12-04",
            date_paid="2019-12-04",
            amount=-3.12,
            account=account_bis
        )
        o.save()

        # income (instead of expense):
        o = Operation(
            bank_date="2019-12-04",
            date_paid="2019-12-04",
            amount=3.12,
            account=account_bis
        )
        o.save()

        # pending operation
        o = Operation(
            date_paid="2019-12-04",
            amount=-3.12,
            account=account_bis
        )
        o.save()

        g = graphs.get_account_categories_expenses_repartition(
            account, datetime.date(2019, 12, 1), datetime.date(2020, 1, 1)
        )
        self.assertEqual(len(g), 0)

        # normal
        o = Operation(
            bank_date="2019-12-04",
            date_paid="2019-12-04",
            amount=-3.12,
            account=account
        )
        o.save()

        g = graphs.get_account_categories_expenses_repartition(
            account, datetime.date(2019, 12, 1), datetime.date(2020, 1, 1)
        )
        self.assertEqual(len(g), 1)
        self.assertEqual(g[0]['categories'], [
            {'id': Category.without_category().pk, 'title': Category.without_category().title},
        ])
        self.assertEqual(g[0]['balance'], -3.12)

        o = Operation(
            bank_date="2019-12-07",
            date_paid="2019-12-06",
            amount=-5.12,
            account=account
        )
        o.save()
        o.categories.add(categories[0])
        o.save()

        o = Operation(
            bank_date="2019-12-07",
            date_paid="2019-12-06",
            amount=-5.16,
            account=account
        )
        o.save()
        o.categories.add(categories[1])
        o.save()

        o = Operation(
            bank_date="2019-12-13",
            date_paid="2019-12-13",
            amount=-5.65,
            account=account
        )
        o.save()
        o.categories.add(categories[1])
        o.save()

        o = Operation(
            bank_date="2019-12-14",
            date_paid="2019-12-14",
            amount=-10.65,
            account=account
        )
        o.save()
        o.categories.add(categories[0])
        o.categories.add(categories[1])
        o.save()

        o = Operation(
            bank_date="2019-12-15",
            date_paid="2019-12-15",
            amount=-10,
            account=account
        )
        o.save()
        o.categories.add(categories[0])
        o.categories.add(categories[1])
        o.save()

        g = graphs.get_account_categories_expenses_repartition(
            account, datetime.date(2019, 12, 1), datetime.date(2020, 1, 1)
        )
        self.assertEqual(len(g), 4)
        self.assertEqual(g[0]['categories'], [
            {'id': Category.without_category().pk, 'title': Category.without_category().title},
        ])
        self.assertEqual(g[0]['balance'], -3.12)
        self.assertEqual(
            g[1]['categories'],
            [{'id': categories[0].pk, 'title': categories[0].title}]
        )
        self.assertEqual(g[1]['balance'], -5.12)
        self.assertEqual(
            g[2]['categories'],
            [{'id': categories[1].pk, 'title': categories[1].title}]
        )
        self.assertEqual(g[2]['balance'], -10.81)
        self.assertEqual(
            g[3]['categories'],
            [
                {'id': categories[0].pk, 'title': categories[0].title},
                {'id': categories[1].pk, 'title': categories[1].title}
            ]
        )
        self.assertEqual(g[3]['balance'], -20.65)
        self.assertEqual(g[3]['title'], categories[0].title + "/" + categories[1].title)

    def test_get_account_categories_incomes_repartition(self):
        account = Account(number=123)
        account.save()

        account_bis = Account(number=124)
        account_bis.save()

        c1 = Category(title="c1", auto_affect="")
        c2 = Category(title="c2", auto_affect="foo")
        Category.objects.bulk_create([c1, c2])
        categories = Category.objects.all()

        # wrong period:
        o = Operation(
            bank_date="2019-11-04",
            date_paid="2019-11-04",
            amount=3.12,
            account=account
        )
        o.save()
        o = Operation(
            bank_date="2020-01-01",
            date_paid="2020-01-01",
            amount=3.12,
            account=account
        )
        o.save()

        # wrong account:
        o = Operation(
            bank_date="2019-12-04",
            date_paid="2019-12-04",
            amount=3.12,
            account=account_bis
        )
        o.save()

        # expense (instead of income):
        o = Operation(
            bank_date="2019-12-04",
            date_paid="2019-12-04",
            amount=-3.12,
            account=account_bis
        )
        o.save()

        # pending operation
        o = Operation(
            date_paid="2019-12-04",
            amount=3.12,
            account=account_bis
        )
        o.save()

        g = graphs.get_account_categories_incomes_repartition(
            account, datetime.date(2019, 12, 1), datetime.date(2020, 1, 1)
        )
        self.assertEqual(len(g), 0)

        # normal
        o = Operation(
            bank_date="2019-12-04",
            date_paid="2019-12-04",
            amount=3.12,
            account=account
        )
        o.save()

        g = graphs.get_account_categories_incomes_repartition(
            account, datetime.date(2019, 12, 1), datetime.date(2020, 1, 1)
        )
        self.assertEqual(len(g), 1)
        self.assertEqual(g[0]['categories'], [
            {'id': Category.without_category().pk, 'title': Category.without_category().title},
        ])
        self.assertEqual(g[0]['balance'], 3.12)

        o = Operation(
            bank_date="2019-12-07",
            date_paid="2019-12-06",
            amount=5.12,
            account=account
        )
        o.save()
        o.categories.add(categories[0])
        o.save()

        o = Operation(
            bank_date="2019-12-07",
            date_paid="2019-12-06",
            amount=5.16,
            account=account
        )
        o.save()
        o.categories.add(categories[1])
        o.save()

        o = Operation(
            bank_date="2019-12-13",
            date_paid="2019-12-13",
            amount=5.65,
            account=account
        )
        o.save()
        o.categories.add(categories[1])
        o.save()

        o = Operation(
            bank_date="2019-12-14",
            date_paid="2019-12-14",
            amount=10.65,
            account=account
        )
        o.save()
        o.categories.add(categories[0])
        o.categories.add(categories[1])
        o.save()

        o = Operation(
            bank_date="2019-12-15",
            date_paid="2019-12-15",
            amount=10,
            account=account
        )
        o.save()
        o.categories.add(categories[0])
        o.categories.add(categories[1])
        o.save()

        g = graphs.get_account_categories_incomes_repartition(
            account, datetime.date(2019, 12, 1), datetime.date(2020, 1, 1)
        )
        self.assertEqual(len(g), 4)
        self.assertEqual(g[0]['categories'], [
            {'id': Category.without_category().pk, 'title': Category.without_category().title},
        ])
        self.assertEqual(g[0]['balance'], 3.12)
        self.assertEqual(
            g[1]['categories'],
            [{'id': categories[0].pk, 'title': categories[0].title}]
        )
        self.assertEqual(g[1]['balance'], 5.12)
        self.assertEqual(
            g[2]['categories'],
            [{'id': categories[1].pk, 'title': categories[1].title}]
        )
        self.assertEqual(g[2]['balance'], 10.81)
        self.assertEqual(
            g[3]['categories'],
            [
                {'id': categories[0].pk, 'title': categories[0].title},
                {'id': categories[1].pk, 'title': categories[1].title}
            ]
        )
        self.assertEqual(g[3]['balance'], 20.65)
        self.assertEqual(g[3]['title'], categories[0].title + "/" + categories[1].title)

    def test_get_category_monthly_balance(self):
        account = Account(number=123)
        account.save()

        account_bis = Account(number=124)
        account_bis.save()

        c0 = Category(title="c0", auto_affect="")
        c1 = Category(title="c1", auto_affect="foo")
        Category.objects.bulk_create([c0, c1])
        categories = Category.objects.all()

        o = Operation(
            bank_date="2019-12-04",
            date_paid="2019-12-04",
            amount=3.12,
            account=account_bis
        )
        o.save()
        o.categories.add(categories[0])
        o.categories.add(categories[1])
        o.save()

        o = Operation(
            bank_date="2019-12-04",
            date_paid="2019-12-04",
            amount=3.12,
            account=account
        )
        o.save()
        o.categories.add(categories[0])
        o.save()

        o = Operation(
            date_paid="2019-12-04",
            amount=3.12,
            account=account
        )
        o.save()
        o.categories.add(categories[0])
        o.save()

        o = Operation(
            bank_date="2019-12-04",
            date_paid="2019-12-04",
            amount=7.12,
            account=account
        )
        o.save()
        o.categories.add(categories[1])
        o.save()

        o = Operation(
            bank_date="2020-12-13",
            date_paid="2020-12-13",
            amount=-15.12,
            account=account
        )
        o.save()

        o = Operation(
            bank_date="2020-12-20",
            date_paid="2020-12-20",
            amount=11.16,
            account=account
        )
        o.save()

        o = Operation(
            bank_date="2020-01-20",
            date_paid="2020-01-20",
            amount=200,
            account=account
        )
        o.save()

        o = Operation(
            bank_date="2020-01-20",
            date_paid="2020-01-20",
            amount=100,
            account=account
        )
        o.save()
        o.categories.add(categories[0])
        o.save()

        g = graphs.get_category_monthly_balance(categories[0])
        self.assertEqual(len(g), 2)
        self.assertEqual(str(g[0]['month']), "December 2019")
        self.assertEqual(g[0]['balance'], 6.24)
        self.assertEqual(str(g[1]['month']), "January 2020")
        self.assertEqual(g[1]['balance'], 100)

        g = graphs.get_category_monthly_balance(Category.without_category())
        self.assertEqual(len(g), 2)
        self.assertEqual(str(g[0]['month']), "January 2020")
        self.assertEqual(g[0]['balance'], 200)
        self.assertEqual(str(g[1]['month']), "December 2020")
        self.assertEqual(g[1]['balance'], -3.96)

    def test_get_category_repartition(self):
        account = Account(number=123)
        account.save()

        account_bis = Account(number=124)
        account_bis.save()

        account_ter = Account(number=125)
        account_ter.save()

        account_quad = Account(number=126)
        account_quad.save()

        c0 = Category(title="c0", auto_affect="")
        c1 = Category(title="c1", auto_affect="foo")
        Category.objects.bulk_create([c0, c1])
        categories = Category.objects.all()

        o = Operation(
            bank_date="2019-12-04",
            date_paid="2019-12-04",
            amount=3.12,
            account=account_bis
        )
        o.save()
        o.categories.add(categories[0])
        o.categories.add(categories[1])
        o.save()

        o = Operation(
            bank_date="2019-12-04",
            date_paid="2019-12-04",
            amount=3.12,
            account=account
        )
        o.save()
        o.categories.add(categories[0])
        o.save()

        o = Operation(
            date_paid="2019-12-04",
            amount=3.12,
            account=account
        )
        o.save()
        o.categories.add(categories[0])
        o.save()

        o = Operation(
            bank_date="2019-12-04",
            date_paid="2019-12-04",
            amount=6.12,
            account=account
        )
        o.save()
        o.categories.add(categories[0])
        o.save()

        o = Operation(
            bank_date="2019-12-04",
            date_paid="2019-12-04",
            amount=7.12,
            account=account
        )
        o.save()
        o.categories.add(categories[1])
        o.save()

        o = Operation(
            bank_date="2019-12-20",
            date_paid="2019-12-20",
            amount=15.12,
            account=account
        )
        o.save()

        o = Operation(
            bank_date="2019-12-20",
            date_paid="2019-12-20",
            amount=14.24,
            account=account_bis
        )
        o.save()

        o = Operation(
            bank_date="2020-12-20",
            date_paid="2020-12-20",
            amount=-11.16,
            account=account_ter
        )
        o.save()
        o.categories.add(categories[0])
        o.save()

        # These two operations cancel each other,
        # so there is no money from categories[0] on account_quad:
        o = Operation(
            bank_date="2020-12-20",
            date_paid="2020-12-20",
            amount=-11.16,
            account=account_quad
        )
        o.save()
        o.categories.add(categories[0])
        o.save()
        o = Operation(
            bank_date="2020-12-21",
            date_paid="2020-12-21",
            amount=11.16,
            account=account_quad
        )
        o.save()
        o.categories.add(categories[0])
        o.save()

        g = graphs.get_category_repartition(categories[0])
        self.assertEqual(len(g), 3)
        self.assertEqual(g[0]['account'], account.id)
        self.assertEqual(g[0]['balance'], 9.24)
        self.assertEqual(g[1]['account'], account_bis.id)
        self.assertEqual(g[1]['balance'], 3.12)
        self.assertEqual(g[2]['account'], account_ter.id)
        self.assertEqual(g[2]['balance'], -11.16)

        g = graphs.get_category_repartition(Category.without_category())
        self.assertEqual(len(g), 2)
        self.assertEqual(g[0]['account'], account.id)
        self.assertEqual(g[0]['balance'], 15.12)
        self.assertEqual(g[1]['account'], account_bis.id)
        self.assertEqual(g[1]['balance'], 14.24)
