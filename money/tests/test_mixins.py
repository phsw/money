import datetime

from django.test import RequestFactory, TestCase
from django.views.generic import TemplateView

from money.mixins import SelectMonthMixin, ListDestAccountsToCloneOperationMixin
from money.models.accounts import Account


class SelectMonthMixinTest(TestCase):
    class DummyView(SelectMonthMixin, TemplateView):
        pass

    def test_compute_months(self):
        today = datetime.date.today()

        # Default: no month given
        v = self.DummyView(request=RequestFactory().get('/'))
        context = v.get_context_data()

        self.assertEqual(v.asked_date, today)
        self.assertEqual(context['asked_date'], today)
        self.assertEqual(v.begin_asked_month, datetime.date(
            today.year, today.month, 1
        ))
        self.assertFalse(context['display_next_month_button'])

        # Given month in the middle of the year:
        v = self.DummyView(request=RequestFactory().get('/?date=July 2020'))
        context = v.get_context_data()

        self.assertEqual(v.asked_date, datetime.date(2020, 7, 1))
        self.assertEqual(v.begin_asked_month, datetime.date(2020, 7, 1))
        self.assertEqual(v.end_asked_month, datetime.date(2020, 8, 1))
        self.assertEqual(context['asked_date'], datetime.date(2020, 7, 1))
        self.assertEqual(context['previous_month'].year, 2020)
        self.assertEqual(context['previous_month'].month, 6)
        self.assertEqual(context['next_month'].year, 2020)
        self.assertEqual(context['next_month'].month, 8)
        self.assertTrue(context['display_next_month_button'])

        # January (previous month will be in previous year):
        v = self.DummyView(request=RequestFactory().get('/?date=January 2020'))
        context = v.get_context_data()

        self.assertEqual(v.asked_date, datetime.date(2020, 1, 1))
        self.assertEqual(v.begin_asked_month, datetime.date(2020, 1, 1))
        self.assertEqual(v.end_asked_month, datetime.date(2020, 2, 1))
        self.assertEqual(context['asked_date'], datetime.date(2020, 1, 1))
        self.assertEqual(context['previous_month'].year, 2019)
        self.assertEqual(context['previous_month'].month, 12)
        self.assertEqual(context['next_month'].year, 2020)
        self.assertEqual(context['next_month'].month, 2)
        self.assertTrue(context['display_next_month_button'])

        # December (next month will be in next year):
        v = self.DummyView(request=RequestFactory().get('/?date=December 2020'))
        context = v.get_context_data()

        self.assertEqual(v.asked_date, datetime.date(2020, 12, 1))
        self.assertEqual(v.begin_asked_month, datetime.date(2020, 12, 1))
        self.assertEqual(v.end_asked_month, datetime.date(2021, 1, 1))
        self.assertEqual(context['asked_date'], datetime.date(2020, 12, 1))
        self.assertEqual(context['previous_month'].year, 2020)
        self.assertEqual(context['previous_month'].month, 11)
        self.assertEqual(context['next_month'].year, 2021)
        self.assertEqual(context['next_month'].month, 1)
        self.assertTrue(context['display_next_month_button'])

        # Current month is given
        v = self.DummyView(request=RequestFactory().get('/?date=' + today.strftime("%B %Y")))
        context = v.get_context_data()

        self.assertEqual(v.asked_date, datetime.date(today.year, today.month, 1))
        self.assertFalse(context['display_next_month_button'])


class ListDestAccountsToCloneOperationMixinTest(TestCase):
    class DummyView(ListDestAccountsToCloneOperationMixin, TemplateView):
        pass

    def test_list_dest_accounts(self):
        account1 = Account(title="foo", number=1)
        account1.save()
        account2 = Account(title="bar", number=2)
        account2.save()
        account3 = Account(title="odd one out", number=3, no_pendings=True)
        account3.save()

        v = self.DummyView()
        self.assertQuerysetEqual(
            v.get_context_data()['dest_accounts_clone_operation'],
            [account2, account1]
        )
