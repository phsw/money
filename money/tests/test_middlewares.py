import datetime

from django.test import TestCase

from money.models.import_authorization import ImportAuthorization


class UpdateImportTimeMiddlewareTest(TestCase):
    def test_correct_remaining_time(self):
        session = self.client.session  # also initialize session engine of the client

        # Default: no import grant
        self.client.get('/')
        self.assertNotIn('import_remaining_seconds', self.client.session)

        # An import grant:
        grant = ImportAuthorization(key='foo')
        grant.save()

        self.client.get('/')
        self.assertIn('import_remaining_seconds', self.client.session)

        # An expired grant:
        grant.date -= datetime.timedelta(days=10)
        grant.save()

        self.client.get('/')
        self.assertNotIn('import_remaining_seconds', self.client.session)

        # No grant, but the session key is present:
        grant.delete()

        session['import_remaining_seconds'] = 42
        session.save()
        self.client.get('/')
        self.assertNotIn('import_remaining_seconds', self.client.session)
