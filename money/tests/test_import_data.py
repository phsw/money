from django.test import TestCase
import bs4
import datetime

from money.utils.import_data import ImportWebBankOperations, AccountType, ImportCsvOperations,\
        ImportCsvBankOperations, ProcessImportBankOperations, CANE2019BankWebParser,\
        CANE2020DefaultBankWebParser, CANE2020EpargneBankWebParser, CANE2020LongTermeBankWebParser, \
        CANE2023EpargneBankWebParser, CANE2024EpargneBankWebParser
from money.models.operations import Operation
from money.models.accounts import Account

# We are ignoring long line errors because HTML snippets can be very long:
# flake8: noqa: E501


class TestProcessImportBankOperations(TestCase):
    def test_find_date_indication(self):
        o = Operation(
            bank_date="2019-12-14",
            bank_description="Bank description"
        )
        self.assertIsNone(ProcessImportBankOperations._find_date_indication(o))

        o = Operation(
            bank_date="2019-12-14",
            bank_description="Bank 14/10 description"
        )
        self.assertIsNone(ProcessImportBankOperations._find_date_indication(o))

        o = Operation(
            bank_date="2019-12-14",
            bank_description="Bank description a/b"
        )
        self.assertIsNone(ProcessImportBankOperations._find_date_indication(o))

        o = Operation(
            bank_date="2019-12-14",
            bank_description="Bank description 12/13"
        )
        self.assertIsNone(ProcessImportBankOperations._find_date_indication(o))

        o = Operation(
            bank_date="2019-12-14",
            bank_description="Bank description 42/10"
        )
        self.assertIsNone(ProcessImportBankOperations._find_date_indication(o))

        o = Operation(
            bank_date="2019-12-14",
            bank_description="Bank description 13/12"
        )
        self.assertEqual(str(ProcessImportBankOperations._find_date_indication(o)), "2019-12-13")

        o = Operation(
            bank_date="2019-01-02",
            bank_description="Bank description 30/12"
        )
        self.assertEqual(str(ProcessImportBankOperations._find_date_indication(o)), "2018-12-30")

        o = Operation(
            bank_date="2019-08-02",
            bank_description="Bank description 29/07"
        )
        self.assertEqual(str(ProcessImportBankOperations._find_date_indication(o)), "2019-07-29")

        # test two week limit:
        o = Operation(
            bank_date="2019-08-25",
            bank_description="Bank description 05/08"
        )
        self.assertIsNone(ProcessImportBankOperations._find_date_indication(o))
        o = Operation(
            bank_date="2019-08-05",
            bank_description="Bank description 25/08"
        )
        self.assertIsNone(ProcessImportBankOperations._find_date_indication(o))

    def test_keep_only_new_operations(self):
        account = Account(number="123")
        account.save()

        import_bank_account = ProcessImportBankOperations()
        import_bank_account.type = AccountType.DEFAULT
        import_bank_account.account = account

        imported_operations = [
            Operation(
                bank_date='2018-02-22',
                date_paid='2018-02-22',
                bank_description='Stuff A',
                amount=14.18
            ),
            Operation(
                bank_date='2018-02-23',
                date_paid='2018-02-23',
                bank_description='Stuff B',
                amount=15
            ),
            Operation(
                bank_date='2018-02-23',
                date_paid='2018-02-23',
                bank_description='Stuff C',
                amount=16
            )
        ]

        import_bank_account.operations = imported_operations

        import_bank_account.keep_only_new_operations()
        self.assertEquals(len(imported_operations), len(import_bank_account.operations))

        stored_operations = [
            Operation(
                bank_date='2018-02-22',
                date_paid='2018-02-22',
                bank_description='Stuff A',
                amount=14.18,
                account=account
            ),
            Operation(
                bank_date='2018-02-23',
                date_paid='2018-02-23',
                bank_description='Stuff B',
                amount=15,
                account=account
            )
        ]

        Operation.objects.bulk_create(stored_operations)

        import_bank_account.keep_only_new_operations()
        self.assertEquals(len(import_bank_account.operations), 1)
        self.assertEquals(import_bank_account.operations[0].bank_description, "Stuff C")

        Operation.objects.all().delete()

        stored_operations = [
            Operation(
                date_paid='2019-12-05',
                bank_date='2019-12-05',
                amount=-460,
                bank_description="abc",
                bank_comment="Comment abc",
                read=False,
                method="Virement",
                account=account
            ),
            Operation(
                date_paid='2019-12-03',
                bank_date='2019-12-03',
                amount=-2.26,
                bank_description="ghi",
                bank_comment="Comment ghi",
                read=False,
                method="Prelevement",
                account=account
            )
        ]
        Operation.objects.bulk_create(stored_operations)

        import_bank_account.operations = stored_operations

        import_bank_account.keep_only_new_operations()
        self.assertEqual(len(import_bank_account.operations), 0)

        # When there are twice the same operation in a row:
        Operation.objects.all().delete()

        stored_operations = [
            Operation(
                date_paid='2019-12-05',
                bank_date='2019-12-05',
                amount=-460,
                bank_description="abc",
                bank_comment="Comment abc",
                read=False,
                method="Virement",
                account=account
            ),
            Operation(
                date_paid='2019-12-05',
                bank_date='2019-12-05',
                amount=-460,
                bank_description="abc",
                bank_comment="Comment abc",
                read=False,
                method="Virement",
                account=account
            )
        ]
        Operation.objects.bulk_create(stored_operations)

        import_bank_account.operations = [o for o in stored_operations]
        import_bank_account.keep_only_new_operations()
        self.assertEqual(len(import_bank_account.operations), 0)

        Operation.objects.all().delete()

        import_bank_account.operations = stored_operations
        import_bank_account.keep_only_new_operations()
        self.assertEqual(len(import_bank_account.operations), 2)


    def test_match_with_pendings(self):
        account = Account(number="123")
        account.save()

        import_bank_account = ProcessImportBankOperations()
        import_bank_account.type = AccountType.DEFAULT
        import_bank_account.account = account

        imported_operations = [
            Operation(
                bank_date='2018-02-22',
                date_paid='2018-02-22',
                bank_description='Bank Stuff A',
                amount=14.18
            ),
            Operation(
                bank_date='2018-02-22',
                date_paid='2018-02-22',
                bank_description='Bank Stuff B',
                amount=15
            ),
            Operation(
                bank_date='2018-02-23',
                date_paid='2018-02-23',
                bank_description='Bank Stuff C',
                amount=15
            ),
            Operation(  # to test many matching, but incorrect date
                bank_date='2019-03-23',
                date_paid='2019-03-23',
                bank_description='Bank Stuff D',
                amount=17
            ),
            Operation(  # to test many matching and an exact date
                bank_date='2019-03-23',
                date_paid='2019-03-23',
                bank_description='Bank Stuff E',
                amount=18
            ),
            Operation(  # to test matching thanks to date in bank description
                bank_date='2019-07-23',
                date_paid='2019-07-23',
                bank_description='Bank Stuff F 22/07',
                amount=19
            )
        ]

        import_bank_account.operations = imported_operations

        # DB is empty: every imported bank operation has to be created
        import_bank_account.match_with_pendings()  # not saved in DB
        self.assertEquals(len(imported_operations), len(import_bank_account.to_create))
        self.assertEquals(len(import_bank_account.to_update), 0)

        # DB contains one "matchable" pending operation:
        pending_operations = [
            Operation(  # matches with "Bank Stuff A"
                date_paid='2018-02-22',
                description='Stuff A',
                amount=14.18,
                account=account
            ),
            Operation(  # doesn't match because of amount
                date_paid='2018-02-23',
                description='Stuff B',
                amount=15.01,
                account=account
            ),
            Operation(  # doesn't match because of bank operations happen too late
                date_paid='2017-02-23',
                description='Stuff B',
                amount=15,
                account=account
            ),
            Operation(  # for "Bank Stuff D"
                date_paid='2019-03-22',
                description='Stuff D',
                amount=17,
                account=account
            ),
            Operation(  # for "Bank Stuff D"
                date_paid='2019-03-21',
                description='Stuff D',
                amount=17,
                account=account
            ),
            Operation(  # for "Bank Stuff D"
                date_paid='2019-03-23',
                description='Stuff D',
                amount=17,
                account=account
            ),
            Operation(  # for "Bank Stuff D"
                date_paid='2019-03-23',
                description='Stuff D',
                amount=17,
                account=account
            ),
            Operation(  # for "Bank Stuff E"
                date_paid='2019-03-22',
                description='Stuff E',
                amount=18,
                account=account
            ),
            Operation(  # for "Bank Stuff E"
                date_paid='2019-03-23',
                description='Stuff E',
                amount=18,
                account=account
            ),
            Operation(  # for "Bank Stuff F"
                date_paid='2019-07-22',
                description='Stuff E',
                amount=19,
                account=account
            ),
            Operation(  # for "Bank Stuff F"
                date_paid='2019-07-21',
                description='Stuff E',
                amount=19,
                account=account
            )
        ]

        Operation.objects.bulk_create(pending_operations)

        import_bank_account = ProcessImportBankOperations()
        import_bank_account.type = AccountType.DEFAULT
        import_bank_account.account = account
        import_bank_account.operations = imported_operations

        import_bank_account.match_with_pendings()
        self.assertEquals(len(import_bank_account.to_create), 5)
        self.assertEquals(len(import_bank_account.to_update), 1)

        # Test with the same amount at different dates:
        Operation.objects.all().delete()

        pending_operations = [
            Operation(
                date_paid='2018-02-18',
                description='Stuff A',
                amount=-30,
                account=account
            ),
            Operation(
                date_paid='2018-02-21',
                description='Stuff B',
                amount=-30,
                account=account
            )
        ]

        Operation.objects.bulk_create(pending_operations)

        import_bank_account = ProcessImportBankOperations()
        import_bank_account.type = AccountType.DEFAULT
        import_bank_account.account = account
        import_bank_account.operations = [Operation(
            bank_date='2018-02-21',
            date_paid='2018-02-21',
            bank_description='Bank Stuff A',
            amount=-30
        )]

        import_bank_account.match_with_pendings()
        self.assertEquals(len(import_bank_account.to_create), 1)
        self.assertEquals(len(import_bank_account.to_update), 0)

        import_bank_account = ProcessImportBankOperations()
        import_bank_account.type = AccountType.DEFAULT
        import_bank_account.account = account
        import_bank_account.operations = [Operation(
            bank_date='2018-02-18',
            date_paid='2018-02-18',
            bank_description='Bank Stuff A',
            amount=-30
        )]

        import_bank_account.match_with_pendings()
        self.assertEquals(len(import_bank_account.to_create), 0)
        self.assertEquals(len(import_bank_account.to_update), 1)

        import_bank_account = ProcessImportBankOperations()
        import_bank_account.type = AccountType.DEFAULT
        import_bank_account.account = account
        import_bank_account.operations = [
            Operation(
                bank_date='2018-02-21',
                date_paid='2018-02-21',
                bank_description='Bank Stuff A',
                amount=-30
            ),
            Operation(
                bank_date='2018-02-23',
                date_paid='2018-02-23',
                bank_description='Bank Stuff A',
                amount=-30
            )
        ]

        import_bank_account.match_with_pendings()
        self.assertEquals(len(import_bank_account.to_create), 2)
        self.assertEquals(len(import_bank_account.to_update), 0)

        import_bank_account = ProcessImportBankOperations()
        import_bank_account.type = AccountType.DEFAULT
        import_bank_account.account = account
        import_bank_account.operations = [
            Operation(
                bank_date='2018-02-21',
                date_paid='2018-02-21',
                bank_description='Stuff A 18/02',
                amount=-30
            ),
            Operation(
                bank_date='2018-02-23',
                date_paid='2018-02-23',
                bank_description='Bank Stuff B 21/02',
                amount=-30
            )
        ]

        import_bank_account.match_with_pendings()
        self.assertEquals(len(import_bank_account.to_create), 0)
        self.assertEquals(len(import_bank_account.to_update), 2)

        Operation.objects.all().delete()

        pending_operations = [
            Operation(
                date_paid='2018-02-18',
                description='Stuff A',
                amount=-30,
                account=account
            ),
            Operation(
                date_paid='2018-02-18',
                description='Stuff B',
                amount=-30,
                account=account
            ),
            Operation(
                date_paid='2018-02-21',
                description='Stuff C',
                amount=-30,
                account=account
            )
        ]

        Operation.objects.bulk_create(pending_operations)

        import_bank_account = ProcessImportBankOperations()
        import_bank_account.type = AccountType.DEFAULT
        import_bank_account.account = account
        import_bank_account.operations = [Operation(
            bank_date='2018-02-21',
            date_paid='2018-02-21',
            bank_description='Bank Stuff A',
            amount=-30
        )]

        import_bank_account.match_with_pendings()
        self.assertEquals(len(import_bank_account.to_create), 1)
        self.assertEquals(len(import_bank_account.to_update), 0)

        import_bank_account = ProcessImportBankOperations()
        import_bank_account.type = AccountType.DEFAULT
        import_bank_account.account = account
        import_bank_account.operations = [Operation(
            bank_date='2018-02-18',
            date_paid='2018-02-18',
            bank_description='Bank Stuff A',
            amount=-30
        )]

        import_bank_account.match_with_pendings()
        self.assertEquals(len(import_bank_account.to_create), 1)
        self.assertEquals(len(import_bank_account.to_update), 0)

    def test_import_datetime(self):
        # test if import datetime is correct when account has disabled pending
        # operations:
        account = Account(number="123")
        account.no_pendings = True
        account.save()

        import_bank_account = ProcessImportBankOperations()
        import_bank_account.type = AccountType.DEFAULT
        import_bank_account.account = account

        imported_operations = [
            Operation(
                bank_date='2018-02-22',
                date_paid='2018-02-22',
                bank_description='Bank Stuff A',
                amount=14.18
            )
        ]

        import_bank_account.operations = imported_operations
        import_bank_account.match_with_pendings()
        import_bank_account.save()
        self.assertIsNotNone(imported_operations[0].import_datetime)
        self.assertLess(
            datetime.datetime.now() - imported_operations[0].import_datetime,
            datetime.timedelta(seconds=1)
        )

        # test if import datetime is correct when account has pending
        # operations and none match:
        account = Account(number="124")
        account.save()

        import_bank_account = ProcessImportBankOperations()
        import_bank_account.type = AccountType.DEFAULT
        import_bank_account.account = account

        imported_operations = [
            Operation(
                bank_date='2018-02-23',
                date_paid='2018-02-23',
                bank_description='Bank Stuff B',
                amount=14.19
            )
        ]

        import_bank_account.operations = imported_operations
        import_bank_account.match_with_pendings()
        import_bank_account.save()
        self.assertIsNotNone(imported_operations[0].import_datetime)
        self.assertLess(
            datetime.datetime.now() - imported_operations[0].import_datetime,
            datetime.timedelta(seconds=1)
        )

        # test if import datetime is correct when account has pending
        # operation and one match:
        account = Account(number="125")
        account.save()

        pending_operations = [
            Operation(  # match with "Bank Stuff A"
                date_paid='2018-02-24',
                description='Stuff C',
                amount=14.20,
                account=account
            )
        ]

        Operation.objects.bulk_create(pending_operations)

        imported_operations = [
            Operation(
                bank_date='2018-02-24',
                date_paid='2018-02-24',
                bank_description='Bank Stuff C',
                amount=14.20
            )
        ]

        import_bank_account = ProcessImportBankOperations()
        import_bank_account.type = AccountType.DEFAULT
        import_bank_account.account = account
        import_bank_account.operations = imported_operations

        import_bank_account.match_with_pendings()
        import_bank_account.save()

        pending_operation = Operation.objects.filter(account=account).first()
        self.assertIsNotNone(pending_operation.import_datetime)
        self.assertLess(
            datetime.datetime.now() - pending_operation.import_datetime,
            datetime.timedelta(seconds=1)
        )


class TestCANE2020DefaultBankWebParser(TestCase):
    def test_convert_line(self):
        import_bank_account = CANE2020DefaultBankWebParser("")

        # Normal:
        soup = bs4.BeautifulSoup(
            """
            <li class="Operation js-Operation" id="operation-detail-28">
                <a href="#None" class="Operation-header Operation-header--notDropDown js-Operation-header">
                    <div id="dateOperation" class="Operation-date" data-format="OperationDateFormatter" aria-label="Jan 20, 2020 12:00:00 AM">20 Janvier</div>
                    <div id="dateValeur" class="Operation-valueDate" data-format="OperationDateFormatter" aria-label="Jan 20, 2020 12:00:00 AM">20 Janvier</div>
                    <div class="Operation-icon npc-card"></div>
                    <div class="Operation-main">
                        <div class="Operation-type">PAIEMENT PAR CARTE      </div>
                        <div id="libelleOperation" class="Operation-name">SUPERMARCHE DE LA CAROTTE 19/01</div>
                    </div>
                    <div id="montant" class="Operation-value Operation-value--negative" aria-label="moins 11.99 €">-11,99&nbsp;€</div>
                </a>
            </li>""", features="html.parser"
        )

        operation = import_bank_account._convert_line(soup.li)

        self.assertEqual(str(operation.bank_date), "2020-01-20")
        self.assertEqual(str(operation.date_paid), "2020-01-20")
        self.assertEqual(operation.method, "Paiement par carte")
        self.assertEqual(
            operation.bank_description,
            "Supermarche de la carotte 19/01"
        )
        self.assertEqual(operation.comment, "")
        self.assertEqual(operation.amount, -11.99)
        self.assertEqual(operation.bank_import_method, Operation.BankOperationImportMethod.WEB)

        # With comment:
        soup = bs4.BeautifulSoup(
            """
            <li class="Operation js-Operation" id="operation-detail-25">
                <a href="#None" class="Operation-header js-Operation-header">
                    <div id="dateOperation" class="Operation-date" data-format="OperationDateFormatter" aria-label="Jan 27, 2020 12:00:00 AM">27 Janvier</div>
                    <div id="dateValeur" class="Operation-valueDate" data-format="OperationDateFormatter" aria-label="Jan 27, 2020 12:00:00 AM">27 Janvier</div>
                    <div class="Operation-icon npc-transfer"></div>
                    <div class="Operation-main">
                        <div class="Operation-type">VIREMENT EN VOTRE FAVEUR</div>
                        <div id="libelleOperation" class="Operation-name">ENTREPRISE GENIALE</div>
                    </div>
                    <div id="montant" class="Operation-value Operation-value--positive" aria-label="1258.27 €">+ 1&nbsp;258,27&nbsp;€</div>
                </a>
                <div class="Operation-list">

                    <div class="Operation-item" id="operation-supp-25" data-loaded="true" data-displayed-operation-index="25">
                        <div class="Operation-date"></div>
                        <div class="Operation-valueDate"></div>
                        <div class="Operation-icon"></div>
                        <div class="Operation-main">
                            <div class="Operation-descriptionLine">Entreprise foobar</div>
                            <div class="Operation-descriptionLine"></div>
                            <div class="Operation-descriptionLine"></div>
                            <div class="Operation-descriptionLine"></div>
                            <div class="Operation-descriptionLine"></div>
                            <div class="Operation-descriptionLine">Code bizarre</div>
                        </div>
                    </div>
                </div>
            </li>""", features="html.parser"
        )
        operation = import_bank_account._convert_line(soup.li)

        self.assertEqual(str(operation.bank_date), "2020-01-27")
        self.assertEqual(str(operation.date_paid), "2020-01-27")
        self.assertEqual(operation.method, "Virement en votre faveur")
        self.assertEqual(operation.bank_description, "Entreprise geniale")
        self.assertEqual(operation.bank_comment, "Entreprise foobar\nCode bizarre")
        self.assertEqual(operation.amount, 1258.27)
        self.assertEqual(operation.bank_import_method, Operation.BankOperationImportMethod.WEB)

        # Space after id "libelleOperation"...:
        soup = bs4.BeautifulSoup(
            """
            <li class="Operation js-Operation" id="operation-detail-0">
                <a class="Operation-header Operation-header--notDropDown js-Operation-header" href="#None">
                    <div aria-label="Jul 20, 2020 12:00:00 AM" class="Operation-date" data-format="OperationDateFormatter" id="dateOperation">20 Juillet</div>
                    <div aria-label="Jul 20, 2020 12:00:00 AM" class="Operation-valueDate" data-format="OperationDateFormatter" id="dateValeur">20 Juillet</div>
                    <div class="Operation-icon npc-card"></div>
                    <div class="Operation-main">
                        <div class="Operation-type">PAIEMENT PAR CARTE      </div>
                        <div class="Operation-name" id="libelleOperation ">COIFFEUR</div>
                    </div>
                    <div aria-label="moins 33 €" class="Operation-value Operation-value--negative" id="montant">-7,56 €</div>
                </a>
            </li>""", features="html.parser"
        )
        operation = import_bank_account._convert_line(soup.li)

        self.assertEqual(str(operation.bank_date), "2020-07-20")
        self.assertEqual(str(operation.date_paid), "2020-07-20")
        self.assertEqual(operation.method, "Paiement par carte")
        self.assertEqual(operation.bank_description, "Coiffeur")
        self.assertEqual(operation.bank_comment, "")
        self.assertEqual(operation.amount, -7.56)
        self.assertEqual(operation.bank_import_method, Operation.BankOperationImportMethod.WEB)

        # Buggy operation type, so use icon class:
        soup = bs4.BeautifulSoup(
            """
            <li class="Operation js-Operation" id="operation-detail-2">
                <a href="#None" class="Operation-header js-Operation-header">
                    <div id="dateOperation" class="Operation-date" data-format="OperationDateFormatter" aria-label="Jul 17, 2020 12:00:00 AM">17 Juillet</div>
                    <div id="dateValeur" class="Operation-valueDate" data-format="OperationDateFormatter" aria-label="Jul 17, 2020 12:00:00 AM">17 Juillet</div>
                    <div class="Operation-icon npc-debit"></div>
                    <div class="Operation-main">
                        <div class="Operation-type data-content=" libelletypeoperation"=""></div>
                        <div id="libelleOperation" class="Operation-name">Phone</div>
                    </div>
                    <div id="montant" class="Operation-value Operation-value--negative" aria-label="moins 15657.99 €">-15657,99&nbsp;€</div>
                </a>
            </li>""", features="html.parser"
        )
        operation = import_bank_account._convert_line(soup.li)
        self.assertEqual(operation.method, "Prélèvement")

        # Operation is new, so there are <b> tags:
        soup = bs4.BeautifulSoup(
            """
            <li class="Operation js-Operation" id="operation-detail-0">
                <a class="Operation-header js-Operation-header" href="#None">
                    <div aria-label="Jul 29, 2020 12:00:00 AM" class="Operation-date" data-format="OperationDateFormatter" id="dateOperation"><b>29 Juillet</b></div>
                    <div aria-label="Jul 29, 2020 12:00:00 AM" class="Operation-valueDate" data-format="OperationDateFormatter" id="dateValeur"><b>29 Juillet</b></div>
                    <div class="Operation-icon npc-debit"></div>
                    <div class="Operation-main">
                        <div class="Operation-type data-content=" libelletypeoperation"=""><b></b></div>
                        <div class="Operation-name" id="libelleOperation"><b>ABC</b></div>
                    </div>
                    <div aria-label="moins 33.33 €" class="Operation-value Operation-value--negative" id="montant"><b>-33,33 €</b></div>
                </a>
            </li>""", features="html.parser"
        )
        operation = import_bank_account._convert_line(soup.li)

        self.assertEqual(str(operation.bank_date), "2020-07-29")
        self.assertEqual(str(operation.date_paid), "2020-07-29")
        self.assertEqual(operation.method, "Prélèvement")
        self.assertEqual(operation.bank_description, "Abc")
        self.assertEqual(operation.bank_comment, "")
        self.assertEqual(operation.amount, -33.33)
        self.assertEqual(operation.bank_import_method, Operation.BankOperationImportMethod.WEB)

    def test_parse(self):
        account = Account(number=123456789)
        account.save()

        with open('fixtures/cane2020default.html', 'rb') as f:
            import_bank_account = ImportWebBankOperations(f.read(), 'cane2020default')
            import_bank_account.parse()

        self.assertEqual(import_bank_account.account.number, 123456789)
        self.assertEqual(len(import_bank_account.operations), 2)
        self.assertEqual(str(import_bank_account.account.balance_date), "2020-03-07")
        self.assertEqual(import_bank_account.account.balance, 234.56)

    def test_parse_date(self):
        # Without comma after year:
        self.assertEqual(str(CANE2020DefaultBankWebParser._parse_date("Jul 29, 2020 12:00:00 AM")), "2020-07-29")
        # With comma after year:
        self.assertEqual(str(CANE2020DefaultBankWebParser._parse_date("Jul 29, 2020, 12:00:00 AM")), "2020-07-29")
        # Should raise an exception:
        with self.assertRaises(Exception):
            CANE2020DefaultBankWebParser._parse_date("foo")


class TestCANE2020EpargneBankWebParser(TestCase):
    def test_convert_line(self):
        import_bank_account = CANE2020EpargneBankWebParser("")
        today = datetime.date(2020, 3, 13)

        # Normal:
        soup = bs4.BeautifulSoup(
            """
            <div class="col-xs-12 avec-lignes no-pad-lr">
                    <div class="col-xs-3">31/12</div>
                    <div class="col-xs-5">
                        Interets Crediteurs                 
                        <br>
                        De L'annee Taux  0,750%            


                    </div>
                    <div class="col-xs-4 cell-amount">123,45</div>
            </div>
            """, features="html.parser"
        )

        operation = import_bank_account._convert_line(soup.div, today)

        self.assertEqual(str(operation.bank_date), "2019-12-31")
        self.assertEqual(str(operation.date_paid), "2019-12-31")
        self.assertEqual(operation.method, "Interets Crediteurs")
        self.assertEqual(
            operation.bank_description,
            "De L'annee Taux  0,750%"
        )
        self.assertEqual(operation.comment, "")
        self.assertEqual(operation.amount, 123.45)
        self.assertEqual(operation.bank_import_method, Operation.BankOperationImportMethod.WEB)

        # With date:
        soup = bs4.BeautifulSoup(
            """
            <div class="col-xs-12 avec-lignes no-pad-lr">
                    <div class="col-xs-3">30/12</div>
                    <div class="col-xs-5">
                        Virement                           
                        <br>
                        Web Motif Virement         



                        <br>
                        30/12/2019

                    </div>
                    <div class="col-xs-4 cell-amount" data-negative="">
                        -&nbsp;920,00
                    </div>
            </div>
            """, features="html.parser"
        )
        operation = import_bank_account._convert_line(soup.div, today)

        self.assertEqual(str(operation.bank_date), "2019-12-30")
        self.assertEqual(str(operation.date_paid), "2019-12-30")
        self.assertEqual(operation.method, "Virement")
        self.assertEqual(operation.bank_description, "Web Motif Virement")
        self.assertEqual(operation.bank_comment, "")
        self.assertEqual(operation.amount, -920)
        self.assertEqual(operation.bank_import_method, Operation.BankOperationImportMethod.WEB)

        # With date and comment:
        soup = bs4.BeautifulSoup(
            """
            <div class="col-xs-12 avec-lignes no-pad-lr">
                    <div class="col-xs-3">16/10</div>
                    <div class="col-xs-5">
                        Virement                           
                        <br>
                        Virement avec commentaire    



                        <br>
                        Commentaire

                        <br>
                        16/10/2019

                    </div>
                    <div class="col-xs-4 cell-amount" data-negative="">
                        -&nbsp;150,00
                    </div>
            </div>
            """, features="html.parser"
        )
        operation = import_bank_account._convert_line(soup.div, today)

        self.assertEqual(str(operation.bank_date), "2019-10-16")
        self.assertEqual(str(operation.date_paid), "2019-10-16")
        self.assertEqual(operation.method, "Virement")
        self.assertEqual(operation.bank_description, "Virement avec commentaire")
        self.assertEqual(operation.bank_comment, "Commentaire")
        self.assertEqual(operation.amount, -150)
        self.assertEqual(operation.bank_import_method, Operation.BankOperationImportMethod.WEB)

    def test_parse(self):
        account = Account(number=123456789)
        account.save()

        with open('fixtures/cane2020epargne.html', 'rb') as f:
            import_bank_account = ImportWebBankOperations(f.read(), 'cane2020epargne')
            import_bank_account.parse()

        self.assertEqual(import_bank_account.account.number, 123456789)
        self.assertEqual(len(import_bank_account.operations), 4)
        self.assertEqual(str(import_bank_account.account.balance_date), "2020-03-12")
        self.assertEqual(import_bank_account.account.balance, 12345.67)


class TestCANE2024EpargneBankWebParser(TestCase):
    def test_convert_amount(self):
        self.assertEqual(CANE2024EpargneBankWebParser._convert_amount("12\u202f345.67"), 12_345.67)

    def test_find_operation_method_description(self):
        test_cases = [
            ("VIREMENT EN VOTRE FAVEUR Motif MACHIN", ("Virement", "Motif MACHIN")),
            ("VIREMENT EMIS Motif M. MACHIN BIDULE", ("Virement", "Motif M. MACHIN BIDULE")),
            ("INTERETS CREDITEURS DE L'ANNEE TAUX 0,500%", ("Interets Crediteurs", "DE L'ANNEE TAUX 0,500%")),
            ("M. DUPONT JEAN", ("Inconnue", "M. DUPONT JEAN")),
        ]
        for description, output in test_cases:
            with self.subTest(description):
                self.assertEqual(CANE2024EpargneBankWebParser._find_operation_method_description(description), output)

    def test_convert_line(self):
        import_bank_account = CANE2024EpargneBankWebParser("")
        date = "2024-03-14"

        # Normal:
        soup = bs4.BeautifulSoup(
            """
            <app-operation _ngcontent-ng-c3746540348="" _nghost-ng-c1588512882="">
                <div _ngcontent-ng-c1588512882="" class="msl-flex msl-justify-between msl-items-center">
                    <div _ngcontent-ng-c1588512882="" class="msl-flex msl-items-center">
                        <div _ngcontent-ng-c1588512882="" class="msl-p-100 msl-mr-[12px] msl-rounded-100 msl-bg-purple-100">
                            <mds-icon _ngcontent-ng-c1588512882="" name="chequebook" tint="var(--msl-accent-purple-700)" size="100" style="--mds-icon-color: var(--msl-accent-purple-700);">
                            <!---->
                            <!---->
                            </mds-icon></div>
                            <div _ngcontent-ng-c1588512882="">
                            <div _ngcontent-ng-c1588512882="">INTERETS CREDITEURS DE L'ANNEE TAUX  2,000%</div>
                            <!---->
                            <!---->
                            <!---->
                        </div>
                    </div>
                    <div _ngcontent-ng-c1588512882="" class="msl-text-body-l msl-font-700 msl-pl-100 msl-pr-100 msl-bg-secondary-100 msl-text-primary">
                        <mds-amount _ngcontent-ng-c1588512882="">39.03</mds-amount>
                    </div>
                </div>
            </app-operation>
            """, features="html.parser"
        )

        operation = import_bank_account._convert_line(soup, date)

        self.assertEqual(str(operation.bank_date), date)
        self.assertEqual(str(operation.date_paid), date)
        self.assertEqual(operation.method, "Interets Crediteurs")
        self.assertEqual(
            operation.bank_description,
            "DE L'ANNEE TAUX  2,000%"
        )
        self.assertEqual(operation.comment, "")
        self.assertEqual(operation.amount, 39.03)
        self.assertEqual(operation.bank_import_method, Operation.BankOperationImportMethod.WEB)

        # With comment:
        soup = bs4.BeautifulSoup(
            """
            <app-operation _ngcontent-ng-c3746540348="" _nghost-ng-c1588512882="">
                <div _ngcontent-ng-c1588512882="" class="msl-flex msl-justify-between msl-items-center">
                    <div _ngcontent-ng-c1588512882="" class="msl-flex msl-items-center">
                        <div _ngcontent-ng-c1588512882="" class="msl-p-100 msl-mr-[12px] msl-rounded-100 msl-bg-purple-100">
                            <mds-icon _ngcontent-ng-c1588512882="" name="chequebook" tint="var(--msl-accent-purple-700)" size="100" style="--mds-icon-color: var(--msl-accent-purple-700);">
                            <!---->
                            <!---->
                            </mds-icon></div>
                            <div _ngcontent-ng-c1588512882="">
                            <div _ngcontent-ng-c1588512882="">VIREMENT EN VOTRE FAVEUR Motif MACHINE BIDULE</div>
                            <div _ngcontent-ng-c1588512882="" class="msl-text-body-m msl-font-400"> Motif                             </div>
                            <!---->
                            <!---->
                            <!---->
                        </div>
                    </div>
                    <div _ngcontent-ng-c1588512882="" class="msl-text-body-l msl-font-700 msl-pl-100 msl-pr-100 msl-bg-secondary-100 msl-text-primary">
                        <mds-amount _ngcontent-ng-c1588512882="">500</mds-amount>
                    </div>
                </div>
            </app-operation>
            """, features="html.parser"
        )

        operation = import_bank_account._convert_line(soup, date)

        self.assertEqual(str(operation.bank_date), date)
        self.assertEqual(str(operation.date_paid), date)
        self.assertEqual(operation.method, "Virement")
        self.assertEqual(
            operation.bank_description,
            "Motif MACHINE BIDULE"
        )
        self.assertEqual(operation.bank_comment, "Motif")
        self.assertEqual(operation.amount, 500)
        self.assertEqual(operation.bank_import_method, Operation.BankOperationImportMethod.WEB)

    def test_parse(self):
        account = Account(number=123456789)
        account.save()

        with open('fixtures/cane2024epargne.html', 'rb') as f:
            import_bank_account = ImportWebBankOperations(f.read(), 'cane2024epargne')
            import_bank_account.parse()

        self.assertEqual(import_bank_account.account.number, 123456789)
        self.assertEqual(len(import_bank_account.operations), 4)
        self.assertEqual(str(import_bank_account.account.balance_date), "2024-03-31")
        self.assertEqual(import_bank_account.account.balance, 12345.67)

        self.assertEqual(str(import_bank_account.operations[0].bank_date), "2023-12-29")
        self.assertEqual(str(import_bank_account.operations[1].bank_date), "2023-12-29")
        self.assertEqual(str(import_bank_account.operations[2].bank_date), "2024-01-08")
        self.assertEqual(str(import_bank_account.operations[3].bank_date), "2024-03-31")


class TestCANE2023EpargneBankWebParser(TestCase):
    def test_convert_amount(self):
        self.assertEqual(CANE2023EpargneBankWebParser._convert_amount("12\u202f345.67"), 12_345.67)

    def test_find_operation_method_description(self):
        test_cases = [
            ("VIREMENT EN VOTRE FAVEUR Motif MACHIN", ("Virement", "Motif MACHIN")),
            ("VIREMENT EMIS Motif M. MACHIN BIDULE", ("Virement", "Motif M. MACHIN BIDULE")),
            ("INTERETS CREDITEURS DE L'ANNEE TAUX 0,500%", ("Interets Crediteurs", "DE L'ANNEE TAUX 0,500%")),
        ]
        for description, output in test_cases:
            with self.subTest(description):
                self.assertEqual(CANE2023EpargneBankWebParser._find_operation_method_description(description), output)

    def test_convert_line(self):
        import_bank_account = CANE2023EpargneBankWebParser("")

        # Normal:
        soup = bs4.BeautifulSoup(
            """
            <msl-row _ngcontent-rsk-c40="" msljustifycontent="space-between" class="ui-operations-history-row" _nghost-rsk-c19="" style="flex-wrap: initial; justify-content: space-between; align-items: initial; align-content: initial;">
                <msl-column _ngcontent-rsk-c40="" mslbasis="30%" _nghost-rsk-c39="" style="flex-wrap: initial; place-content: initial; align-items: initial; flex-basis: 30%;">
                    <msl-flex _ngcontent-rsk-c40="" msljustifycontent="space-between" mslalignitems="center" _nghost-rsk-c14="" style="flex-wrap: initial; justify-content: space-between; align-items: center; align-content: initial;">
                    <span _ngcontent-rsk-c40="" class="msl-color-grey-800 msl-font-size-s msl-font-weight-s">31 d&#xE9;cembre 2019 </span>
                    <msl-icon _ngcontent-rsk-c40="" name="ic_transfer" icon_color="secondary-3" width="12.71" height="13.33" class="msl-padding-inline-xs" _nghost-rsk-c13="">
                        <div _ngcontent-rsk-c13="" style="width: 12.71px; height: 13.33px;" aria-hidden="true">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M18 12.7h.2l3.5 3.6c.4.3.4.9 0 1.3l-3.5 3.6a1 1 0 0 1-1.5-1.3l1.9-1.9H3a1 1 0 0 1-1-.9V17c0-.5.4-1 .9-1h15.7l-1.8-1.8a1 1 0 0 1-.1-1.4 1 1 0 0 1 1.4-.1Zm-7.8-10c.4.4.4 1 0 1.5L8.4 6H19a1 1 0 0 1 0 2H8.4l1.8 1.8a1 1 0 1 1-1.4 1.4L5.3 7.7a1 1 0 0 1 0-1.4l3.5-3.6a1 1 0 0 1 1.4 0Z" fill="currentColor" class="fill-color" fill-rule="evenodd" style="fill: var(--msl-color-secondary-3);"/>
                        </svg>
                        </div>
                    </msl-icon>
                    </msl-flex>
                </msl-column>
                <msl-column _ngcontent-rsk-c40="" mslbasis="50%" _nghost-rsk-c39="" style="flex-wrap: initial; place-content: initial; align-items: initial; flex-basis: 50%;">
                    <div _ngcontent-rsk-c40="" class="libelle-opetation msl-color-grey-800 msl-font-size-s msl-font-weight-m"> INTERETS CREDITEURS DE L'ANNEE TAUX  2,000% </div>
                    <!---->
                    <div _ngcontent-rsk-c40="">  </div>
                    <!---->
                    <div _ngcontent-rsk-c40="">  </div>
                    <msl-row _ngcontent-rsk-c40="" _nghost-rsk-c19="" style="flex-wrap: initial; place-content: initial; align-items: initial;"/>
                </msl-column>
                <msl-column _ngcontent-rsk-c40="" mslbasis="20%" mslalignitems="flex-end" _nghost-rsk-c39="" style="flex-wrap: initial; place-content: initial; align-items: flex-end; flex-basis: 20%;">
                    <msl-flex _ngcontent-rsk-c40="" _nghost-rsk-c14="" style="flex-wrap: initial; place-content: initial; align-items: initial;">
                    <span _ngcontent-rsk-c40="" class="no-margin msl-font-weight-m msl-color-secondary-1">+ 192,50  &#x20AC;</span>
                    </msl-flex>
                </msl-column>
            </msl-row>
            """, features="html.parser"
        )

        operation = import_bank_account._convert_line(soup)

        self.assertEqual(str(operation.bank_date), "2019-12-31")
        self.assertEqual(str(operation.date_paid), "2019-12-31")
        self.assertEqual(operation.method, "Interets Crediteurs")
        self.assertEqual(
            operation.bank_description,
            "DE L'ANNEE TAUX  2,000%"
        )
        self.assertEqual(operation.comment, "")
        self.assertEqual(operation.amount, 192.50)
        self.assertEqual(operation.bank_import_method, Operation.BankOperationImportMethod.WEB)

        # With comment and required date adjustement:
        soup = bs4.BeautifulSoup(
            """
            <msl-row _ngcontent-rsk-c40="" msljustifycontent="space-between" class="ui-operations-history-row" _nghost-rsk-c19="" style="flex-wrap: initial; justify-content: space-between; align-items: initial; align-content: initial;">
                <msl-column _ngcontent-rsk-c40="" mslbasis="30%" _nghost-rsk-c39="" style="flex-wrap: initial; place-content: initial; align-items: initial; flex-basis: 30%;">
                    <msl-flex _ngcontent-rsk-c40="" msljustifycontent="space-between" mslalignitems="center" _nghost-rsk-c14="" style="flex-wrap: initial; justify-content: space-between; align-items: center; align-content: initial;">
                    <span _ngcontent-rsk-c40="" class="msl-color-grey-800 msl-font-size-s msl-font-weight-s">14 juin 2023 </span>
                    <msl-icon _ngcontent-rsk-c40="" name="ic_transfer" icon_color="secondary-3" width="12.71" height="13.33" class="msl-padding-inline-xs" _nghost-rsk-c13="">
                        <div _ngcontent-rsk-c13="" style="width: 12.71px; height: 13.33px;" aria-hidden="true">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M18 12.7h.2l3.5 3.6c.4.3.4.9 0 1.3l-3.5 3.6a1 1 0 0 1-1.5-1.3l1.9-1.9H3a1 1 0 0 1-1-.9V17c0-.5.4-1 .9-1h15.7l-1.8-1.8a1 1 0 0 1-.1-1.4 1 1 0 0 1 1.4-.1Zm-7.8-10c.4.4.4 1 0 1.5L8.4 6H19a1 1 0 0 1 0 2H8.4l1.8 1.8a1 1 0 1 1-1.4 1.4L5.3 7.7a1 1 0 0 1 0-1.4l3.5-3.6a1 1 0 0 1 1.4 0Z" fill="currentColor" class="fill-color" fill-rule="evenodd" style="fill: var(--msl-color-secondary-3);"/>
                        </svg>
                        </div>
                    </msl-icon>
                    </msl-flex>
                </msl-column>
                <msl-column _ngcontent-rsk-c40="" mslbasis="50%" _nghost-rsk-c39="" style="flex-wrap: initial; place-content: initial; align-items: initial; flex-basis: 50%;">
                    <div _ngcontent-rsk-c40="" class="libelle-opetation msl-color-grey-800 msl-font-size-s msl-font-weight-m"> VIREMENT EN VOTRE FAVEUR Salaire MACHIN BIDULE </div>
                    <div _ngcontent-rsk-c40="" class="msl-margin-stack-xs ng-star-inserted"/>
                    <!---->
                    <div _ngcontent-rsk-c40=""> Salaire                             </div>
                    <!---->
                    <div _ngcontent-rsk-c40="">  </div>
                    <msl-row _ngcontent-rsk-c40="" _nghost-rsk-c19="" style="flex-wrap: initial; place-content: initial; align-items: initial;"/>
                </msl-column>
                <msl-column _ngcontent-rsk-c40="" mslbasis="20%" mslalignitems="flex-end" _nghost-rsk-c39="" style="flex-wrap: initial; place-content: initial; align-items: flex-end; flex-basis: 20%;">
                    <msl-flex _ngcontent-rsk-c40="" _nghost-rsk-c14="" style="flex-wrap: initial; place-content: initial; align-items: initial;">
                    <span _ngcontent-rsk-c40="" class="no-margin msl-font-weight-m msl-color-secondary-1">+ 1&#x202F;000,00  &#x20AC;</span>
                    </msl-flex>
                </msl-column>
            </msl-row>
            """, features="html.parser"
        )

        operation = import_bank_account._convert_line(soup)

        self.assertEqual(str(operation.bank_date), "2023-06-14")
        self.assertEqual(str(operation.date_paid), "2023-06-14")
        self.assertEqual(operation.method, "Virement")
        self.assertEqual(
            operation.bank_description,
            "Salaire MACHIN BIDULE"
        )
        self.assertEqual(operation.bank_comment, "Salaire")
        self.assertEqual(operation.amount, 1000)
        self.assertEqual(operation.bank_import_method, Operation.BankOperationImportMethod.WEB)

    def test_parse(self):
        account = Account(number=123456789)
        account.save()

        with open('fixtures/cane2023epargne.html', 'rb') as f:
            import_bank_account = ImportWebBankOperations(f.read(), 'cane2023epargne')
            import_bank_account.parse()

        self.assertEqual(import_bank_account.account.number, 123456789)
        self.assertEqual(len(import_bank_account.operations), 3)
        self.assertEqual(str(import_bank_account.account.balance_date), "2023-05-09")
        self.assertEqual(import_bank_account.account.balance, 12345.67)


class TestCANE2020LongTermeBankWebParser(TestCase):
    def test_convert_line(self):
        import_bank_account = CANE2020LongTermeBankWebParser("")
        today = datetime.date(2020, 3, 13)

        # Normal:
        soup = bs4.BeautifulSoup(
            """
            <tr>
                  <td style="padding:8px 2px;">
                        <div class="col-xs-12 no-pad-lr">
                              <div class="col-xs-3 no-pad-lr">
                                    <span>
                                          10/01/2020
                                    </span>
                              </div>

                              <div class="col-xs-6 no-pad-lr">
                                    <span>
                                          Virement En Votre Faveur            
                                          <br>
                                          Mens. Cpte Cheques  01/20          


                                    </span>
                              </div>


                              <div class="col-xs-3 cell-amount no-pad-lr">
                                    <span>
                                          45,00
                                    </span>
                              </div>
                        </div>
                  </td>
            </tr>
            """, features="html.parser"
        )

        operation = import_bank_account._convert_line(soup.tr)

        self.assertEqual(str(operation.bank_date), "2020-01-10")
        self.assertEqual(str(operation.date_paid), "2020-01-10")
        self.assertEqual(operation.method, "Virement En Votre Faveur")
        self.assertEqual(operation.bank_description, "Virement En Votre Faveur")
        self.assertEqual(operation.bank_comment, "Mens. Cpte Cheques  01/20")
        self.assertEqual(operation.comment, "")
        self.assertEqual(operation.amount, 45)
        self.assertEqual(operation.bank_import_method, Operation.BankOperationImportMethod.WEB)

    def test_parse(self):
        account = Account(number=123456789)
        account.save()

        with open('fixtures/cane2020longterme.html', 'rb') as f:
            import_bank_account = ImportWebBankOperations(f.read(), 'cane2020longterme')
            import_bank_account.parse()

        self.assertEqual(import_bank_account.account.number, 123456789)
        self.assertEqual(len(import_bank_account.operations), 6)
        self.assertEqual(str(import_bank_account.account.balance_date), "2020-03-14")
        self.assertEqual(import_bank_account.account.balance, 1234.56)


class TestCANE2019BankWebParser(TestCase):
    def test_convert_amount(self):
        self.assertEqual(CANE2019BankWebParser._convert_amount("4\n904.89"), 4904.89)

    def test_convert_line(self):
        import_bank_account = CANE2019BankWebParser("")
        import_bank_account._type = AccountType.DEFAULT

        # Normal:
        soup = bs4.BeautifulSoup(
            '<tr class="ligne-impaire" id="ouvrir3">'
            '   <td class="cel-texte">14/01</td>'
            '   <td style="vertical-align: top;" width="20px"></td>'
            '   <td class="cel-texte">'
            '       Paiement Par Carte                 <br>'
            '       Supermarché de la carotte      12/01   '
            '       <div id="#lopmm3" style="display: none;"></div>'
            '   </td>'
            '   <td class="cel-texte">&nbsp;</td>'
            '   <td class="cel-num cel-neg">-&nbsp;30,98</td>'
            '</tr>',
            features="html.parser"
        )

        operation = import_bank_account._convert_line(
            soup.tr,
            datetime.date(2019, 2, 14)
        )

        self.assertEqual(str(operation.bank_date), "2019-01-14")
        self.assertEqual(str(operation.date_paid), "2019-01-14")
        self.assertEqual(operation.method, "Paiement Par Carte")
        self.assertEqual(
            operation.bank_description,
            "Supermarché de la carotte      12/01"
        )
        self.assertEqual(operation.comment, "")
        self.assertEqual(operation.amount, -30.98)
        self.assertEqual(operation.bank_import_method, Operation.BankOperationImportMethod.WEB)

        # Year before:
        soup = bs4.BeautifulSoup(
            '<tr class="ligne-impaire" id="ouvrir3">'
            '   <td class="cel-texte">14/02</td>'
            '   <td style="vertical-align: top;" width="20px"></td>'
            '   <td class="cel-texte">'
            '       Paiement Par Carte                 <br>'
            '       Supermarché de la carotte      12/01   '
            '       <div id="#lopmm3" style="display: none;"></div>'
            '   </td>'
            '   <td class="cel-texte">&nbsp;</td>'
            '   <td class="cel-num cel-neg">-&nbsp;30,98</td>'
            '</tr>',
            features="html.parser"
        )
        operation = import_bank_account._convert_line(
            soup.tr,
            datetime.date(2019, 1, 12)
        )

        self.assertEqual(str(operation.bank_date), "2018-02-14")
        self.assertEqual(str(operation.date_paid), "2018-02-14")

        # With comment:
        soup = bs4.BeautifulSoup(
            '<tr class="ligne-impaire" id="ouvrir7">'
            '   <td class="cel-texte">11/01</td>'
            '   <td style="vertical-align: top;" width="20px">'
            '       <a href="javascript:;">'
            '           <img src="deplier.png" onclick="flipflop(\'#lopmm7\');'
            '                   changeImage(\'image_fleche_7\');">'
            '       </a>'
            '   </td>'
            '   <td class="cel-texte">'
            '       Virement<br>'
            '       Société des virements   '
            '       <div id="#lopmm7" style="display: none;">'
            '           Virement pour Jean-Michel<br>'
            '           Gain du Loto<br>'
            '           Foire aux alouettes'
            '       </div>'
            '   </td>'
            '   <td class="cel-texte">&nbsp;</td>'
            '   <td class="cel-num">200,00</td>'
            '</tr>',
            features="html.parser"
        )
        operation = import_bank_account._convert_line(
            soup.tr,
            datetime.date(2019, 1, 12)
        )

        self.assertEqual(
            operation.bank_comment,
            "Virement pour Jean-Michel\nGain du Loto\nFoire aux alouettes"
        )

        # Considered as new by the bank:
        soup = bs4.BeautifulSoup(
            '<tr class=ligne-paire id="ouvrir0">'
            '   <td class="cel-texte gras">15/04</td>'
            '   <td style="vertical-align: top;" width="20px"></td>'
            '   <td class="cel-texte gras">'
            '	    Paiement Par Carte'
            '       <br>'
            '   	Sandwich     13/04'
            '		<div id="#lopmm0"></div>'
            '   </td>'
            '   <td class="cel-texte gras">&nbsp;</td>'
            '   <td class="cel-num cel-neg gras">-&nbsp;6,75</td>'
            '</tr>',
            features="html.parser"
        )
        operation = import_bank_account._convert_line(
            soup.tr,
            datetime.date(2019, 4, 21)
        )

        self.assertEqual(operation.bank_description, "Sandwich     13/04")

        # Amount with space:
        soup = bs4.BeautifulSoup(
            '<tr class="ligne-impaire" id="ouvrir3">'
            '   <td class="cel-texte">14/01</td>'
            '   <td style="vertical-align: top;" width="20px"></td>'
            '   <td class="cel-texte">'
            '       Paiement Par Carte                 <br>'
            '       Big amount      12/01   '
            '       <div id="#lopmm3" style="display: none;"></div>'
            '   </td>'
            '   <td class="cel-texte">&nbsp;</td>'
            '   <td class="cel-num cel-neg">-&nbsp;1 030,98</td>'
            '</tr>',
            features="html.parser"
        )

        operation = import_bank_account._convert_line(
            soup.tr,
            datetime.date(2019, 2, 14)
        )

        self.assertEqual(operation.amount, -1030.98)

    def test_convert_lines(self):
        import_bank_account = CANE2019BankWebParser("")
        import_bank_account._type = AccountType.DEFAULT

        soup = bs4.BeautifulSoup(
            '<tr class="ligne-impaire" id="ouvrir3">'
            '   <td class="cel-texte">14/01</td>'
            '   <td style="vertical-align: top;" width="20px"></td>'
            '   <td class="cel-texte">'
            '       Paiement Par Carte                 <br>'
            '       Supermarché de la carotte      12/01   '
            '       <div id="#lopmm3" style="display: none;"></div>'
            '   </td>'
            '   <td class="cel-texte">&nbsp;</td>'
            '   <td class="cel-num cel-neg">-&nbsp;30,98</td>'
            '</tr>'
            '<tr class="ligne-paire" id="ouvrir3">'
            '   <td class="cel-texte">14/02</td>'
            '   <td style="vertical-align: top;" width="20px"></td>'
            '   <td class="cel-texte">'
            '       Paiement Par Carte                 <br>'
            '       Supermarché de la carotte      12/01   '
            '       <div id="#lopmm3" style="display: none;"></div>'
            '   </td>'
            '   <td class="cel-texte">&nbsp;</td>'
            '   <td class="cel-num cel-neg">-&nbsp;30,98</td>'
            '</tr>',
            features="html.parser"
        )
        import_bank_account._convert_lines(
            soup.select('tr'),
            datetime.date.fromisoformat("2019-12-31")
        )

        operations = import_bank_account.operations

        self.assertEqual(
            str(operations[0].bank_date),
            "2019-01-14"
        )
        self.assertEqual(
            str(operations[0].date_paid),
            "2019-01-14"
        )
        self.assertEqual(
            str(operations[1].bank_date),
            "2018-02-14"
        )
        self.assertEqual(
            str(operations[1].date_paid),
            "2018-02-14"
        )

        import_bank_account.operations = []

        import_bank_account._convert_lines(
            soup.select('tr'),
            datetime.date.fromisoformat("2018-12-31")
        )

        operations = import_bank_account.operations

        self.assertEqual(
            str(operations[0].bank_date),
            "2018-01-14"
        )
        self.assertEqual(
            str(operations[0].date_paid),
            "2018-01-14"
        )
        self.assertEqual(
            str(operations[1].bank_date),
            "2017-02-14"
        )
        self.assertEqual(
            str(operations[1].date_paid),
            "2017-02-14"
        )

    def test_parse(self):
        account = Account(number=123456789)
        account.save()

        with open('fixtures/bank_account.html', 'rb') as f:
            import_bank_account = ImportWebBankOperations(
                f.read().decode('windows-1252'),
                'cane2019'
            )
            import_bank_account.parse()

        self.assertEqual(import_bank_account.account.number, 123456789)
        self.assertEqual(import_bank_account.parser._type, AccountType.DEFAULT)
        self.assertEqual(len(import_bank_account.operations), 2)
        self.assertEqual(import_bank_account.account.balance, 234.56)
        self.assertEqual(str(import_bank_account.account.balance_date), "2019-03-17")

    def test_parse_long_term(self):
        account = Account(number=123456789)
        account.save()

        with open('fixtures/bank_account_long_term.html', 'rb') as f:
            import_bank_account = ImportWebBankOperations(
                f.read().decode('windows-1252'),
                'cane2019'
            )
            import_bank_account.parse()

            self.assertEqual(import_bank_account.account.number, 123456789)
            self.assertEqual(import_bank_account.parser._type, AccountType.EPARGNE_LONG_TERME)
            self.assertEqual(len(import_bank_account.operations), 11)
            self.assertEqual(str(import_bank_account.account.balance_date), "2019-05-19")
            self.assertEqual(import_bank_account.account.balance, 4904.89)

    def test_parse_other(self):
        account = Account(number=123456789)
        account.save()

        with open('fixtures/bank_account_other.html', 'rb') as f:
            import_bank_account = ImportWebBankOperations(f.read(), 'cane2019')
            import_bank_account.parse()

            self.assertEqual(import_bank_account.account.number, 123456789)
            self.assertEqual(import_bank_account.parser._type, AccountType.EPARGNE)
            self.assertEqual(len(import_bank_account.operations), 2)
            self.assertEqual(str(import_bank_account.account.balance_date), "2019-12-22")
            self.assertEqual(import_bank_account.account.balance, 22720.53)

            self.assertEqual(str(import_bank_account.operations[0].bank_date), "2019-11-26")
            self.assertEqual(import_bank_account.operations[0].method, "Virement")
            self.assertEqual(
                import_bank_account.operations[0].bank_description,
                "Salaire Arrondi"
            )
            self.assertEqual(import_bank_account.operations[0].amount, 1000.)

            self.assertEqual(str(import_bank_account.operations[1].bank_date), "2019-12-01")
            self.assertEqual(import_bank_account.operations[1].method, "Virement")
            self.assertEqual(
                import_bank_account.operations[1].bank_description,
                "Remboursement ABC"
            )
            self.assertEqual(import_bank_account.operations[1].amount, 150.)


class TestImportCsvOperations(TestCase):
    def test_parse_operations(self):
        account = Account(number=123456789)

        import_csv = ImportCsvOperations('fixtures/csv_operation_import.csv', account)
        import_csv._parse_operations()

        operations = [o[0] for o in import_csv.operations]

        self.assertEqual(len(operations), 4)

        self.assertEqual(str(operations[0].date_paid), "2016-02-19")
        self.assertEqual(operations[0].method, "Virement")
        self.assertEqual(operations[0].description, "Virement 1")
        self.assertEqual(operations[0].comment, "")
        self.assertEqual(operations[0].amount, 50.)
        self.assertEqual(operations[0].bank_description, "")
        self.assertEqual(operations[0].bank_comment, "")
        self.assertEqual(operations[0].bank_date, None)
        self.assertEqual(operations[0].account, account)
        self.assertEqual(operations[0].bank_import_method, None)

        self.assertEqual(operations[1].amount, -13.12)

        self.assertEqual(str(operations[3].date_paid), "2016-06-29")
        self.assertEqual(operations[3].method, "Virement")
        self.assertEqual(operations[3].description, "Virement avec long commentaire")
        self.assertEqual(operations[3].comment, "")
        self.assertEqual(operations[3].amount, 500.)
        self.assertEqual(operations[3].bank_description, "")
        self.assertEqual(operations[3].bank_comment, "")
        self.assertEqual(operations[3].bank_date, None)
        self.assertEqual(operations[3].account, account)


class TestImportCsvBankOperations(TestCase):
    def test_parse_operations(self):
        account = Account(number=123456789)

        import_csv = ImportCsvBankOperations('fixtures/csv_bank_operation_import.csv', account)
        import_csv.parse()

        operations = import_csv.operations

        self.assertEqual(len(operations), 4)

        self.assertEqual(str(operations[0].bank_date), "2016-02-19")
        self.assertEqual(str(operations[0].date_paid), "2016-02-19")
        self.assertEqual(operations[0].method, "Virement")
        self.assertEqual(operations[0].bank_description, "Virement 1")
        self.assertEqual(operations[0].bank_comment, "")
        self.assertEqual(operations[0].amount, 50.)
        self.assertEqual(operations[0].description, "")
        self.assertEqual(operations[0].comment, "")
        self.assertEqual(operations[0].account, account)
        self.assertEqual(operations[0].bank_import_method, Operation.BankOperationImportMethod.CSV)

        self.assertEqual(operations[1].amount, -13.12)

        self.assertEqual(str(operations[3].date_paid), "2016-06-29")
        self.assertEqual(str(operations[3].bank_date), "2016-06-29")
        self.assertEqual(operations[3].method, "Virement")
        self.assertEqual(operations[3].bank_description, "Virement avec long commentaire")
        self.assertEqual(operations[3].bank_comment, "Commentaire\nRetour à la ligne")
        self.assertEqual(operations[3].amount, 500.)
        self.assertEqual(operations[3].description, "")
        self.assertEqual(operations[3].comment, "")
        self.assertEqual(operations[3].account, account)
