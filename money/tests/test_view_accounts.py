import re

from django.test import TestCase
from django.urls import reverse

from money.models.accounts import Account
from money.models.operations import Operation


class AccountIndexViewTest(TestCase):
    def test_without_account(self):
        response = self.client.get(reverse('accounts_index'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['accounts'], [])

    def test_with_empty_account_with_balance_limit(self):
        account = Account(title="DummyAccount", number=13, balance_limit=100)
        account.save()

        response = self.client.get(reverse('accounts_index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, account.title)
        self.assertContains(response, account.number)
        self.assertQuerysetEqual(response.context['accounts'], [account])
        self.assertEquals(response.context['sum_accounts'], 0)
        self.assertIsNotNone(re.search(r"<td.*>\s*-\s*</td>", response.content.decode()))

    def test_with_empty_account(self):
        account = Account(title="DummyAccount", number=13)
        account.save()

        response = self.client.get(reverse('accounts_index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, account.title)
        self.assertContains(response, account.number)
        self.assertQuerysetEqual(response.context['accounts'], [account])
        self.assertEquals(response.context['sum_accounts'], 0)
        self.assertIsNotNone(re.search(r"<td.*>\s*-\s*</td>", response.content.decode()))

    def test_with_account_with_balance(self):
        account_a = Account(title="DummyAccount", number=13, balance=40.12)
        account_a.save()
        account_b = Account(title="Imported account with null balance", number=14, balance=0)
        account_b.save()

        response = self.client.get(reverse('accounts_index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, account_a.title)
        self.assertContains(response, account_a.number)
        self.assertContains(response, account_b.title)
        self.assertContains(response, account_b.number)
        self.assertContains(response, 40)
        self.assertContains(response, 12)
        self.assertContains(response, 0)
        self.assertContains(response, "00")
        self.assertQuerysetEqual(
            response.context['accounts'],
            [account_a, account_b]
        )
        self.assertEquals(response.context['sum_accounts'], 40.12)
        self.assertIsNone(re.search(r"<td.*>\s*-\s*</td>", response.content.decode()))

    def test_with_many_accounts(self):
        account_a = Account(title="Account A", number=13, balance=40.12)
        account_a.save()
        account_b = Account(title="Account B", number=14, balance=100)
        account_b.save()
        account_c = Account(title="Account C", number=15, balance=None)
        account_c.save()

        response = self.client.get(reverse('accounts_index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, account_a.title)
        self.assertContains(response, account_a.number)
        self.assertContains(response, 40)
        self.assertContains(response, 12)
        self.assertContains(response, account_b.title)
        self.assertContains(response, account_b.number)
        self.assertContains(response, 100)
        self.assertContains(response, "00")
        self.assertContains(response, account_c.title)
        self.assertContains(response, account_c.number)
        self.assertIsNotNone(re.search(r"<td.*>\s*-\s*</td>", response.content.decode()))
        self.assertQuerysetEqual(
            response.context['accounts'],
            [account_a, account_b, account_c]
        )
        self.assertEquals(response.context['sum_accounts'], 140.12)

    def test_homepage(self):
        response_homepage = self.client.get(reverse('homepage'))
        self.assertEqual(response_homepage.status_code, 200)

        response_accounts_index = self.client.get(reverse('accounts_index'))
        self.assertEqual(response_accounts_index.status_code, 200)

        # Since these pages contain a form to mark all imported accounts as
        # done, they contain a csrf token which is different on these two pages:
        self.assertEqual(
            response_homepage.content.decode(),
            response_accounts_index.content.decode().replace(
                str(response_accounts_index.context['csrf_token']),
                str(response_homepage.context['csrf_token'])
            )
        )


class AccountDetailViewTest(TestCase):
    def test_with_empty_account_with_balance_limit(self):
        account = Account(title="DummyAccount", number=13, balance_limit=100)
        account.save()

        response = self.client.get(reverse('accounts_view', args=[account.pk]))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, account.title)
        self.assertContains(response, account.number)
        self.assertEqual(response.context['object'], account)

    def test_with_initial_amount(self):
        account = Account(title="DummyAccount", number=13, balance=110, initial_amount=100)
        account.save()

        operation = Operation(
            bank_date='2018-02-21',
            date_paid='2018-02-21',
            description='Stuff 1',
            method="Virement",
            account=account,
            amount=10
        )
        operation.save()

        response = self.client.get(reverse('accounts_view', args=[account.pk]))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['local_balance_without_pending'], account.balance)
        self.assertEqual(response.context['diff_bank_local_balance'], 0.)

    def test_with_wrong_initial_amount(self):
        account = Account(title="DummyAccount", number=13, balance=110)
        account.save()

        operation = Operation(
            bank_date='2018-02-21',
            date_paid='2018-02-21',
            description='Stuff 1',
            method="Virement",
            account=account,
            amount=10
        )
        operation.save()

        response = self.client.get(reverse('accounts_view', args=[account.pk]))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['local_balance_without_pending'], 10)
        self.assertEqual(response.context['diff_bank_local_balance'], 100)

    def test_with_support_amount(self):
        account = Account(title="DummyAccount", number=13, support_amount=100)
        account.save()

        operation = Operation(
            bank_date='2018-02-21',
            date_paid='2018-02-21',
            description='Stuff 1',
            method="Virement",
            account=account,
            amount=10,
            takes_from_support=True,
        )
        operation.save()

        response = self.client.get(reverse('accounts_view', args=[account.pk]))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['total_from_support_amount'], operation.amount)
        self.assertEqual(
            response.context['current_support_amount'],
            account.support_amount-operation.amount
        )


class AccountStatsViewTest(TestCase):
    def test_with_empty_account_with_balance_limit(self):
        account = Account(title="DummyAccount", number=13, balance_limit=100)
        account.save()

        response = self.client.get(reverse('accounts_stats', args=[account.pk]))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, account.title)
        self.assertEqual(response.context['object'], account)


class OperationDeleteViewTest(TestCase):
    def test_post(self):
        account = Account(title="DummyAccount", number=13)
        account.save()

        response = self.client.post(reverse('accounts_delete', args=[account.pk]))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Account.objects.count(), 0)
