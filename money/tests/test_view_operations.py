from django.test import TestCase
from django.urls import reverse

from money.models.accounts import Account
from money.models.categories import Category
from money.models.operations import Operation


class OperationAddViewTest(TestCase):
    def test_valid_get(self):
        account = Account(title="DummyAccount", number=13)
        account.save()

        response = self.client.get(reverse('operations_add', args=[account.pk]))
        self.assertEqual(response.status_code, 200)

    def test_refund(self):
        account = Account(title="DummyAccount", number=13)
        account.save()

        operation_a = Operation(
                date_paid='2020-12-22',
                description='Stuff A',
                method="Card",
                account=account,
                amount=-50
        )
        operation_a.save()

        response = self.client.get(
            reverse('operations_add', args=[account.pk]) + "?refund=" + str(operation_a.pk)
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['form']['_amount'].initial, -operation_a.amount)

    def test_paid_dates(self):
        # Single paid date:
        account = Account(title="DummyAccount", number=13)
        account.save()

        data = {
            'dates-TOTAL_FORMS': 1,
            'dates-INITIAL_FORMS': 0,
            'dates-0-date_paid': '2020-12-24',
            '_amount': 10,
            'method': 'Card',
            'description': 'Stuff C',
            'comment': '',
            'receipts-TOTAL_FORMS': 0,
            'receipts-INITIAL_FORMS': 0
        }
        response = self.client.post(reverse(
            'operations_add', args=[account.pk]
        ), data, follow=False)
        self.assertEqual(response.status_code, 302)

        operation = Operation.objects.first()
        self.assertEqual(str(operation.date_paid), "2020-12-24")

        # Many paid dates:
        account = Account(title="DummyAccount", number=14)
        account.save()

        with open('fixtures/bank_account.html', 'rb') as f:
            data = {
                'dates-TOTAL_FORMS': 3,
                'dates-INITIAL_FORMS': 0,
                'dates-0-date_paid': '2020-12-25',
                'dates-1-date_paid': '2020-12-26',
                'dates-2-date_paid': '2020-12-27',
                '_amount': 10,
                'method': 'Card',
                'description': 'Stuff C',
                'comment': '',
                'receipts-TOTAL_FORMS': 1,
                'receipts-INITIAL_FORMS': 0,
                'receipts-1-receipt_file': f,
                'receipts-1-kind': 3
            }
            response = self.client.post(reverse(
                'operations_add', args=[account.pk]
            ), data, follow=False)
            self.assertEqual(response.status_code, 302)

        operations = Operation.objects.filter(account=account).all().order_by('_date_paid')
        self.assertEqual(3, len(operations))
        self.assertEqual(str(operations[0].date_paid), "2020-12-25")
        self.assertEqual(operations[0].operationreceipt_set.all().count(), 1)
        self.assertEqual(str(operations[1].date_paid), "2020-12-26")
        self.assertEqual(operations[1].operationreceipt_set.all().count(), 0)
        self.assertEqual(str(operations[2].date_paid), "2020-12-27")
        self.assertEqual(operations[2].operationreceipt_set.all().count(), 0)

    def test_upload_receipt(self):
        account = Account(title="DummyAccount", number=13)
        account.save()

        with open('fixtures/bank_account.html', 'rb') as f:
            data = {
                'dates-TOTAL_FORMS': 1,
                'dates-INITIAL_FORMS': 0,
                'dates-0-date_paid': '2020-12-24',
                '_amount': 10,
                'method': 'Card',
                'description': 'Stuff C',
                'comment': '',
                'receipts-TOTAL_FORMS': 1,
                'receipts-INITIAL_FORMS': 0,
                'receipts-1-receipt_file': f,
                'receipts-1-kind': 3
            }
            response = self.client.post(reverse(
                'operations_add', args=[account.pk]
            ), data, follow=False)
            self.assertEqual(response.status_code, 302)

        operation = Operation.objects.first()
        self.assertEqual(1, operation.operationreceipt_set.all().count())
        self.assertEqual(operation.operationreceipt_set.all()[0].get_kind_display(), "Other")

    def test_link_all_to_all(self):
        account = Account(title="DummyAccount", number=13)
        account.save()

        operation_a = Operation(
                date_paid='2020-12-22',
                description='Stuff A',
                method="Card",
                account=account,
                amount=-50
        )
        operation_a.save()

        operation_b = Operation(
                date_paid='2020-12-23',
                description='Stuff B',
                method="Card",
                account=account,
                amount=30
        )
        operation_b.save()

        operation_a.linked_operations.add(operation_b)
        operation_a.save()
        operation_b.save()

        data = {
            'dates-TOTAL_FORMS': 1,
            'dates-INITIAL_FORMS': 0,
            'dates-0-date_paid': '2020-12-24',
            '_amount': 10,
            'method': 'Card',
            'description': 'Stuff C',
            'comment': '',
            'linked_operations': [operation_a.pk],
            'all_to_all': 'on',
            'receipts-TOTAL_FORMS': 0,
            'receipts-INITIAL_FORMS': 0
        }
        response = self.client.post(reverse(
            'operations_add', args=[account.pk]
        ), data, follow=False)
        self.assertEqual(response.status_code, 302)

        operations = Operation.objects.filter(account=account)

        self.assertEqual(operations[2].linked_operations.count(), 2)
        self.assertNotIn(operations[2], operations[2].linked_operations.all())
        self.assertIn(operations[0], operations[2].linked_operations.all())
        self.assertIn(operations[1], operations[2].linked_operations.all())

        self.assertEqual(operations[0].linked_operations.count(), 2)
        self.assertNotIn(operations[0], operations[0].linked_operations.all())
        self.assertIn(operations[1], operations[0].linked_operations.all())
        self.assertIn(operations[2], operations[0].linked_operations.all())

        self.assertEqual(operations[1].linked_operations.count(), 2)
        self.assertNotIn(operations[1], operations[1].linked_operations.all())
        self.assertIn(operations[0], operations[1].linked_operations.all())
        self.assertIn(operations[2], operations[1].linked_operations.all())


class OperationEditViewTest(TestCase):
    def test_link_all_to_all(self):
        account = Account(title="DummyAccount", number=13)
        account.save()

        operation_a = Operation(
                date_paid='2020-12-22',
                description='Stuff A',
                method="Card",
                account=account,
                amount=-50
        )
        operation_a.save()

        operation_b = Operation(
                date_paid='2020-12-23',
                description='Stuff B',
                method="Card",
                account=account,
                amount=30
        )
        operation_b.save()

        operation_a.linked_operations.add(operation_b)
        operation_a.save()
        operation_b.save()

        operation_c = Operation(
                date_paid='2020-12-24',
                description='Stuff C',
                method="Card",
                account=account,
                amount=10
        )
        operation_c.save()

        data = {
            '_date_paid': operation_c.date_paid,
            '_amount': operation_c.amount,
            'method': operation_c.method,
            'description': operation_c.description,
            'comment': operation_c.comment,
            'read': 'on',
            'linked_operations': [operation_a.pk],
            'all_to_all': 'on',
            'receipts-TOTAL_FORMS': 0,
            'receipts-INITIAL_FORMS': 0
        }
        response = self.client.post(reverse(
            'operations_edit', args=[operation_c.pk]
        ), data, follow=False)
        self.assertEqual(response.status_code, 302)

        operations = Operation.objects.filter(account=account)

        self.assertEqual(operations[2].linked_operations.count(), 2)
        self.assertNotIn(operations[2], operations[2].linked_operations.all())
        self.assertIn(operations[0], operations[2].linked_operations.all())
        self.assertIn(operations[1], operations[2].linked_operations.all())

        self.assertEqual(operations[0].linked_operations.count(), 2)
        self.assertNotIn(operations[0], operations[0].linked_operations.all())
        self.assertIn(operations[1], operations[0].linked_operations.all())
        self.assertIn(operations[2], operations[0].linked_operations.all())

        self.assertEqual(operations[1].linked_operations.count(), 2)
        self.assertNotIn(operations[1], operations[1].linked_operations.all())
        self.assertIn(operations[0], operations[1].linked_operations.all())
        self.assertIn(operations[2], operations[1].linked_operations.all())

    def test_upload_receipt(self):
        account = Account(title="DummyAccount", number=13)
        account.save()

        operation = Operation(
                date_paid='2021-10-19',
                description='Foo',
                method="Card",
                account=account,
                amount=-10.
        )
        operation.save()

        with open('fixtures/bank_account.html', 'rb') as f:
            data = {
                '_date_paid': operation.date_paid,
                '_amount': operation.amount,
                'method': operation.method,
                'dates-TOTAL_FORMS': 1,
                'dates-INITIAL_FORMS': 0,
                'receipts-TOTAL_FORMS': 1,
                'receipts-INITIAL_FORMS': 0,
                'receipts-1-receipt_file': f,
                'receipts-1-kind': 2
            }
            response = self.client.post(reverse(
                'operations_edit', args=[operation.pk]
            ), data, follow=False)
            self.assertEqual(response.status_code, 302)

        operation = Operation.objects.first()
        self.assertEqual(1, operation.operationreceipt_set.all().count())
        self.assertEqual(operation.operationreceipt_set.all()[0].get_kind_display(), "Other")


class OperationSearchViewTest(TestCase):
    def test_valid_get(self):
        account_a = Account(title="Account A", number=13)
        account_a.save()
        account_b = Account(title="Account B", number=14)
        account_b.save()

        category_a = Category(title="Category A")
        category_a.save()
        category_b = Category(title="Category B")
        category_b.save()

        # Default GET:
        response = self.client.get(reverse('operations_search'))
        self.assertEqual(response.status_code, 200)
        self.assertIn(account_a.pk, response.context['form']['account'].value())
        self.assertIn(account_b.pk, response.context['form']['account'].value())
        self.assertIsNone(response.context['form']['categories'].value())

        # Search in an account:
        response = self.client.get(
            reverse('operations_search') + "?account={}".format(account_b.pk)
        )
        self.assertEqual(response.context['form'].initial['account'], account_b)
        self.assertIsNone(response.context['form']['categories'].value())
        self.assertEqual(response.status_code, 200)

        # Search in a category:
        response = self.client.get(
            reverse('operations_search') + "?category={}".format(category_b.pk)
        )
        self.assertEqual(response.context['form'].initial['categories'], category_b)
        self.assertIn(account_a.pk, response.context['form']['account'].value())
        self.assertIn(account_b.pk, response.context['form']['account'].value())
        self.assertEqual(response.status_code, 200)

    def test_read_unread_filter(self):
        account = Account(title="Dummy Account", number=16)
        account.save()

        read_bank_operation = Operation(
                bank_date='2018-02-23',
                date_paid='2018-02-23',
                bank_description='Read Bank operation',
                account=account,
                read=True,
                amount=15
        )
        read_bank_operation.save()
        unread_bank_operation = Operation(
                bank_date='2018-02-24',
                date_paid='2018-02-24',
                bank_description='Unread Bank operation',
                account=account,
                read=False,
                amount=13
        )
        unread_bank_operation.save()
        pending_operation = Operation(
                date_paid='2018-02-21',
                description='Pending operation',
                method="Virement",
                account=account,
                amount=10,
                read=True  # pending operations are read by default
        )
        pending_operation.save()

        # By default, do not distinguish read status:
        response = self.client.post(reverse('operations_search'), {
            "account": [account.pk],
            "select_pendings": True,
            "select_bank_operations": True,
            "read": True,
            "unread": True
        })
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['pendings']), 1)
        self.assertIn(pending_operation, response.context['pendings'])
        self.assertEqual(len(response.context['bank_operations']), 2)
        self.assertIn(read_bank_operation, response.context['bank_operations'])
        self.assertIn(unread_bank_operation, response.context['bank_operations'])

        # Only read:
        response = self.client.post(reverse('operations_search'), {
            "account": [account.pk],
            "select_pendings": True,
            "select_bank_operations": True,
            "unread": False,
            "read": True
        })
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['pendings']), 1)
        self.assertIn(pending_operation, response.context['pendings'])
        self.assertEqual(len(response.context['bank_operations']), 1)
        self.assertIn(read_bank_operation, response.context['bank_operations'])

        # Only unread:
        response = self.client.post(reverse('operations_search'), {
            "account": [account.pk],
            "select_pendings": True,
            "select_bank_operations": True,
            "read": False,
            "unread": True
        })
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['form'].is_valid())
        self.assertEqual(len(response.context['pendings']), 0)
        self.assertEqual(len(response.context['bank_operations']), 1)
        self.assertIn(unread_bank_operation, response.context['bank_operations'])

        # Neither read nor unread is an error:
        response = self.client.post(reverse('operations_search'), {
            "account": [account.pk],
            "select_pendings": True,
            "select_bank_operations": True,
            "read": False,
            "unread": False
        })
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.context['form'].is_valid())


class OperationUnlinkViewTest(TestCase):
    def test_get(self):
        account = Account(title="DummyAccount", number=13)
        account.save()

        operation_a = Operation(
            date_paid='2020-12-22',
            bank_date='2020-12-22',
            description='Stuff A',
            bank_description="Bank Stuff A",
            method="Card",
            account=account,
            amount=-50
        )
        operation_a.save()

        response = self.client.get(reverse('operations_unlink', args=[operation_a.pk]))
        self.assertEqual(response.status_code, 405)

    def test_post(self):
        account = Account(title="DummyAccount", number=13)
        account.save()

        operation_a = Operation(
            date_paid='2020-12-22',
            bank_date='2020-12-22',
            description='Stuff A',
            bank_description="Bank Stuff A",
            method="Card",
            account=account,
            amount=-50
        )
        operation_a.save()

        response = self.client.post(reverse('operations_unlink', args=[operation_a.pk]))
        self.assertEqual(response.status_code, 302)

        self.assertEqual(Operation.objects.count(), 2)
        operations = Operation.objects.all().order_by("pk")
        self.assertTrue(operations[0].is_bank_operation)
        self.assertFalse(operations[1].is_bank_operation)


class OperationDeleteViewTest(TestCase):
    def test_post(self):
        account = Account(title="DummyAccount", number=13)
        account.save()

        operation_a = Operation(
            date_paid='2020-12-22',
            description='Stuff A',
            method="Card",
            account=account,
            amount=-50
        )
        operation_a.save()

        response = self.client.post(reverse('operations_delete', args=[operation_a.pk]))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Operation.objects.count(), 0)
