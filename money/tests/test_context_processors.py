from django.test import TestCase

from money.context_processors import app_settings


class ContextProcessorTest(TestCase):
    def test_app_settings(self):
        settings = app_settings()

        self.assertIn('app', settings)
        self.assertIn('paginate_by', settings['app'])
        self.assertIn('import_time_limit', settings['app'])
        self.assertIn('site_name', settings['app'])
        self.assertIn('hidden_pending_operations_time_boundary', settings['app'])
        self.assertIn('git_repository', settings)
        self.assertIn('browser_extension_url', settings)
