import datetime

from django.conf import settings

from money.models.import_authorization import ImportAuthorization


class UpdateImportTimeMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def _reset_session(self, request):
        if 'import_remaining_seconds' in request.session:
            del request.session['import_remaining_seconds']

    def __call__(self, request):
        grant = ImportAuthorization.objects.first()
        if grant is not None:
            timeout = grant.date + datetime.timedelta(seconds=settings.MONEY['import_time_limit'])
            now = datetime.datetime.now()
            if now < timeout:
                request.session['import_remaining_seconds'] = int((timeout - now).total_seconds())
            else:
                self._reset_session(request)
        else:
            self._reset_session(request)

        return self.get_response(request)
