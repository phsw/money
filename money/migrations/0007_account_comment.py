# Generated by Django 3.0.2 on 2020-04-07 18:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('money', '0006_auto_20200314_1812'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='comment',
            field=models.TextField(blank=True),
        ),
    ]
