# Generated by Django 3.0.2 on 2020-02-22 12:40

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    replaces = [('money', '0003_auto_20200221_2114'), ('money', '0004_auto_20200222_1046'), ('money', '0005_auto_20200222_1051'), ('money', '0006_auto_20200222_1108'), ('money', '0007_auto_20200222_1109')]

    dependencies = [
        ('money', '0002_operation_receipt_file_squashed_0006_operationreceipt_filename'),
    ]

    operations = [
        migrations.AlterField(
            model_name='operation',
            name='_date_paid',
            field=models.DateField(db_column='date_paid', default=datetime.date.today),
        ),
        migrations.CreateModel(
            name='OperationTemplate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('amount', models.FloatField(blank=True, default=None, null=True)),
                ('description', models.CharField(blank=True, default=None, max_length=255, null=True)),
                ('comment', models.TextField(blank=True, default=None, null=True)),
                ('method', models.CharField(blank=True, default=None, max_length=50, null=True)),
                ('categories', models.ManyToManyField(blank=True, to='money.Category')),
                ('accounts', models.ManyToManyField(to='money.Account')),
            ],
        ),
        migrations.AddConstraint(
            model_name='operationtemplate',
            constraint=models.UniqueConstraint(fields=('title',), name='unique_name'),
        ),
        migrations.AddConstraint(
            model_name='operationtemplate',
            constraint=models.CheckConstraint(check=models.Q(models.Q(_negated=True, amount=None), models.Q(_negated=True, description=None), models.Q(_negated=True, comment=''), models.Q(_negated=True, method=None), _connector='OR'), name='must_provide_data'),
        ),
    ]
