from django.conf import settings


def app_settings(request=None):
    """A context processor to get application settings in templates."""
    return {
        'app': settings.MONEY,
        'git_repository': "https://gitlab.com/phsw/money",
        'browser_extension_url': "https://gitlab.com/phsw/money/-/blob/master/INSTALL.md#browser-extension",  # noqa: E501
    }
