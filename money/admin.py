from django.contrib import admin

from money.models.accounts import Account
from money.models.operations import Operation, OperationReceipt
from money.models.operation_templates import OperationTemplate

admin.site.register(Account)
admin.site.register(Operation)
admin.site.register(OperationReceipt)
admin.site.register(OperationTemplate)
