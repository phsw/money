import datetime
import traceback
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView
from django.views.generic.detail import SingleObjectMixin
from django.views import View
from django.conf import settings
from django.db.models import Q, Sum, F, Count
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.core.files.uploadhandler import TemporaryFileUploadHandler
from django.utils.decorators import method_decorator
from django.db import transaction
from django.shortcuts import redirect

from money.models.accounts import Account
from money.models.operations import Operation
from money.forms.accounts import AccountForm
from money.mixins import SelectMonthMixin, ListDestAccountsToCloneOperationMixin
from money.utils.import_data import ImportCsvOperations, ImportCsvBankOperations
from money.utils import graphs, operations, stats


class AccountIndexView(ListView):
    template_name = 'accounts/index.html'
    context_object_name = 'accounts'
    queryset = Account.objects.all().annotate(
        occupancy=100*F('balance') / F('balance_limit'),
        nb_not_read=Count(
            'operation',
            filter=Q(operation__read=False) & ~Q(operation___bank_date=None)
        ),
        nb_pendings=Count('operation', filter=Q(operation___bank_date=None))
    ).order_by('title')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['sum_accounts'] = sum(
            [a.balance for a in context['object_list'] if a.balance is not None]
        )

        return context


class AccountDetailView(SingleObjectMixin, ListView, ListDestAccountsToCloneOperationMixin):
    model = Account
    template_name = 'accounts/detail.html'
    object = None
    paginate_by = settings.MONEY['paginate_by']
    asked_date = None

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=Account.objects.all().annotate(
            occupancy=100*F('balance') / F('balance_limit')
        ))

        date = request.GET.get('date', None)
        current_page = int(request.GET.get('page', 1))

        if date is not None:
            self.asked_date = datetime.date.fromisoformat(date)
            target_page = operations.get_page_containing_date(self.object, self.asked_date)

            if current_page != target_page:
                url_params = "?page=" + str(target_page) + "&date=" + str(self.asked_date)
                return redirect(
                    reverse_lazy('accounts_view', kwargs={'pk': self.object.pk}) + url_params
                )

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['hidden_operations'] = operations.hidden_operations(
            context['object_list'], self.object
        )

        context['asked_date'] = self.asked_date

        context['nb_not_read'] = Operation.objects.filter(
            Q(account=self.object) & ~Q(_bank_date=None) & Q(read=False)
        ).count()

        context['local_balance_without_pending'] = Operation.objects.filter(
            Q(account=self.object) & ~Q(_bank_date=None)
        ).aggregate(Sum('_amount'))['_amount__sum']

        if context['local_balance_without_pending'] is not None:
            context['local_balance_without_pending'] = round(
                context['local_balance_without_pending'] + self.object.initial_amount, 2
            )

            if self.object.balance is not None:
                context['diff_bank_local_balance'] = abs(
                    self.object.balance - context['local_balance_without_pending']
                )

        if not self.object.no_pendings:
            context['pending_operations'] = Operation.objects.filter(
                Q(account=self.object) & Q(_bank_date=None)
            ).select_related('account').order_by('-_date_paid').prefetch_related(
                'categories',
                'operationreceipt_set',
                'linked_operations',
                'linked_operations__account'
            )

            context['pending_balance'] = Operation.objects.filter(
                Q(account=self.object) & Q(_bank_date=None)
            ).aggregate(Sum('_amount'))['_amount__sum'] or 0.

            context['local_balance_with_pending'] = context['pending_balance']
            if context['local_balance_without_pending'] is not None:
                context['local_balance_with_pending'] += context['local_balance_without_pending']

            date_boundary = datetime.date.today() + datetime.timedelta(
                days=settings.MONEY['hidden_pending_operations_time_boundary']
            )
            context['hidden_pending_operations'] = Operation.objects.filter(
                Q(account=self.object) & Q(_bank_date=None) & Q(_date_paid__gte=date_boundary)
            )

        if self.object.support_amount:
            context['total_from_support_amount'] = Operation.objects.filter(
                Q(account=self.object) & ~Q(_bank_date=None) & Q(takes_from_support=True)
            ).aggregate(Sum('_amount'))['_amount__sum'] or 0.

            context['current_support_amount'] = round(
                self.object.support_amount - context['total_from_support_amount'], 2
            )

        return context

    def get_queryset(self, *args, **kwargs):
        return Operation.objects.filter(
                Q(account=self.object) & ~Q(_bank_date=None)
        ).select_related('account').order_by(
            '-_bank_date', '-import_datetime', '-_date_paid'
        ).prefetch_related(
            'categories', 'operationreceipt_set', 'linked_operations', 'linked_operations__account'
        )


class AccountGraphView(SelectMonthMixin, DetailView):
    model = Account
    template_name = 'accounts/graphs.html'
    object = None

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=Account.objects.all())

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        graph_monthly_balance, display_balance_limit = graphs.get_account_monthly_balances(
            self.object, self.end_asked_month
        )
        context['graph_monthly_balance'] = graph_monthly_balance[-24:]
        context['display_balance_limit'] = display_balance_limit

        context['graph_expense_categories'] = graphs.get_account_categories_expenses_repartition(
            self.object, self.begin_asked_month, self.end_asked_month
        )
        context['expense_categories_total'] = sum(
            [r['balance'] for r in context['graph_expense_categories']]
        )
        context['graph_income_categories'] = graphs.get_account_categories_incomes_repartition(
            self.object, self.begin_asked_month, self.end_asked_month
        )
        context['income_categories_total'] = sum(
            [r['balance'] for r in context['graph_income_categories']]
        )

        context['categories_repartition'] = graphs.get_account_categories_repartition(self.object)
        context['graph_categories_repartition'] = [
            e for e in context['categories_repartition'] if e['balance'] > 0
        ]

        return context


class AccountStatsView(SelectMonthMixin, DetailView):
    model = Account
    template_name = 'accounts/stats.html'
    object = None

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=Account.objects.all())

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['month_repartition'] = stats.get_categories_repartition_by_months(
            self.object, self.end_asked_month
        )
        context['year_repartition'] = stats.get_categories_repartition_by_years(
            self.object, self.asked_date.year+1
        )

        return context


@method_decorator(csrf_exempt, 'dispatch')
class AccountAddView(CreateView):
    model = Account
    template_name = 'accounts/add.html'
    form_class = AccountForm
    object = None

    def post(self, request, *args, **kwargs):
        request.upload_handlers = [TemporaryFileUploadHandler(request)]
        return self._post(request, *args, **kwargs)

    @method_decorator(csrf_protect)
    def _post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        try:
            with transaction.atomic():
                self.object = form.save()

                if 'csvOperations' in form.files and not self.object.no_pendings:
                    try:
                        importCsvOperations = ImportCsvOperations(
                            form.files['csvOperations'].temporary_file_path(),
                            self.object
                        )
                        importCsvOperations.process()
                    except Exception as e:
                        traceback.print_exc()
                        raise Exception("csvOperations", str(e))

                if 'csvBankOperations' in form.files:
                    try:
                        importCSvBankOperations = ImportCsvBankOperations(
                            form.files['csvBankOperations'].temporary_file_path(),
                            self.object
                        )
                        importCSvBankOperations.process()
                    except Exception as e:
                        traceback.print_exc()
                        raise Exception("csvBankOperations", str(e))
        except Exception as e:
            field, msg = e.args
            form.add_error(field, msg)
            return super().form_invalid(form)

        return redirect(self.get_success_url())

    def get_success_url(self):
        return reverse_lazy('accounts_view', kwargs={'pk': self.object.pk})


class AccountEditView(UpdateView):
    model = Account
    template_name = 'accounts/edit.html'
    form_class = AccountForm

    def get_success_url(self):
        return reverse_lazy('accounts_view', kwargs={'pk': self.object.pk})


class AccountDeleteView(DeleteView):
    model = Account
    template_name = None
    http_method_names = ["delete", "post"]
    success_url = reverse_lazy('accounts_index')


class AccountExportCsvView(SingleObjectMixin, View):
    model = Account

    def get(self, request, *args, **kwargs):
        account = self.get_object()

        results = Operation.objects.filter(
            Q(account=account) & ~Q(_bank_date=None)
        ).order_by('_bank_date', 'import_datetime', '_date_paid').prefetch_related('categories')

        return operations.operations_to_csv(results, account.title)
