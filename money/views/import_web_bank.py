import datetime
import string
import random

from django.views import View
from django.http import HttpResponseBadRequest, JsonResponse
from django.views.generic.base import TemplateView
from django.utils.translation import gettext_lazy as _
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.template.defaultfilters import pluralize
from django.urls import reverse
from django.conf import settings

from money.models.accounts import Account
from money.models.import_authorization import ImportAuthorization
from money.utils.import_data import ImportWebBankOperations


class ImportFromBankView(View):
    """
    View called when exporting data from bank website, with browser extension.
    """

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def post(self, request):
        grant = ImportAuthorization.objects.first()

        if grant is None:
            response = _("Import not allowed.")
        else:
            t = datetime.datetime.now()
            elapsed_time = abs((t - grant.date).total_seconds())

            if (request.POST['key'] != grant.key
                    or elapsed_time > settings.MONEY['import_time_limit']):
                response = _("Import not allowed.")
            elif 'content' not in request.POST or 'parser' not in request.POST:
                response = _("Error: Missing information.")
            else:
                try:
                    import_bank_account = ImportWebBankOperations(
                        request.POST['content'],
                        request.POST['parser']
                    )
                    import_bank_account.process()
                    response = _(
                        "Import succeded: {0} imported operation{1}, "
                        "{2} matching pending operation{3}"
                    ).format(
                        import_bank_account.nb_new,
                        pluralize(import_bank_account.nb_new),
                        import_bank_account.nb_pendings,
                        pluralize(import_bank_account.nb_pendings)
                    )

                    if import_bank_account.nb_pendings+import_bank_account.nb_new > 0:
                        imported_accounts = request.session.get('imported_accounts', [])
                        already_in_list = False
                        for a in imported_accounts:
                            if a['pk'] == str(import_bank_account.account.id):
                                already_in_list = True
                                break
                        if not already_in_list:
                            imported_accounts.append({
                                'url': reverse(
                                    'accounts_view',
                                    kwargs={'pk': import_bank_account.account.id}
                                ),
                                'title': import_bank_account.account.title,
                                'pk': str(import_bank_account.account.id)
                            })
                            request.session['imported_accounts'] = imported_accounts
                except Exception as e:
                    response = _("Error: {}").format(e)
                    # raise e

        return JsonResponse({'response': response})


class ImportWebBankAllowView(TemplateView):
    template_name = "allow_import.html"
    grant_key = None

    def get(self, request, *args, **kwargs):
        ImportAuthorization.objects.all().delete()

        self.present_accounts = Account.objects.all().count() > 0

        if self.present_accounts:
            self.grant_key = ''.join(
                random.choice(string.ascii_lowercase + string.digits) for _ in range(64)
            )

            grant = ImportAuthorization(key=self.grant_key)
            grant.save()

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['key'] = self.grant_key
        context['hide_toast'] = True
        context['present_accounts'] = self.present_accounts

        return context


class ImportWebBankAjaxImportedAccountsView(View):
    """
    View called by AJAX call on allow_import page, to notify imported accounts
    """
    def get(self, request):
        return JsonResponse(request.session.get('imported_accounts', []), safe=False)


class ImportWebBankDeleteImportedAccountView(View):
    http_method_names = ["post"]

    def post(self, request, *args, **kwargs):
        if request.headers.get('x-requested-with') != 'XMLHttpRequest':
            return HttpResponseBadRequest()

        new_imported_accounts = []
        for a in request.session.get('imported_accounts', []):
            if a['pk'] != str(kwargs['account_pk']):
                new_imported_accounts.append(a)
        request.session['imported_accounts'] = new_imported_accounts

        return JsonResponse({"result": True})


class ImportWebBankDeleteAllImportedAccountsView(View):
    http_method_names = ["post"]

    def post(self, request, *args, **kwargs):
        if request.headers.get('x-requested-with') != 'XMLHttpRequest':
            return HttpResponseBadRequest()

        if 'imported_accounts' in request.session:
            del request.session['imported_accounts']

        return JsonResponse({"result": True})
