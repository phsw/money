from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView, DetailView
from django.views import View
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import FormView
from django.http import HttpResponseBadRequest, JsonResponse
from django.shortcuts import redirect, render
from django.utils.translation import gettext_lazy as _
from django.db import transaction
from django.db.models import Q

from money.mixins import ListDestAccountsToCloneOperationMixin
from money.models.accounts import Account
from money.models.categories import Category
from money.models.operations import Operation
from money.models.operation_templates import OperationTemplate
from money.forms.operations import OperationForm, BankOperationSetPendingForm, \
    OperationReceiptFormSet, OperationPaidDateFormSet, OperationSearchForm
from money.utils import operations as operation_utils


class OperationAddView(CreateView, SingleObjectMixin):
    model = Account
    template_name = 'operations/add.html'
    form_class = OperationForm
    account = None

    def dispatch(self, request, *args, **kwargs):
        self.account = self.get_object()

        if self.account.no_pendings:
            return HttpResponseBadRequest(
                _("Cannot add operation to an account with disabled pending operations.")
            )

        return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        """
        New operations can be:
        - empty (default case)
        - copied from another operation
        - pre-filled with informations coming from an operation template
        - a refund, then the new operation is prefilled with informations
          coming from another operation
        """
        initial = super().get_initial()
        pattern = None
        is_refund = False
        operation_template_pk = self.request.GET.get('template', None)
        duplicated_operation_pk = self.request.GET.get('duplicate', None)
        refunded_operation_pk = self.request.GET.get('refund', None)

        if refunded_operation_pk is not None:
            is_refund = True
            duplicated_operation_pk = refunded_operation_pk

        if operation_template_pk is not None and operation_template_pk.isdigit():
            op_template = OperationTemplate.objects.filter(pk=operation_template_pk).first()

            if op_template is not None and self.account in op_template.accounts.all():
                pattern = op_template
        elif duplicated_operation_pk is not None and duplicated_operation_pk.isdigit():
            duplicated_op = Operation.objects.filter(pk=duplicated_operation_pk).first()

            if duplicated_op is not None:
                pattern = duplicated_op

        if pattern is not None:
            initial = {**initial, **operation_utils.get_init_values(pattern, is_refund)}

        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['account'] = self.account

        if self.request.POST:
            context['form_dates'] = OperationPaidDateFormSet(
                self.request.POST,
                self.request.FILES,
                prefix="dates"
            )
            context['form_receipts'] = OperationReceiptFormSet(
                self.request.POST,
                self.request.FILES,
                instance=self.object,
                prefix="receipts"
            )
        else:
            context['form_dates'] = OperationPaidDateFormSet(prefix="dates")
            context['form_receipts'] = OperationReceiptFormSet(
                instance=self.object,
                prefix="receipts"
            )

        return context

    def form_valid(self, form):
        context = self.get_context_data()
        form_receipts = context['form_receipts']
        form_dates = context['form_dates']

        if not form_dates.is_valid():
            return super().form_invalid(form_dates)

        with transaction.atomic():
            first_operation_pk = None
            first_operation_date_paid = None
            for f in form_dates:
                form.instance.pk = None  # Force the creation of a new operation
                form.instance.date_paid = f.cleaned_data['date_paid']
                form.instance.account = self.account
                form.instance.read = True
                form.save()
                if (first_operation_pk is None
                        or form.instance.date_paid < first_operation_date_paid):
                    first_operation_date_paid = form.instance.date_paid
                    first_operation_pk = form.instance.pk

                if form.cleaned_data['all_to_all']:
                    operation_utils.all_to_all_linked_operations(form.instance)

            # Save receipts to the first operation:
            if form_receipts.is_valid():
                form_receipts.instance = Operation.objects.get(pk=first_operation_pk)
                form_receipts.save()

        url = reverse_lazy('accounts_view', kwargs={'pk': self.account.pk}) + "#tab_tables"
        return redirect(url)


class OperationEditView(UpdateView):
    model = Operation
    template_name = 'operations/edit.html'
    form_class = OperationForm

    def get(self, request, *args, **kwargs):
        # TODO: check referer is from this app
        request.session['edit_referer'] = request.META.get('HTTP_REFERER')
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.POST:
            context['form_receipts'] = OperationReceiptFormSet(
                self.request.POST,
                self.request.FILES,
                instance=self.object,
                prefix="receipts"
            )
        else:
            context['form_receipts'] = OperationReceiptFormSet(
                instance=self.object,
                prefix="receipts"
            )

        return context

    def form_valid(self, form):
        context = self.get_context_data()
        form_receipts = context['form_receipts']

        with transaction.atomic():
            self.object = form.save()

            if form_receipts.is_valid():
                form_receipts.instance = self.object
                form_receipts.save()

            if form.cleaned_data['all_to_all']:
                operation_utils.all_to_all_linked_operations(self.object)

        return redirect(self.get_success_url())

    def get_success_url(self):
        url = self.request.session.get('edit_referer', None)

        if url is None:
            url = reverse_lazy('accounts_view', kwargs={'pk': self.object.account.pk})
        else:
            url += "#tab_tables"
            del self.request.session['edit_referer']

        return url


class OperationDeleteView(DeleteView):
    model = Operation
    template_name = None
    http_method_names = ["delete", "post"]

    def get_success_url(self):
        return reverse_lazy('accounts_view', kwargs={'pk': self.object.account.pk}) + "#tab_tables"


class OperationDetailView(DetailView, ListDestAccountsToCloneOperationMixin):
    model = Operation
    template_name = 'operations/detail.html'
    object = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['pending_linked'] = self.object.linked_operations.select_related('account').filter(
            Q(_bank_date=None)
        ).prefetch_related(
            'categories', 'operationreceipt_set', 'linked_operations', 'linked_operations__account'
        ).order_by('-_date_paid')
        context['sum_pending_linked'] = sum([o.amount for o in context['pending_linked']])

        context['bank_linked'] = self.object.linked_operations.select_related('account').filter(
            ~Q(_bank_date=None)
        ).prefetch_related(
            'categories', 'operationreceipt_set', 'linked_operations', 'linked_operations__account'
        ).order_by('-_bank_date', '-import_datetime', '-_date_paid')
        context['sum_bank_linked'] = sum([o.amount for o in context['bank_linked']])

        return context

    def get_queryset(self, *args, **kwargs):
        return Operation.objects.all().select_related('account').prefetch_related(
            'categories', 'operationreceipt_set', 'linked_operations', 'linked_operations__account'
        )


class OperationMarkAllReadView(SingleObjectMixin, View):
    model = Account
    http_method_names = ["post"]

    def post(self, request, *args, **kwargs):
        account = self.get_object()

        operations = Operation.objects.filter(account=account, read=False)

        for o in operations:
            o.read = True

        Operation.objects.bulk_update(operations, fields=['read'])

        # TODO: check referer is from this app
        url = request.META.get('HTTP_REFERER') + "#tab_tables"
        return redirect(url)


class OperationUnlinkView(SingleObjectMixin, View):
    model = Operation
    operation = None
    http_method_names = ["post"]

    def dispatch(self, request, *args, **kwargs):
        self.operation = self.get_object()

        if not self.operation.is_merged_with_pending:
            return HttpResponseBadRequest()

        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        operation_utils.unlink(self.operation)

        # TODO: check referer is from this app
        url = request.META.get('HTTP_REFERER')

        if url is None:
            url = reverse_lazy('accounts_view', kwargs={'pk': self.operation.account.pk})
        else:
            url += "#tab_tables"

        return redirect(url)


class OperationSetReadView(SingleObjectMixin, View):
    model = Operation
    operation = None
    http_method_names = ["post"]

    def dispatch(self, request, *args, **kwargs):
        self.operation = self.get_object()

        # We can set reading status only to a bank operation (merged or not with a pending)
        if not self.operation.is_bank_operation:
            return HttpResponseBadRequest()

        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.operation.read = True
        self.operation.save()

        if request.headers.get('x-requested-with') == 'XMLHttpRequest':
            return JsonResponse({"result": True})
        else:
            # TODO: check referer is from this app
            url = request.META.get('HTTP_REFERER')

            if url is None:
                url = reverse_lazy('accounts_view', kwargs={'pk': self.operation.account.pk})
            else:
                url += "#tab_tables"

            return redirect(url)


class BankOperationSetPendingView(UpdateView):
    model = Operation
    template_name = 'operations/set_pending.html'
    form_class = BankOperationSetPendingForm
    operation = None

    def dispatch(self, request, *args, **kwargs):
        self.operation = self.get_object()

        if self.operation.is_merged_with_pending:
            return HttpResponseBadRequest()

        return super().dispatch(request, *args, **kwargs)

    def get_form(self, form_class=BankOperationSetPendingForm):
        if self.request.method == "POST":
            return self.form_class(self.operation, self.request.POST)
        else:
            return self.form_class(self.operation)

    def get(self, request, *args, **kwargs):
        # TODO: check referer is from this app
        request.session['link_referer'] = request.META.get('HTTP_REFERER')
        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        pending_operation = form.cleaned_data['pending_operation']
        account = self.operation.account

        if pending_operation.date_paid > self.operation.bank_date:
            form.add_error(
                "pending_operation",
                _("Transaction cannot be paid after bank registered it !")
            )
            return super().form_invalid(form)

        operation_utils.merge_bank_in_pending_operation(self.operation, pending_operation)
        pending_operation.save()
        self.operation.delete()

        url = self.request.session.get('link_referer', None)

        if url is None:
            url = reverse_lazy('accounts_view', kwargs={'pk': account.pk})
        else:
            url += "#tab_tables"
            del self.request.session['link_referer']

        return redirect(url)

    def get_queryset(self, *args, **kwargs):
        return Operation.objects.all().select_related('account').order_by(
            '-_bank_date', '-import_datetime', '-_date_paid'
        ).prefetch_related('categories')


class OperationSearchView(FormView, ListDestAccountsToCloneOperationMixin):
    template_name = "operations/search.html"
    form_class = OperationSearchForm

    def get_initial(self):
        account_pk = self.request.GET.get('account', None)
        if account_pk is not None:
            account = Account.objects.filter(pk=account_pk).first()
            if account is not None:
                return {'account': account}

        category_pk = self.request.GET.get('category', None)
        if category_pk is not None:
            category = Category.objects.filter(pk=category_pk).first()
            if category is not None:
                return {'categories': category}

        return super().get_initial()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['bank_operations'] = []
        context['pendings'] = []
        context['display_results'] = False

        return context

    def form_valid(self, form):
        has_error = False

        if (form.cleaned_data['select_bank_operations'] is False
                and form.cleaned_data['select_pendings'] is False):
            form.add_error(
                "select_bank_operations",
                _("You must select at least one type of operation !")
            )
            form.add_error("select_pendings", "")
            has_error = True

        if (form.cleaned_data['date_end'] is not None
                and form.cleaned_data['date_start'] is not None
                and form.cleaned_data['date_end'] < form.cleaned_data['date_start']):
            form.add_error(
                'date_start',
                _("Date in this field must be before date in \"Paid before\" field.")
            )
            form.add_error('date_end', "")
            has_error = True

        if form.cleaned_data['read'] is False and form.cleaned_data['unread'] is False:
            form.add_error("read", _("You must select read and/or not read operations."))
            form.add_error("unread", "")
            has_error = True

        if has_error:
            return super().form_invalid(form)

        criteria = Q(account__in=form.cleaned_data['account'])

        if form.cleaned_data['date_start'] is not None:
            criteria &= Q(_date_paid__gte=form.cleaned_data['date_start'])

        if form.cleaned_data['date_end'] is not None:
            criteria &= Q(_bank_date__lte=form.cleaned_data['date_end'])

        if form.cleaned_data['amount'] is not None:
            criteria &= Q(_amount=form.cleaned_data['amount'])

        if form.cleaned_data['description'] != '':
            criteria &= (Q(description__contains=form.cleaned_data['description'])
                         | Q(comment__contains=form.cleaned_data['description'])
                         | Q(bank_description__contains=form.cleaned_data['description'])
                         | Q(bank_comment__contains=form.cleaned_data['description']))

        if form.cleaned_data['read'] is False:
            criteria &= Q(read=False)
        if form.cleaned_data['unread'] is False:
            criteria &= Q(read=True)

        context = self.get_context_data()

        if form.cleaned_data['select_bank_operations']:
            results = Operation.objects.filter(
                criteria & ~Q(_bank_date=None)
            ).select_related('account').order_by(
                '-_bank_date', '-import_datetime', '-_date_paid'
            ).prefetch_related(
                'categories',
                'operationreceipt_set',
                'linked_operations',
                'linked_operations__account'
            )
            context['bank_operations'] = operation_utils.filter_by_categories(
                results,
                form.cleaned_data['categories'],
                form.cleaned_data['has_all_categories'],
                form.cleaned_data['has_only_these_categories']
            )

        if 'csv_export' in form.data:
            return operation_utils.operations_to_csv(context['bank_operations'], 'results')

        if form.cleaned_data['select_pendings']:
            results = Operation.objects.filter(
                criteria & Q(_bank_date=None)
            ).select_related('account').order_by('-_date_paid').prefetch_related(
                'categories',
                'operationreceipt_set',
                'linked_operations',
                'linked_operations__account'
            )
            context['pendings'] = operation_utils.filter_by_categories(
                results,
                form.cleaned_data['categories'],
                form.cleaned_data['has_all_categories'],
                form.cleaned_data['has_only_these_categories']
            )

        context['form'] = form
        context['display_results'] = True
        return render(self.request, self.template_name, context)
