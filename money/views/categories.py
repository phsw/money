from django.conf import settings
from django.db.models import Q
from django.http import JsonResponse
from django.views import View
from django.views.generic import CreateView, ListView, UpdateView, DeleteView
from django.views.generic.detail import SingleObjectMixin
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _

from money.forms.categories import CategoryForm
from money.mixins import ListDestAccountsToCloneOperationMixin
from money.models.categories import Category
from money.models.operations import Operation
from money.utils import auto_affect_categories, graphs


class CategoryIndexView(ListView):
    template_name = 'categories/index.html'
    context_object_name = 'categories'
    queryset = Category.objects.all().order_by('title')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['without_category'] = Category.without_category()

        return context


class CategoryDetailView(SingleObjectMixin, ListView, ListDestAccountsToCloneOperationMixin):
    model = Category
    template_name = 'categories/detail.html'
    object = None
    paginate_by = settings.MONEY['paginate_by']

    def get_object(self, queryset=None):
        assert (queryset is not None)

        pk = self.kwargs.get(self.pk_url_kwarg)
        if pk is not None:
            if pk == 0:
                return Category.without_category()
            else:
                return super().get_object(queryset)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=Category.objects.all())
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        graph_monthly_balance = graphs.get_category_monthly_balance(self.object)
        context['graph_monthly_balance'] = graph_monthly_balance[-24:]

        context['repartition'] = graphs.get_category_repartition(self.object)
        context['graph_repartition'] = [e for e in context['repartition'] if e['balance'] > 0]
        context['repartition_sum'] = sum([r['balance'] for r in context['repartition']])

        context['repartition_errors'] = []
        error_msg = _('''This category has more money on account <a href="%s">%s</a>
             than there is on this account !''')
        for c in context['graph_repartition']:
            if c['account__balance'] is not None and c['balance'] > c['account__balance']:
                context['repartition_errors'].append(
                    error_msg % (
                        reverse_lazy('accounts_view', kwargs={'pk': c['account']}),
                        c['account__title']
                    )
                )

        return context

    def get_queryset(self):
        return Operation.objects.filter(
            graphs.get_operation_category_filter(self.object)
        ).select_related('account').order_by(
            '-_bank_date', '-import_datetime', '-_date_paid'
        ).prefetch_related(
            'categories', 'operationreceipt_set', 'linked_operations', 'linked_operations__account'
        )


class CategoryAddView(CreateView):
    model = Category
    template_name = 'categories/add.html'
    form_class = CategoryForm

    def get_success_url(self):
        return reverse_lazy('categories_view', kwargs={'pk': self.object.pk})


class CategoryEditView(UpdateView):
    model = Category
    template_name = 'categories/edit.html'
    form_class = CategoryForm

    def get_success_url(self):
        return reverse_lazy('categories_view', kwargs={'pk': self.object.pk})


class CategoryDeleteView(DeleteView):
    model = Category
    http_method_names = ["delete", "post"]
    success_url = reverse_lazy('categories_index')


class CategoryGetAutoAffectView(View):
    def get(self, request):
        if 'desc' not in request.GET:
            return JsonResponse([], safe=False)

        categories = Category.objects.filter(~Q(auto_affect="")).order_by('title')
        matching = auto_affect_categories.get_matching_categories(request.GET['desc'], categories)

        return JsonResponse([c.id for c in matching], safe=False)
