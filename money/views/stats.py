from django.views.generic.base import TemplateView

from money.mixins import SelectMonthMixin
from money.utils import graphs, stats


class StatsView(SelectMonthMixin, TemplateView):
    template_name = 'stats/stats.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['graph_monthly_balance'] = graphs.get_all_accounts_monthly_balances(
            self.end_asked_month
        )[-24:]

        context['graph_yearly_balance'] = graphs.get_all_accounts_yearly_balances(
            self.end_asked_month
        )[-24:]

        context['month_repartition'] = stats.get_categories_repartition_by_months(
            None, self.end_asked_month
        )
        context['year_repartition'] = stats.get_categories_repartition_by_years(
            None, self.asked_date.year+1
        )

        return context
