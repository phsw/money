from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView

from money.models.operation_templates import OperationTemplate
from money.models.operations import Operation
from money.forms.operation_templates import OperationTemplateForm
from money.utils import operation_templates as utils


class OperationTemplateIndexView(ListView):
    template_name = 'operation_templates/index.html'
    context_object_name = 'operation_templates'
    queryset = OperationTemplate.objects.all().order_by('title').prefetch_related(
        'categories', 'accounts'
    )


class OperationTemplateAddView(CreateView):
    model = OperationTemplate
    template_name = 'operation_templates/add.html'
    form_class = OperationTemplateForm
    success_url = reverse_lazy('operation_templates_index')

    def get_initial(self):
        initial = super().get_initial()
        operation_pk = self.request.GET.get('operation', None)

        if operation_pk is not None and operation_pk.isdigit():
            operation = Operation.objects.filter(pk=operation_pk).prefetch_related(
                'categories', 'account'
            ).first()

            if operation is not None:
                initial = {**initial, **utils.get_init_values(operation)}

        return initial


class OperationTemplateEditView(UpdateView):
    model = OperationTemplate
    template_name = 'operation_templates/edit.html'
    form_class = OperationTemplateForm
    success_url = reverse_lazy('operation_templates_index')


class OperationTemplateDeleteView(DeleteView):
    model = OperationTemplate
    template_name = None
    http_method_names = ["delete", "post"]
    success_url = reverse_lazy('operation_templates_index')
