import datetime

from django.views.generic.base import ContextMixin

from money.models.accounts import Account


class SelectMonthMixin(ContextMixin):
    asked_date = None  # year-month-day
    begin_asked_month = None  # year-month-1
    end_asked_month = None  # year-month+1-1

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        date = self.request.GET.get('date', None)

        if date is not None:
            self.asked_date = datetime.datetime.strptime(date, "%B %Y").date()
        else:
            self.asked_date = datetime.date.today()

        context['asked_date'] = self.asked_date
        self.begin_asked_month = datetime.date(self.asked_date.year, self.asked_date.month, 1)
        end_month = self.begin_asked_month + datetime.timedelta(days=31)
        self.end_asked_month = datetime.date(end_month.year, end_month.month, 1)
        context['previous_month'] = self.begin_asked_month - datetime.timedelta(days=1)
        context['next_month'] = end_month
        context['display_next_month_button'] = (end_month < datetime.date.today())

        return context


class ListDestAccountsToCloneOperationMixin(ContextMixin):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['dest_accounts_clone_operation'] = Account.objects.filter(
            no_pendings=False
        ).order_by('title')

        return context
