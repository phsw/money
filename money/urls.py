from django.urls import path

from .views import accounts, operations, categories, operation_templates, import_web_bank, stats

urlpatterns = [
    path('', accounts.AccountIndexView.as_view(), name='homepage'),

    path('accounts', accounts.AccountIndexView.as_view(), name='accounts_index'),
    path('accounts/add', accounts.AccountAddView.as_view(), name='accounts_add'),
    path('accounts/view/<int:pk>', accounts.AccountDetailView.as_view(), name='accounts_view'),
    path(
        'accounts/view/<int:pk>/graphs',
        accounts.AccountGraphView.as_view(),
        name='accounts_graph'
    ),
    path(
        'accounts/view/<int:pk>/stats',
        accounts.AccountStatsView.as_view(),
        name='accounts_stats'
    ),
    path('accounts/edit/<int:pk>', accounts.AccountEditView.as_view(), name='accounts_edit'),
    path('accounts/delete/<int:pk>', accounts.AccountDeleteView.as_view(), name='accounts_delete'),
    path('accounts/csv/<int:pk>', accounts.AccountExportCsvView.as_view(), name='accounts_to_csv'),

    path(
        'accounts/<int:pk>/add_operation',
        operations.OperationAddView.as_view(),
        name='operations_add'
    ),
    path(
        'operations/view/<int:pk>',
        operations.OperationDetailView.as_view(),
        name='operations_view'
    ),
    path(
        'operations/edit/<int:pk>',
        operations.OperationEditView.as_view(),
        name='operations_edit'
    ),
    path(
        'operations/unlink/<int:pk>',
        operations.OperationUnlinkView.as_view(),
        name='operations_unlink'
    ),
    path(
        'operations/read/<int:pk>',
        operations.OperationSetReadView.as_view(),
        name='operations_read'
    ),
    path(
        'operations/set_pending/<int:pk>',
        operations.BankOperationSetPendingView.as_view(),
        name='operations_set_pending'
    ),
    path(
        'operations/delete/<int:pk>',
        operations.OperationDeleteView.as_view(),
        name='operations_delete'
    ),
    path(
        'operations/search',
        operations.OperationSearchView.as_view(),
        name="operations_search"
    ),
    path(
        'accounts/<int:pk>/mark_all_as_read',
        operations.OperationMarkAllReadView.as_view(),
        name='operations_mark_all_read'
    ),

    path(
        'import_web_bank/import_from_bank',
        import_web_bank.ImportFromBankView.as_view(),
        name="import_web_bank_import_from_bank"
    ),
    path(
        'import_web_bank/allow',
        import_web_bank.ImportWebBankAllowView.as_view(),
        name="import_web_bank_allow"
    ),
    path(
        'import_web_bank/imported_accounts',
        import_web_bank.ImportWebBankAjaxImportedAccountsView.as_view(),
        name="import_web_bank_imported_accounts"
    ),
    path(
        'import_web_bank/delete/<int:account_pk>',
        import_web_bank.ImportWebBankDeleteImportedAccountView.as_view(),
        name='import_web_bank_delete_imported_account'
    ),
    path(
        'import_web_bank/delete_all',
        import_web_bank.ImportWebBankDeleteAllImportedAccountsView.as_view(),
        name='import_web_bank_delete_all_imported_accounts'
    ),

    path('categories', categories.CategoryIndexView.as_view(), name='categories_index'),
    path('categories/add', categories.CategoryAddView.as_view(), name='categories_add'),
    path(
        'categories/view/<int:pk>',
        categories.CategoryDetailView.as_view(),
        name='categories_view'
    ),
    path('categories/edit/<int:pk>', categories.CategoryEditView.as_view(), name='categories_edit'),
    path(
        'categories/delete/<int:pk>',
        categories.CategoryDeleteView.as_view(),
        name='categories_delete'
    ),
    path(
        'categories/get_auto_affect',
        categories.CategoryGetAutoAffectView.as_view(),
        name='categories_get_auto_affect'
    ),

    path(
        'operation_templates',
        operation_templates.OperationTemplateIndexView.as_view(),
        name='operation_templates_index'
    ),
    path(
        'operation_templates/add',
        operation_templates.OperationTemplateAddView.as_view(),
        name='operation_templates_add'
    ),
    path(
        'operation_templates/edit/<int:pk>',
        operation_templates.OperationTemplateEditView.as_view(),
        name='operation_templates_edit'
    ),
    path(
        'operation_templates/delete/<int:pk>',
        operation_templates.OperationTemplateDeleteView.as_view(),
        name='operation_templates_delete'
    ),

    path('stats', stats.StatsView.as_view(), name="stats")
]
