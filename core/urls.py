from django.urls import path, include
from django.conf import settings
from django.contrib import admin


urlpatterns = [
    path('admin', admin.site.urls),
    path('', include('money.urls'))
]


if settings.DEBUG:  # pragma: no cover
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    from django.conf.urls.static import static

    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG_TOOLBAR_INSTALLED:
    import debug_toolbar

    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
