import importlib

from .common import *


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

if importlib.util.find_spec('debug_toolbar') is not None:
    DEBUG_TOOLBAR_INSTALLED = True

    INSTALLED_APPS.append('debug_toolbar')
    MIDDLEWARE.append('debug_toolbar.middleware.DebugToolbarMiddleware')

    # Keep this here, to easily switch to True, if needed:
    DEBUG_TOOLBAR_CONFIG = {
        'SHOW_TOOLBAR_CALLBACK': lambda r: False,
    }
