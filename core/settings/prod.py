from .common import *  # noqa: F401


DEBUG = False

ALLOWED_HOSTS = ["*"]

# Save backtrace of HTTP 500 errors in logs:
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    "formatters": {
        "verbose": {
            'format': '[%(asctime)s] [%(process)d] [%(levelname)s]  %(module)s  %(message)s',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'root': {
        'handlers': ['console'],
        'level': 'ERROR',
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': 'ERROR',
            'propagate': False,
        },
    }
}
