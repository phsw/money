# Money

[![pipeline status](https://gitlab.com/phsw/money/badges/master/pipeline.svg)](https://gitlab.com/phsw/money/-/commits/master) [![coverage report](https://gitlab.com/phsw/money/badges/master/coverage.svg)](https://gitlab.com/phsw/money/-/commits/master)


**Web application to manage personal finances.**

### Features:
- import bank operations from the website of your bank (only Crédit Agricole is supported for now)
- handle many accounts
- categorize operations (manually or automatically)
- register operations not yet registered by the bank (*pending* operations)
- search operations
- export operation tables to CSV
- graphs of account balance evolution, category balance evolution, category repartition among accounts, ...
- link operations together
- save documents related to operations
- operation templates (for pending operations)


### Getting started
See [installation guide](./INSTALL.md) and [upgrade guide](./UPGRADE.md).


### Todo

- multi-user system (authentification for each user, access only to your accounts)
- create accounts not related to bank (to count coins in your wallet, for instance)
- store account statements
- interface translation (adapt date format, amount representation, ...)

These are only ideas for a far future, I don't really need them right now. So if you are interested in one of these features, let me know, it will motivate me to develop them !
