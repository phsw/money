# Upgrade

Make a backup of your database and the content of the folder `uploads`.

Get the last version:
```bash
git pull
```

By comparing with `money/settings.py.dist`, check if there is no missing configuration keys in your `money/settings.py`.

Update back-end dependencies:
```bash
pip install -r requirements.txt
```

Apply database migrations:
```bash
python manage.py migrate
```

Update front-end dependencies:
```bash
yarn install --frozen-lockfile
```

Restart your server (eg Gunicorn).
