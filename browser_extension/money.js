content_key = document.getElementById("money_allow_import_key");
content_url = document.getElementById("money_allow_import_url");

if (content_key != null && content_url != null)
{
  let money_allow_import = {
    key: content_key.innerText,
    url: content_url.innerText
  };
  browser.storage.local.set({money_allow_import}).then(function() {
    document.getElementById("money_allow_import_fetch_key_try").classList.add("d-none");
    document.getElementById("money_allow_import_fetch_key_success").classList.remove("d-none");
  }, alert);
}
