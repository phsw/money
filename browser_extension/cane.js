var phsw_money_export_query_selector = ""
var phsw_money_export_parser = "";

const phsw_money_lang = navigator.appName == "Netscape" ? navigator.language : navigator.userLanguage;

const phsw_money_messages_i18n = {
  en: {
    errorLookConsole: "Error. Have a look on the console.",
    errorNoSelector: "Error: no query selector",
    errorNoToken: "Error: no token available to export data. Allow import in Money first.",
    exportButtonContent: "Export to Money"
  },
  fr: {
    errorLookConsole: "Erreur. Consultez la console.",
    errorNoSelector: "Erreur : pas de sélecteur disponible.",
    errorNoToken: "Erreur: pas de jeton disponible pour exporter les données. Commencez par autoriser l'import dans Money.",
    exportButtonContent: "Exporter vers Money"
  }
};

var phsw_money_messages = phsw_money_messages_i18n.en;
if (phsw_money_lang == "fr") {
  phsw_money_messages = phsw_money_messages_i18n.fr;
}

function phsw_money_alert_error_in_console(e) {
  console.log(e);
  alert(phsw_money_messages.errorLookConsole);
}

function phsw_money_export() {
  e = window.event;
  e.preventDefault();

  if (phsw_money_export_query_selector == "") {
    alert(phsw_money_messages.errorNoSelector);
    return;
  }

  var table = document.querySelector(phsw_money_export_query_selector);

  browser.storage.local.get("money_allow_import").then(function(item) {
    if (!Object.hasOwn(item, "money_allow_import"))
    {
      alert(phsw_money_messages.errorNoToken);
      return;
    }

    fetch(item.money_allow_import.url, {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: encodeURIComponent("key") + "=" + encodeURIComponent(item.money_allow_import.key)
            + "&" + encodeURIComponent("parser") + "=" + encodeURIComponent(phsw_money_export_parser)
            + "&" + encodeURIComponent("content") + "=" + encodeURIComponent(table.innerHTML)
    }).then(function(response) {
      if (response.ok)
      {
        return Promise.resolve(response.json());
      }
      throw Response.error();
    }).then(function(data) {
      alert(data.response);
    }).catch(phsw_money_alert_error_in_console);
  }, phsw_money_alert_error_in_console);
}

function phsw_money_create_export_button()
{
  var button = document.createElement("a");
  button.href = "#";
  button.textContent = phsw_money_messages.exportButtonContent;
  button.onclick = phsw_money_export;
  return button;
}

var phsw_money_start_time = new Date();
var phsw_money_timeout_timer = setInterval(function() {
  /* Try to find HTML elements to add the button for 10 seconds, in case the
   * page takes some time to load some JavaScript application... */

  var now = new Date();
  var elapsed_time = (now - phsw_money_start_time) / 1000;
  var found = false;

  if (document.querySelector("div.Synthesis-content") != null)
  {
      phsw_money_export_parser = "cane2020default";
      phsw_money_export_query_selector = "div.Synthesis-content";

      var button_list = document.querySelector(phsw_money_export_query_selector + " h1");
      if (button_list != null)
      {
        button_list.after(phsw_money_create_export_button());
        found = true;
      }
  }
  else
  {
    var button_list = document.querySelector("app-account-info");
    if (button_list != null)
    {
      phsw_money_export_parser = "cane2024epargne"
      phsw_money_export_query_selector = "app-root"

      button_list.appendChild(phsw_money_create_export_button());
      found = true;
    }
  }

  if (found || elapsed_time > 10)
  {
    /* We found HTML elements to add the button, or for 10 seconds we didn't
     * find anything, so we give up. */
    clearInterval(phsw_money_timeout_timer);
  }
}, 1000);
