// Chart.js
Chart.defaults.datasets.line.tension = 0.4;
Chart.defaults.animation = {
    duration: 0
};
Chart.defaults.responsiveAnimationDuration = 0;
var chart_tooltip_label = {
    callbacks: {
        label: function(context) {
            return context.label + ": " + context.parsed.toFixed(2) + "€";
        }
    }
};

function addChartColorBadgesInTables(labels, colors, target) {
    for (var i = 0; i < labels.length; i++) {
        // Putting "o" in the span is a hack to get (almost) a square shape:
        $('[data-graph-color="' + target + '-' + labels[i] + '"]').prepend('<span class="badge" style="background-color: ' + colors[i] + '; color: ' + colors[i] + ';">o</span>');
    }
}

var colors = [
    'rgb(255, 99, 132)', // red
    'rgb(255, 159, 64)', // orange
    'rgb(255, 205, 86)', // yellow
    'rgb(75, 192, 192)', // green
    'rgb(54, 162, 235)', // blue
    'rgb(153, 102, 255)', // purple
    'rgb(201, 203, 207)' // grey
];

// From https://github.com/nagix/chartjs-plugin-colorschemes/blob/master/src/colorschemes/colorschemes.tableau.js
var colors_tableau20 = [
    '#4e79a7',
    '#a0cbe8',
    '#f28e2b',
    '#ffbe7d',
    '#59a14f',
    '#8cd17d',
    '#b6992d',
    '#f1ce63',
    '#499894',
    '#86bcb6',
    '#e15759',
    '#ff9d9a',
    '#79706e',
    '#bab0ac',
    '#d37295',
    '#fabfd2',
    '#b07aa1',
    '#d4a6c8',
    '#9d7660',
    '#d7b5a6'
];


$(function () {
    // Display tab according to URL hash tag suffix:
    var hash = document.location.hash;
    var prefix = "tab_";
    if (hash) {
        $('.nav-tabs a[href="'+hash.replace(prefix,"")+'"]').tab('show');
    }

    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash.replace("#", "#" + prefix);
    });




    // Configure month picker:
    var today = new Date();
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    $(".monthpicker").datepicker({
        minViewMode: "months",
        format: 'MM yyyy',
        endDate: months[today.getMonth()] + " " + today.getFullYear(),
        zIndexOffset: 1022 // to avoid being hidden by operation table header
    });


    // Configure tooltip:
    $('[data-toggle="tooltip"]').tooltip();


    // Configure popover:
    $('[data-toggle="popover-hover"]').popover({
        trigger: 'hover',
        container: 'body'
    });


    // Modal window, used to confirm actions:
    $("form[data-modal='confirm']").submit(function(e, params) {
        /*
            * First time when the form is submitted, a popup is displayed to ask confirmation
            * If confirmed, the form is submitted again, but with the param send to really
            * submit the form, without JS interception. Thus, the page is reloaded.
            */
        var localParams = params || {};
        const jElement = $(e.currentTarget);

        if (!localParams.send) {
            e.preventDefault();

            Swal.fire({
                title: jElement.data('text'),
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#d33",
                confirmButtonText: "Yes",
                cancelButtonText: "Cancel",
                showCloseButton: true,
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return new Promise((resolve) => {
                        jElement.trigger(e.type, { 'send': true });
                        // Don't resolve the promise as we want the popup to stay open until the page is reloaded:
                        // resolve();
                    });
                }
            });
        }
    });


    // Configure Select2:
    $(".selectmultiple").select2({
        width: '100%'
    });


    // Configure crispy formsets:
    $('.receipts-formset-row').formset({
        addText: '<i class="fa fa-plus"></i>',
        addCssClass: 'btn btn-sm btn-primary',
        deleteText: '<i class="fa fa-trash"></i>',
        deleteCssClass: 'btn btn-danger',
        deleteContainerClass: 'receipts-formset-delete-button',
        addContainerClass: 'receipts-formset-add-button',
        formCssClass: 'receipts-dynamic-form',
        hideLastAddForm: true
    });
    $('.dates-formset-row').formset({
        addText: '<i class="fa fa-plus"></i>',
        addCssClass: 'btn btn-sm btn-primary',
        deleteText: '<i class="fa fa-trash"></i>',
        deleteCssClass: 'btn btn-sm btn-danger',
        deleteContainerClass: 'dates-formset-delete-button',
        formCssClass: 'dates-dynamic-form',
        addContainerClass: 'dates-formset-add-button',
        added: function (e) {
            installDatePicker();
        }
    });

    /* Workaround to display name of file to upload in the file field.
     * Bootstrap template of crispy_forms does it with a JavaScript event
     * listening on the change of the file field. Unfortunately
     * jquery.formset.js copies the file field when a new one is request, thus
     * the script listening events from this new field is not executed. */
    $('#receipts_fieldset').on('change', '.receipts-formset-row .custom-file-input', function(e) {
        // Adapted from crispy_forms/templates/bootstrap4/layout.field_file.html:
        e.target.parentNode.querySelector('.custom-file-label').textContent = e.target.files[0].name;
    });


    /* Configure date picker.
     * It has to be done after formsets may save the template, to avoid the
     * template form contains an event to the datepicker. */
    function installDatePicker() {
        $(".datepicker").datepicker({
            format: 'yyyy-mm-dd',
            todayBtn: "linked",
            zIndexOffset: 1024 // to avoid being hidden by operation table header
        });
    }
    installDatePicker();


    // Use AJAX to mark operations as read:
    $("form[id^='read-form-']").submit(function(e, params) {
        e.preventDefault();
        const form = $(e.currentTarget);
        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize(),
            success: function(data) {
                if (data.result)
                {
                    form.parent().children("div").children("button[form='" + form.attr('id') + "']").remove();
                    form.parent().parent().removeClass("table-secondary");
                }
                else
                {
                    alert(data);
                    console.log(data);
                }
            },
            error: function(data) {
                alert(data);
                console.log(data);
            }
        });
    });


    // Toast to list imported accounts to handle:
    if ($('#import-toast div').length > 2)
    {
        $('#import-toast').css('z-index', 1021).toast('show');

        $('.toast-delete-imported-account').click(function(e) {
            const target = $(e.currentTarget);
            const form = target.parent().children("form");
            target.children("i").addClass("d-none");
            target.children("div").removeClass("d-none");
            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: form.serialize(),
                success: function(data) {
                    if (data.result)
                    {
                        target.parent().animate({height: '0px'}, 100, function() {
                            $(this).remove();

                            if ($('#import-toast div').length == 2)
                            {
                                $('#import-toast').toast('hide').css('z-index', -1);
                            }
                        });
                    }
                    else
                    {
                        alert(data);
                        console.log(data);
                        target.children("i").removeClass("d-none");
                        target.children("div").addClass("d-none");
                    }
                },
                error: function(data) {
                    alert(data);
                    console.log(data);
                    target.children("i").removeClass("d-none");
                    target.children("div").addClass("d-none");
                }
            });
        });

        $('#toast-delete-all-imported-accounts').click(function(e) {
            const target = $(e.currentTarget);
            const form = target.parent().children("form");
            target.children("i").addClass("d-none");
            target.children("div").removeClass("d-none");
            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: form.serialize(),
                success: function(data) {
                    if (data.result)
                    {
                        $('#import-toast').toast('hide').css('z-index', -1);
                    }
                    else
                    {
                        alert(data);
                        console.log(data);
                        target.children("i").removeClass("d-none");
                        target.children("div").addClass("d-none");
                    }
                },
                error: function(data) {
                    alert(data);
                    console.log(data);
                    target.children("i").removeClass("d-none");
                    target.children("div").addClass("d-none");
                }
            });
        });
    }


    // Display spinner if import is allowed:
    var menu_spinner = $('#allowed_import_spinner');
    if (menu_spinner.length > 0)
    {
        menu_spinner.removeClass('d-none');
        setTimeout(function() {
            menu_spinner.addClass('d-none');
        }, menu_spinner.data('timeout') * 1000);
    }


    /* Amount calculator
     *
     * On the page listing operations in an account, the table header has a
     * z-index of 1020, so the toast needs a superior z-index. The z-index
     * cannot be constantly set, because otherwise it prevents interacting with
     * content covered by the toast, even when it is hidden. */
    var calculator_nb_items = 0;
    var calculator_sum = 0;
    var calculator_hidden = true;

    function calculator_update()
    {
        function update_field(field, value)
        {
            field.html(value.toFixed(2) + "&nbsp;€");
            field.removeClass("text-success text-danger");
            if (value > 0)
            {
                field.addClass("text-success");
            }
            else if (value < 0)
            {
                field.addClass("text-danger");
            }
        }

        $('#calculator-nb-items').text(calculator_nb_items);
        update_field($('#calculator-sum'), calculator_sum);
        update_field($('#calculator-avg'), calculator_sum / calculator_nb_items);

        if (calculator_nb_items > 0 && calculator_hidden)
        {
            calculator_hidden = false;
            $('#calculator-toast').css('z-index', 1021).toast('show');
        }
        else if (calculator_nb_items == 0 && !calculator_hidden)
        {
            calculator_reset();
        }
    }

    $('.amount').click(function(e) {
        var target = $(this);
        if (target.parent().is("mark"))
        {
            target.unwrap();
            calculator_nb_items -= 1;
            calculator_sum -= parseFloat(target.data('amount'));
        }
        else
        {
            target.wrap("<mark></mark>");
            calculator_nb_items += 1;
            calculator_sum += parseFloat(target.data('amount'));
        }
        calculator_update();
    });

    function calculator_reset()
    {
        $('#calculator-toast').toast('dispose').css('z-index', -1);

        calculator_sum = 0;
        calculator_nb_items = 0;
        calculator_hidden = true;

        $('mark .amount').each(function(e) {
            $(this).unwrap();
        });
    }

    $('#calculator-toast .close').click(function(e) {
        calculator_reset();
    });


    // Modal to select target account for cloning or refunding an operation:
    $('.open-select-dest-account-modal').click(function(e) {
        $('#modal-select-dest-account .modal-title').text($(e.currentTarget).data('modal-title'));
        $('#modal-select-dest-account .modal-body p').text($(e.currentTarget).data('modal-text'));

        $('#modal-select-dest-account a').each(function(i, a) {
            $(a).attr("href", $(a).data('base-href') + "?" + $(e.currentTarget).data('action') + "=" + $(e.currentTarget).data('operation'));
        });
        $('#modal-select-dest-account').modal();
    });
});
