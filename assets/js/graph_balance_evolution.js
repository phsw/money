$(function () {
    $(".graph_balance_evolution").each(function(index) {
        var this_graph = $(this);

        var graph_monthly_balance_options = {
            type: 'bar',
            data: {
                labels: this_graph.data("x").split(";"),
                datasets: [{
                    type: "line",
                    fill: false,
                    borderColor: colors[0],
                    backgroundColor: colors[0],
                    pointBackgroundColor: colors[0],
                    label: "Global Balance",
                    data: this_graph.data("line")
                },{
                    label: "Balance",
                    backgroundColor: colors[3],
                    data: this_graph.data("y")
                }]
            },
            options: {
                aspectRatio: 4,
                plugins: {
                    tooltip: {
                        mode: 'index',
                        intersect: false,
                        callbacks: {
                            label: function(context) {
                                var label = context.dataset.label || '';

                                if (label) {
                                    label += ': ';
                                }
                                if (context.parsed.y != null) {
                                    label += context.parsed.y.toFixed(2);
                                }
                                label += "€";
                                return label;
                            }
                        }
                    }
                }
            }
        };

        var balance_limit = this_graph.data("balance-limit");
        if (balance_limit != null)
        {
            graph_monthly_balance_options.options.plugins.annotation = {
                events: ["click"],
                annotations: [{
                    type: "line",
                    mode: "horizontal",
                    scaleID: "y",
                    value: balance_limit,
                    borderColor: "grey",
                    borderWidth: 1,
                    borderDash: [4, 4],
                    label: {
                        content: "Balance Limit",
                        enabled: true,
                        position: "start",
                        fontSize: 10,
                        backgroundColor: "grey"
                    }
                }]
            };
        }

        var chart = new Chart(this_graph.get(0).getContext('2d'), graph_monthly_balance_options);
    });
});
