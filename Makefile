money/settings.py:
	@cp money/settings.py.dist money/settings.py

flake:
ifeq ($(IN_GIT_HOOK), 1)
	cp .flake8 .flake8-git
	git diff --name-only | tr '\n' ',' | sed 's/^/\t/' >> .flake8-git
	flake8 --config=.flake8-git
	rm .flake8-git
else
	flake8
endif

test: flake money/settings.py
	python manage.py test

coverage: money/settings.py
	coverage run manage.py test; \
	coverage report | grep -v 100%

clean:
	rm -rf htmlcov .coverage

run-prod: money/settings.py
	firefox http://127.0.0.1:8000; \
	python manage.py runserver
