# Installation

**Requirement:** at least Python 3.10

Create a virtual environment:
```bash
python3 -m venv venv
source venv/bin/activate
```

Install dependencies listed in `requirements.txt`:
```bash
pip install -r requirements.txt
```

If you will use MySQL as database:
```bash
sudo apt install python3-dev default-libmysqlclient-dev
pip install -r requirements-mysql.txt
```

Install front-end dependencies with `yarn`:
```bash
yarn install --frozen-lockfile
```

Development tools are listed in `requirements-dev.txt`.


## Set up application specific settings

Copy `money/settings.py.dist` to `money/settings.py` and adapt the content of this file according to your needs.


## Set up database

### Backends

#### SQLite

Adapt the `DATABASES` variable in `core/settings.py`:
```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'database.sqlite'
    }
}
```

#### MySQL

Duplicate file `mysql.cnf.dist` and create database with the following command:
```sql
CREATE DATABASE money CHARACTER SET utf8mb4;
```

If some configuration keys are specific, it is possible to define them directly in `money/settings.py`:
```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'money',
        'USER': 'root',
        'HOST': '/opt/lampp/var/mysql/mysql.sock',
        'OPTIONS': {
            'sql_mode': 'STRICT_ALL_TABLES',
        }
    }
}
```

### Apply migrations

```bash
python manage.py migrate
```



## Create user:

Only useful to connect to the administration backend, sometimes needed to overstep restrictions imposed by Money.

```bash
python manage.py createsuperuser
```


## Run server

To test, you can use built-in Django server:
```bash
python manage.py runserver
```

If you want to access to Money from a different computer, you need to specify the listening address:
```bash
python manage.py runserver 0.0.0.0:8000
```
and add the server IP in the list `ALLOWED_HOSTS` in the `money/settings.py` file.




## Browser extension

In order to import data from bank website, a [Firefox
extension](https://addons.mozilla.org/fr/firefox/addon/money-exporter/) is
available. This addon adds a button on bank webpages to export account data to
Money.

To manually build the extension:
```bash
npm install --global web-ext
cd browser_extension
web-ext build
```
Then, you can temporarily load the extension in the `about:debugging` page. The
extension will be loaded until you restart Firefox. You can update the
extension by clicking on the button *Refresh* on the same page. If you have
previously installed the addon from the Mozilla system, you can disable it on
the page `about:addons`.

The publication of the addon is managed on the [Mozilla
website](https://addons.mozilla.org/developers/).
