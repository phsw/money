# Hacking

## Add a new parser

1. Download the fixture of what will be imported in `fixtures/` and remove
   sensitive data.
2. Duplicate a `Test*BankWebParser` class in `money/tests/test_import_data.py`
   and adapt its tests.
3. Duplicate a `*BankWebParser` class in `money/utils/import_data.py` and adapt
   its behaviour.
4. Add the corresponding entry to the `parser` dict of the class
   `ImportWebBankOperations` in `money/utils/import_data.py`.
5. Make sure the tests still pass:
    ```sh
    python3 manage.py test money.tests.test_import_data
    ```
6. Adapt the code of the browser extension in `browser_extension/cane.js`.
7. Increase the version in `browser_extension/manifest.json`.
8. Build and test the extension, as explained in [INSTALL](INSTALL.md).
